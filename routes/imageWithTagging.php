<?php 

Route::get('/image-tag/create','imageWithTagging\imageWithTaggingController@create')->name('image-tag.create');
Route::get('/image-tag','imageWithTagging\imageWithTaggingController@index')->name('image-tag.index');
Route::post('/images/move', 'imageWithTagging\ImageController@move')->name('image-tag.move');
Route::post('/image-tag/uploadFile','imageWithTagging\imageWithTaggingController@uploadFile')->name('image-tag.upload');
Route::get('/image-tag/{id}/edit','imageWithTagging\imageWithTaggingController@edit')->name('image-tag.edit');
Route::get('/image-tag/editImage/{id?}','imageWithTagging\imageWithTaggingController@getImage')->name('image-tag.image.edit');
Route::patch('image-tag/update/',['uses' => 'imageWithTagging\imageWithTaggingController@update','as' => 'image-tag.update',]);
Route::post('images/delete','imageWithTagging\imageWithTaggingController@deleteImage')->name('images.delete');
Route::get('images/edit/delete','imageWithTagging\imageWithTaggingController@deleteEditPageImage')->name('edit.images.delete');
Route::post('image-tag/delete',['uses' => 'imageWithTagging\imageWithTaggingController@delete','as' => 'image-tag.delete',]);

?>