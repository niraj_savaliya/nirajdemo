<?php

Route::group(['namespace'=>'IpbLock'],function(){

	Route::get('ipblock-list','IpblockController@index')->name('ipblock.index');
	Route::post('ipblock-store','IpblockController@store')->name('ipblock.store');
	Route::get('ipblock-status-change','IpblockController@ChangeStatus')->name('ipblock.changestatus');
	Route::post('ipblock-delete','IpblockController@destroy')->name('ipblock.delete');
	Route::get('error-page','IpblockController@errorPage')->name('custome.error');

});

Route::group(['middleware' => 'fw-block-attacks'], function () 
{
    Route::group(['middleware' => 'fw-block-blacklisted'],function(){

        Route::get('coming/soon', function()
        {
            return "We are about to launch, please come back in a few days.";
        });
        Route::group(['middleware' => ['fw-only-whitelisted']], function () 
        {
            Route::get('/firewall', 'HomeController@index');
        });
    });
});