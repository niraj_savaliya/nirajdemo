<?php

Breadcrumbs::register('employees', function ($breadcrumbs) {
	$breadcrumbs->push('Employees', route('ajax-dependency.index'), ['icon' => '<i class="fa fa-list"></i>']);
});
Breadcrumbs::register('create_employee', function ($breadcrumbs) {
	$breadcrumbs->parent('employees');
	$breadcrumbs->push('Add employee', route('ajax-dependency.create'));
});
Breadcrumbs::register('edit_employee', function ($breadcrumbs, $exist_data) {
	$breadcrumbs->parent('employees');
	$breadcrumbs->push($exist_data['name'],route('ajax-dependency.edit', $exist_data));
});

