<?php

use App\Jobs\Activitylog\ActivityLog;



Route::group(['namespace'=>'Activitylog'],function(){
	
	Route::get('activity-log', ['as' => 'activity.index', 'uses' => 'ActivityController@index']);
	
	Route::get('checkactivity',function(){
		dispatch(new ActivityLog('Brand', 'Add', 'Admin add new brand detail'));
	});
});
