<?php
	Route::resource('/dynamic_ajax_dependency','dynamic_ajax_dependency\DynamicAjaxDependencyController',['except'=>'destroy']);
	Route::post('/dynamic_ajax_dependency/delete','dynamic_ajax_dependency\DynamicAjaxDependencyController@destroy')->name('dynamic_ajax_dependency.delete');
	Route::post('dynamic_ajax_dependency/getstate/','dynamic_ajax_dependency\DynamicAjaxDependencyController@getState')->name('dynamic_ajax_dependency.getstate');
	Route::post('dynamic_ajax_dependency/getcity/','dynamic_ajax_dependency\DynamicAjaxDependencyController@getCity')->name('dynamic_ajax_dependency.getcity');
?>