<?php

	
Route::get('social/login',[
			'as'=> 'social.index',
			'uses'=> 'social_login\SocialLoginController@index'
			]);
Route::get('auth/{provider}','social_login\SocialLoginController@redirectToProvider');
		
Route::get('auth/{provider}/callback', 'social_login\SocialLoginController@handleProviderCallback');