<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/welcome',function(){
	return 'hi';
});
Route::get('/',function(){
	return redirect()->route('login');
});

/**Start Route For Admin login module**/
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('login','Auth\LoginController@login');
Route::post('email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/reset','Auth\ResetPasswordController@reset')->name('password.reset.post');
Route::get('password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');
/**End Route For Admin login module**/


Route::group( ['middleware' => ['auth']], function() {

		// start route for change password
		Route::get('/change-password', ['as' => 'changepassword.get', 'uses' => 'ChangePasswordController@create']);
		Route::post('/change-password', ['as' => 'changepassword.post', 'uses' => 'ChangePasswordController@store']);

		/**Start Route for logout**/
		Route::get('logout', 'Auth\LoginController@logout')->name('logout');
		/**End Route for logout**/

		/**Start Route For Dashboard Module**/
		Route::get('dashboard', 'DashboardController@index')->name('dashboard');
	
		/**End Route For Dashboard Module**/

		Route::resource('posts', 'Permission\PostController');
		Route::resource('permissions', 'Permission\PermissionController');
		Route::resource('roles', 'Permission\RoleController');
		Route::resource('users', 'Permission\UserController');
		
});
