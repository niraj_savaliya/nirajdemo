<?php 

Route::group(['namespace'=>'ImageCrop'],function()
{
	Route::get('imagecrop/list','ImageController@index')->name('imagecrop.index');
	Route::get('imagecrop','ImageController@create')->name('imagecrop.create');
	Route::post('imagecrop/store','ImageController@store')->name('imagecrop.store');
	Route::get('imagecrop/edit/{id}','ImageController@edit')->name('imagecrop.edit');
	Route::post('imagecrop/update/{id}','ImageController@update')->name('imagecrop.update');
	Route::post('imagecrop/delete','ImageController@destroy')->name('imagecrop.delete');

	// Route::post('imagecrop-profile', ['as' => 'imagecrop.post', 'uses' => 'ImageController@saveUploadImage']);

	Route::post('/create-image', ['as' => 'image-before-crop.post', 'uses' => 'ImageController@saveImageBeforeCrop']);
	Route::post('/create-image-crop', ['as' => 'image-after-crop.post', 'uses' => 'ImageController@saveImageAfterCrop']);
});
?>