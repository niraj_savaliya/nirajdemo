<?php
	
Route::group(['namespace'=>'PDFGenerate'],function(){

	Route::resource('pdf-generate','PDFController');

	Route::post('get-pdf',['as'=>'download.pdf','uses'=>'PDFController@getPDF']);
});