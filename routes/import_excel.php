<?php

Route::group(['namespace'=>'ImportExcel'],function(){
		Route::get('ImportDetails',['as'=>'import.index','uses'=>'ImportExcelController@index']);
		Route::post('import-excel', ['as'=>'users.import','uses'=>'ImportExcelController@importExcel']);
		Route::post('export-excel', ['as'=>'users.export','uses'=>'ImportExcelController@getExport']);
		Route::get('download-excel', ['as'=>'export.download','uses'=>'ImportExcelController@downloadExcel']);
		Route::post('delete',['as' => 'import.delete' , 'uses' => 'ImportExcelController@destroy']);
});