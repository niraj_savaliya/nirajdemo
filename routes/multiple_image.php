<?php

Route::group(['namespace'=>'MultipleImage'],function(){

	Route::resource('multiple-image','ImageController',['except'=>'delete']);

	//Route::post('multiple-image','ImageController');

	Route::post('delete-image',['as'=>'multiple-image.image.delete','uses'=>'ImageController@deleteImage']);

	Route::post('delete',['as'=>'multiple.delete','uses'=>'ImageController@delete']);

});