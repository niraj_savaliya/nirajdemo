<?php
	
Route::group(['namespace'=>'AjaxDependency'],function(){

	Route::resource('ajax-dependency','EmployeeController', ['except' => 'destroy']);

	Route::post('getState',['as'=>'state.get','uses'=>'EmployeeController@getState']);

	Route::post('getCity',['as'=>'city.get','uses'=>'EmployeeController@getCity']);

	Route::post('employees/delete', 'EmployeeController@destroy')->name('employees.delete');
});