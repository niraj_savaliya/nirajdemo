<?php

	Route::resource('dynamicform','Dynamicform\DynamicformController',['except'=>'destroy']);
	Route::post('dynamicform/delete',[
		'uses' => 'Dynamicform\DynamicformController@delete',
		'as' => 'dynamicform.delete',
	]);