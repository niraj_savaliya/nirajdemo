<?php 

namespace App\Repositories\Image;

use App\Repositories\Image\ImageContract;
use App\Events\Image\ImageIsSaved;
use App\Models\imageTagImage;
use Response, Photo,Event;

/**
 * Class EloquentImageRepository
 * @package App\Repositories\Image
 */
class EloquentImageRepository implements ImageContract {

	/**
	 * @param $file
	 * @return static
	 */
	public function moveToTemp($file) 
	{

		$original_name   = $file->getClientOriginalName();
        $changed_name    = sha1(microtime()) . '.' . $file->getClientOriginalExtension();
        $destinationPath = public_path() . '/images/temp/';
        
        $images_data['name']      = $original_name;
        $images_data['path_name'] = $changed_name;
        $images_data['type']      = $file->getMimeType();
        $images_data['size']      = $file->getSize();

        $file->move($destinationPath,$changed_name);
        $image = imageTagImage::create($images_data);
		return $image->id;
	}

	/**
	 * @param $id
	 * @param $folder
	 * @param $dimension
	 * @return mixed
	 */
	public static function moveToPermanent($id, $folder, $dimension = [null, null]) {
		// dd($dimensi
		$source      = public_path(). '/images/temp/';
        $destination = public_path(). '/upload/' . $folder . '/';
        $image = imageTagImage::find($id);
        // dd($destination);
		// dd($source . $image->path_name);
        if( file_exists($source . $image->path_name) ) {
		// dd($id);

        	if (!file_exists($destination)) {
			    mkdir($destination, 0777, true);
			}
            rename($source . $image->path_name, $destination . $image->path_name);
            // exit;
            // rename($source . $image->path_name, $destination . "image_173x173".$image->path_name);
            $event['path'] = $destination . $image->path_name;
            
            if($dimension == '')
            {
            	$event['dimension'][0] = 0;
            	$event['dimension'][1] = 0;
            }
            $event['dimension'] = $dimension;
           	if(count($dimension)){
           		// dd('');
	            $images = Photo::make($destination . $image->path_name);
	            $images->resize($dimension[0], $dimension[1]);
	            $images->save($destination ."image_173x173_" .$image->path_name);
           	}
        	// Event::fire(new ImageIsSaved(['path' => $destination . $image->path_name, 'dimension' => $dimension]));
        }
	}

	/**
	 * @param $id
	 * @param $folder
	 * @return mixed
	 */
	public function delete($id, $folder) {
		$file  = imageTagImage::find($id);
        $temp_file_delete = public_path().'/images/temp/' . $file->path_name;
        $perm_file_delete = public_path().'/upload/' . $folder . '/' . $file->path_name;
        
        @unlink($temp_file_delete);
        @unlink($perm_file_delete);
        
        $file->delete();
	}
}