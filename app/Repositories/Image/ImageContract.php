<?php 

namespace App\Repositories\Image;

/**
 * Interface ImageContract
 * @package App\Repositories\Image
 */
interface ImageContract {

	/**
	 * @param $data
	 * @return mixed
	 */
	public function moveToTemp($data);

	/**
	 * @param $id
	 * @param $folder
	 * @return mixed
	 */
	public function delete($id, $folder);
}