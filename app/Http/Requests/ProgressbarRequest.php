<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProgressbarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|mimetypes:image/jpeg,image/png,image/jpg|image|max:4096'
        ];
    }

    public function messages(){
        return [
            'image.mimetypes' => 'The image must be type og jpeg or png.',
            'image.max' => 'The image should not be greater than 4MB.'
        ];
    }
}
