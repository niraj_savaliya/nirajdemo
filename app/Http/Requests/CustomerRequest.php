<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                    'fname' => 'required|alpha|max:50',
                    'lname' => 'required|alpha|max:50',
                    'birthdate' => 'required|before:now',
                    'salary' => 'required|numeric',
                    'department_name' => 'required',
                    'email' => 'required|email|unique:customers,email',
                    'address' => 'required',
                ]; 
            }
            case 'PATCH':
            {
                return [
                    'fname' => 'required|alpha|max:50',
                    'lname' => 'required|alpha|max:50',
                    'birthdate' => 'required|before:now',
                    'salary' => 'required|numeric',
                    'department_name' => 'required',
                    'email' => 'required|email',
                    'address' => 'required',
                ]; 
            }
            default : break;
        }
    }

    public function messages()
    {
        return [
            'fname.required' => 'The firstname field is required.',
            'fname.alpha' => 'The firstname may only contain letters.',
            'fname.max' => 'The firstname may not be greater than 50 characters.',
            'lname.required' => 'The lastname field is required.',
            'lname.alpha' => 'The lastname may only contain letters.',
            'lname.max' => 'The lastname may not be greater than 50 characters.',
            //'department_name.required' => 'The department field is required.',
        ];

    }
}
