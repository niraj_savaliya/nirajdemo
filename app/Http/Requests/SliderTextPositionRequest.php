<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class SliderTextPositionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $data = $request->all();
        switch($this->method())
        {
            case 'POST' :
            {
                $rules = [
                    'title' => 'required',
                    'image' =>  'required|mimes:jpeg,png|dimensions:width=1110,height=400',
                ];
                if($data['enable_title'] == '1'){
                    $rules = [
                        'title' => 'required',
                        'position' => 'required',
                        'image' =>  'required|mimes:jpeg,png|dimensions:width=1110,height=400',
                    ];
                }
                return $rules;
            }
            case 'PATCH' :
            {
                $rules = [
                    'title' => 'required',
                    'image' =>  'nullable|mimes:jpeg,png|dimensions:width=1110,height=400',
                ];
                if($data['enable_title'] == '1'){
                    $rules = [
                        'title' => 'required',
                        'position' => 'required',
                        'image' =>  'nullable|mimes:jpeg,png|dimensions:width=1110,height=400',
                    ];
                }
                return $rules;
            }
            default:break;
        }
    }
}
