<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class DynamicformRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        switch($this->method())
        {
            case 'POST':
            {
                $rules = [
                    'fname'=>'required|alpha',
                    'lname'=>'required|alpha',
                    'email' => 'required|email',
                    'address' =>'required',
                ];
                foreach(Input::get('student.student') as $key => $val)
                {
                    $rules['student.student.'.$key.'.birthdate'] = 'required|before:now';
                    $rules['student.student.'.$key.'.city'] = 'required|alpha';
                    $rules['student.student.'.$key.'.mobile'] = 'required|numeric|digits:10';
                    $rules['student.student.'.$key.'.pincode'] = 'required|numeric|digits:6';
                }
                return $rules;
            }
            case 'PATCH':
            {
                $data  = $this->all();
                $rules = [
                    'fname'=>'required|alpha',
                    'lname'=>'required|alpha',
                    'email' => 'required|email',
                    'address' =>'required',
                ];
                foreach(Input::get('student.student') as $key => $val)
                {
                    $rules['student.student.'.$key.'.birthdate'] = 'required|before:now';
                    $rules['student.student.'.$key.'.city'] = 'required|alpha';
                    $rules['student.student.'.$key.'.mobile'] = 'required|numeric|digits:10';
                    $rules['student.student.'.$key.'.pincode'] = 'required|numeric|digits:6';
                }
                return $rules;
            }
            default:break;
        }

    }

    public function messages()
    {
        $messages = [
            'fname.required'=>'Please enter first name.',
            'fname.alpha'=>'This first name containing only letters.',
            'lname.required'=>'Please enter last name.',
            'lname.alpha'=>'This last name containing only letters.',
            'email.required'=>'Please enter email.',
            'email.email' => 'The email field contain email format.',
            'address.required'=>'Please enter address.',
        ];
        foreach(Input::get('student.student') as $key => $val)
        {
            $messages['student.student.'.$key.'.birthdate.required'] = 'Please enter birthdate.';
            $messages['student.student.'.$key.'.birthdate.before'] = 'Not enter a future date.';
            $messages['student.student.'.$key.'.city.required'] = 'Please enter city.';
            $messages['student.student.'.$key.'.city.alpha'] = 'This name containing only letters.';
            $messages['student.student.'.$key.'.mobile.required'] = 'Please enter mobile no.';
            $messages['student.student.'.$key.'.mobile.numeric'] = 'This mobile must be a number.';
            $messages['student.student.'.$key.'.mobile.digits'] = 'This mobile must be 10 digits.';
            $messages['student.student.'.$key.'.pincode.required'] = 'Please enter pincode.';
            $messages['student.student.'.$key.'.pincode.numeric'] = 'This pincode must be a number.';
            $messages['student.student.'.$key.'.pincode.digits'] = 'This pincode must be 6 digits.';
        }

        return $messages;
    }
}
