<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;

class DynamicAjaxDependencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                $rules = [
                    'name' => 'required|regex:/^[\pL\s\-]+$/u',
                    'email' => 'required|email|unique:employee_saves,email',
                ];

                foreach(Input::get('shipping.shipping') as $key => $val)
                {
                    $rules['shipping.shipping.'.$key.'.city'] = 'required';
                    $rules['shipping.shipping.'.$key.'.state'] = 'required';
                    $rules['shipping.shipping.'.$key.'.country'] = 'required';
                }
                return $rules;
            }
            case 'PATCH':
            {
                $rules =[
                    'name' => 'required|regex:/^[\pL\s\-]+$/u',
                    'email' => 'required |email|unique:employee_saves,email,'.$this->segment(3),
                ];
                
                foreach(Input::get('shipping.shipping') as $key => $val)
                {
                    $rules['shipping.shipping.'.$key.'.city'] = 'required';
                    $rules['shipping.shipping.'.$key.'.state'] = 'required';
                    $rules['shipping.shipping.'.$key.'.country'] = 'required';
                }
                return $rules;
            }
            default:break;
        }
    }

    public function messages()
    {
        foreach(Input::get('shipping.shipping') as $key => $val)
        {
            $messages['shipping.shipping.'.$key.'.city.required'] = 'Please Select city';
            $messages['shipping.shipping.'.$key.'.state.required'] = 'Please Select state';
            $messages['shipping.shipping.'.$key.'.country.required'] = 'Please Select country';
        }

        return $messages;
    }
}
