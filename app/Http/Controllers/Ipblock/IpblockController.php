<?php

namespace App\Http\Controllers\Ipblock;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Firewall;

class IpblockController extends Controller
{
	public function index(Request $request)
	{
        $type = $request->get('type');
        $append_query = 1;
        if($type == 'blacklist')
        {
            $append_query = 0;
        }
		if ($request->ajax()) {
            $where_str = "1 = ?";
            $where_params = array(1);

            if (!empty($request->input('sSearch'))) {
                $search = $request->input('sSearch');
                $where_str .= " and ( ip_address like \"%{$search}%\""
                            . " or whitelisted like \"%{$search}%\""
                            . ")";
            }
            $columns = array('id','ip_address','whitelisted');

            $firewall_count = Firewall::select($columns)
                ->whereRaw($where_str, $where_params)
                ->count();

            $firewall = Firewall::select($columns)
                ->where('whitelisted',$append_query)
                ->whereRaw($where_str, $where_params);

            if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
                $firewall = $firewall->take($request->input('iDisplayLength'))
                    ->skip($request->input('iDisplayStart'));
            }
            if ($request->input('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $firewall = $firewall->orderBy($column, $request->input('sSortDir_' . $i));
                }
            }
            $firewall = $firewall->get();

            $response['iTotalDisplayRecords'] = $firewall_count;
            $response['iTotalRecords'] = $firewall_count;
            $response['sEcho'] = intval($request->input('sEcho'));
           
            $response['aaData'] = $firewall->toArray();

            return $response;
        }
    	return view('ipblock.index');
	}
    public function create()
    {
    	return view('ipblock.create');
    }
    public function store(Request $request)
    {
    	$this->validate($request, [
			'ip_address' => 'required|unique:firewall,ip_address',
			
		], [
			'ip_address.required' => 'Enter Ip address.',
		]);

    	$data = $request->all();
		$firewall = new Firewall;

		$firewall->ip_address = $data['ip_address'];
		$firewall->whitelisted = $data['status'];
		$firewall->save();
        
        return response()->json(array('success' => true,'message'=>'User added successfully'),200);

		// return redirect()->route('ipblock.index')->with('message','Record added successfully')->with('message_type','success');
    }
    public function ChangeStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;

        if ($status == 0) {

          $updateRecord = Firewall::where('id', $id)->update(['whitelisted' => 1]);

           return response()->json(['success'=>true,'message'=>'User added in Whitelist successfully'],200);      

        }

        if ($status == 1) {

           $updateRecord = Firewall::where('id', $id)->update(['whitelisted' => 0]);

           return response()->json(['success'=>true,'message'=>'User added in Blacklist successfully'],200);     
        }
    }
    public function destroy(Request $request)
    {
        $id = $request->get('id');

        if (!is_array($id)) {
            $id = array($id);
        }

        $firewall_delete = Firewall::whereIn('id', $id)->delete();
        
        return response()->json(array('success' => true), 200);
    }
    public function errorPage()
    {
        return view('ipblock.error_page');
    }
}
