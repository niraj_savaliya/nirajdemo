<?php

namespace App\Http\Controllers\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Event;
use App\Models\Customer;
use App\Events\Customer\CustomerEvent;
use App\Http\Requests\CustomerRequest;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
          $where_str    = "1 = ?";
          $where_params = array(1); 
          
          if (!empty($request->input('sSearch')))
          {
              $search     = $request->input('sSearch');
              $where_str .= " and (fname like \"%{$search}%\""
                    . " or birthdate like \"%{$search}%\""
                    . " or department_name like \"%{$search}%\""
                    . " or gender like \"%{$search}%\""
                    . " or email like \"%{$search}%\""
                    . ")";
          }                                            
          
           $columns = ['id','created_at','fname','lname','birthdate','salary','department_name','gender','address','email','updated_at'];
          
          $customer_columns_count = Customer::select($columns)
                ->whereRaw($where_str, $where_params)
                ->count();
          
          $customer_list = Customer::select($columns)
                ->whereRaw($where_str, $where_params);
            
          if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $customer_list = $customer_list->take($request->input('iDisplayLength'))
                                    ->skip($request->input('iDisplayStart'));
          }          

          if($request->input('iSortCol_0')){
              $sql_order='';
              for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
              {
                  $column = $columns[$request->input('iSortCol_' . $i)];
                  if(false !== ($index = strpos($column, ' as '))){
                      $column = substr($column, 0, $index);
                  }
                  $customer_list = $customer_list->orderBy($column,$request->input('sSortDir_'.$i));   
              }
          } 

          
          $customer_list = $customer_list->get();

          $response['iTotalDisplayRecords'] = $customer_columns_count;
          $response['iTotalRecords'] = $customer_columns_count;
          $response['sEcho'] = intval($request->input('sEcho'));
          $response['aaData'] = $customer_list->toArray();
          
          return $response;
        }

        return view('customer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $customer_event_data = $request->all();

        $customer_data = [
          'customer_data' => $customer_event_data,
        ];

        Event::fire(new CustomerEvent($customer_data));

        return response()->json(['success'=>true,'message'=>'Data added successfully'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer_data = Customer::find($id);
        $id = $customer_data['id'];

        return view('customer.edit',['customer_data'=>$customer_data,'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, $id)
    {
        $customer_event_data = $request->all();
        $customer_data = [
          'customer_data' => $customer_event_data,
          'id' => $id
        ];
        Event::fire(new CustomerEvent($customer_data));
        return response()->json(['success'=>true,'message'=>'Data updated successfully'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $customer_id = $request->get('id');
        
        $customers = Customer::whereIn('id', $customer_id)->delete();
        return response()->json(['success'=>true,'message'=>'Data deleted successfully'],200);
    }
}
