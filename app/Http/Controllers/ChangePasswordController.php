<?php
/*
| ChangePasswordController
| --------------------------------------------------------------------------
| @Date : 1-march-2018
| Route Name : ChangePassword come from the web.php file
| This controller handles the Password change functionality
|
 */
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Illuminate\Http\Request;

class ChangePasswordController extends Controller {
	/**
	 * Show the form for Updating password.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return View('changepassword');
	}
	/**
	 * Store a newly Updated Password  in users table.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'old_password' => 'required',
			'password' => 'required|confirmed|min:8',
			'password_confirmation' => 'required',
		]);

		$old_input = $request->get('old_password');

		$users = User::find(auth()->user()->id);

		if (!hash::check($old_input, $users->password)) {
			return back()->with('message', 'Old Password is incorrect')
				->with('message_type', 'warning');
		}

		$users->password = Hash::make($request->password);
		$users->save();

		return redirect()->route('dashboard')->with('message', 'Password Change Successfully')->with('message_type', 'success');
	}
}
