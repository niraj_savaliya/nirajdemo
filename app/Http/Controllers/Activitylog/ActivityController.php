<?php

namespace App\Http\Controllers\Activitylog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;

class ActivityController extends Controller
{
    /**
     * Display a listing of the all Activity. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $where_str = "1 = ?";
            $where_params = array(1);

            if (!empty($request->input('sSearch'))) {
                $search = $request->input('sSearch');
                $where_str .= " and ( content_type like \"%{$search}%\""
                            . " or action like \"%{$search}%\""
                            . " or description like \"%{$search}%\""
                            . " or details like \"%{$search}%\""
                            . ")";
            }
            $columns = array('id', 'content_type', 'action','description','details');

            $activity_count = Activity::select('id', 'content_type', 'action','description','details')
                ->whereRaw($where_str, $where_params)
                ->count();

            $activity = Activity::select($columns)
                ->whereRaw($where_str, $where_params);

            if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
                $activity = $activity->take($request->input('iDisplayLength'))
                    ->skip($request->input('iDisplayStart'));
            }
            if ($request->input('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $activity = $activity->orderBy($column, $request->input('sSortDir_' . $i));
                }
            }
            $activity = $activity->get();

            $response['iTotalDisplayRecords'] = $activity_count;
            $response['iTotalRecords'] = $activity_count;
            $response['sEcho'] = intval($request->input('sEcho'));
           
            $response['aaData'] = $activity->toArray();

            return $response;
        }
        return view('activity_log.index');
    }
}
