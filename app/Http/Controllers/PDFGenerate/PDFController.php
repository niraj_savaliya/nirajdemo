<?php

namespace App\Http\Controllers\PDFGenerate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Helpers\FileHelp;
use PDF;
use \Mpdf\Mpdf;
class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	if ($request->ajax()) {

            $where_str = "1 = ?";
            $where_params = array(1);

            if (!empty($request->input('sSearch'))) {
                $search = $request->input('sSearch');
                $where_str .= " and ( name like \"%{$search}%\""
                    . " or country like \"%{$search}%\""
                    . " or state like \"%{$search}%\""
                    . " or city like \"%{$search}%\""
                    . ")";
            }
            $columns = array('id','created_at as creation_date','name','city', 'country', 'state');

            $employee_count = Employee::select('*')
                ->whereRaw($where_str, $where_params)
                ->count();

            $employee = Employee::select($columns)
                ->whereRaw($where_str, $where_params);

            if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
                $employee = $employee->take($request->input('iDisplayLength'))
                    ->skip($request->input('iDisplayStart'));
            }

            if ($request->input('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $employee = $employee->orderBy($column, $request->input('sSortDir_' . $i));
                }
            }
            $employee = $employee->get();
            $response['iTotalDisplayRecords'] = $employee_count;
            $response['iTotalRecords'] = $employee_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $employee->toArray();

            return $response;
        }

        $total_record = Employee::get()->count();

        return view('pdf_generation.index',compact('total_record'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    //code for download pdf 
    public function getPDF(Request $request)
    {
        $total_count = $request->get('total_record');

        $btn_val = $request->get('download');

        $total_input = $request->get('total_number');

        $user_details = Employee::select('name', 'country', 'state', 'city')->get()->toArray();
        if(!file_exists('upload/report'))
        {
            mkdir('upload/report',0777,true);
        }
        if($total_input != null)
        {
            $user_details = array_chunk($user_details,$total_input);
        }
        else
        {
            $user_details = array_chunk($user_details,$total_count);
        }

        $pdf_file = view('pdf_generation.report', compact('user_details'))->render();

        $mpdf = new \Mpdf\Mpdf(['format' => 'A4']);
        $mpdf->SetFont('eurostyle');
        $mpdf->setFooter("Page {PAGENO} of {nb}");
        $mpdf->WriteHTML($pdf_file);
        $blank = $mpdf->page+1;
        $mpdf->Deletepages($blank);
        $filename = 'report_' . date('d_M_Y') . '.pdf';
        $filepath = UPLOAD_PATH . '/report/' . $filename;
        $mpdf->Output($filepath);
        
        return response()->download($filepath);
    }
}
