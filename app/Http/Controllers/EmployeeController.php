<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\City;
use App\State;
use Form;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country_list = ["" => "Select Country"] + Country::pluck('name', 'id')->toArray();

        $state_list = (old('country')) ? [""=>"Select State"] + State::where("country_id",old('country'))->orderBy('name','asc')->pluck('name', 'id')->toArray() : array(""=>"Select State"); // 

        $city_list = (old('state')) ? [""=>"Select City"] + City::where("state_id",old('state'))->orderBy('name','asc')->pluck('name', 'id')->toArray() : array(""=>"Select City");

        return view('hiral.employee.create',compact('city_list','state_list','country_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'name' =>'required',
        ], [
            'name.required' =>'Please enter name',
            'country.required' => 'Select country',
            'state.required' => 'Select state',
            'city.required' => 'Select city', 
        ]);

        $data = $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getState(Request $request)
    {
        $contry_id = $request->get('country_id');

        $get_stateIn = State::where('country_id', $contry_id)->pluck('name', 'id')->toArray();

        $states = ['' => 'Select State'] + $get_stateIn;

        return Form::select('state_id', $states, array('id' => 'state_id'));
    }
    public function getCity(Request $request)
    {
        $state_id = $request->get('state_id');

        $get_cityIn = City::where('state_id', $state_id)->pluck('name', 'id')->toArray();

        $cities = ['' => 'Select City'] + $get_cityIn;

        return Form::select('city_id', $cities, array('id' => 'city_id'));
    }
}
