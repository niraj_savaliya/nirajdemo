<?php

namespace App\Http\Controllers\MultipleImage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventImage;
use App\Helpers\FileHelp;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         if ($request->ajax()) {

            $where_str = "1 = ?";
            $where_params = array(1);

            if (!empty($request->input('sSearch'))) {
                $search = $request->input('sSearch');
                $where_str .= " and ( title like \"%{$search}%\""
                    . " or active like \"%{$search}%\""
                    . ")";
            }
            $columns = array('id','created_at as creation_date','title');

            $employee_count = Event::select('*')
                ->whereRaw($where_str, $where_params)
                ->count();

            $employee = Event::select($columns)
                ->whereRaw($where_str, $where_params);

            if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
                $employee = $employee->take($request->input('iDisplayLength'))
                    ->skip($request->input('iDisplayStart'));
            }

            if ($request->input('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $employee = $employee->orderBy($column, $request->input('sSortDir_' . $i));
                }
            }
            $employee = $employee->get();
            $response['iTotalDisplayRecords'] = $employee_count;
            $response['iTotalRecords'] = $employee_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $employee->toArray();

            return $response;
        }
        return view('multiple_image.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('multiple_image.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, [
            'title' => 'required|unique:events,title',
            'files' => 'required',
        ], [
            'title.required' =>'Please enter title',
            'files.required' =>'Please select images',
            'title.unique' =>'This title already exist'
        ]);

        $event_data = $request->all();
        $save_data = new Event();
        $save_data->fill([$save_data]);
        $save_data->title = ucwords($event_data['title']);
        $save_data->save();
        
        if ($save_data) {

            $files = $request->only('files');
           
            if (isset($files)) {

                $image_array = $files['files'];
                
                if (isset($image_array)) {

                    foreach ($image_array as $key => $single_image_array) {

                        $image_save = new EventImage();
                        
                        if ($request->hasfile('files')) {

                            $event_id = $save_data->id;
                       
                            $filename1 = FileHelp::getfilename($single_image_array);
                            
                            $single_image_array->move(UPLOAD_PATH . '/event_images/',$filename1);
                            
                            $image_save->event_id = $event_id;
                            
                            $image_save->image = $filename1;
                        }
                        $image_save->save();
                    }
                }
            }
        }

        return redirect()->route('multiple-image.index')->with('message', 'Record added successfully')
            ->with('message_type', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event_data = Event::find($id);
       
        $image = EventImage::select('image')->where('event_id', $event_data['id'])->get()->toArray();

        foreach ($image as $key => $single_image) {

            //dd(EVENT_PATH);
            $image_url = EVENT_PATH .$single_image['image'];
            unset($image[$key]['image']);
            $image[$key]['name'] = $single_image['image'];
            $image[$key]['type'] = "image/png";
            $image[$key]['size'] = 87399;
            $image[$key]['file'] = $image_url;
            $image[$key]['data']['url'] = $image_url;
        }
        $image_data = stripcslashes(json_encode($image));
       
        $eventImage = count(EventImage::where('event_id', $event_data['id'])->get()->toArray());
        
        return view("multiple_image.edit",compact('event_data','image_data','eventImage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|unique:events,title,'.$id,
        ], [
            'title.required' =>'Please enter title',
            'title.unique' =>'This title already exist'
        ]);

        $event_data = $request->all();
        $save_data = Event::firstOrNew(['id'=>$id]);
        $save_data->fill([$save_data]);
        $save_data->title = ucwords($event_data['title']);
        $save_data->save();

        $old_images = EventImage::select('image')->where('event_id',$id)->get()->toArray();
        
        if ($save_data) {
            $files = $request->only('files');
            if (!empty($files)) {
        
                $image_array = $files['files'];
                
                if (isset($image_array)) {

                    foreach ($image_array as $key => $single_image_array) {

                        $image_save = new EventImage();
                        
                        if ($request->hasfile('files')) {

                           /* if(!empty($old_images))
                            {
                                foreach ($old_images as $key => $value) {
                                    $image = $value['image'];
                                    $image_unlink_path = IMAGE_PATH . "/event_images";
                                    $unlink_old_image = FileHelp::UnlinkImage($image_unlink_path, $image);
                                }
                            }*/

                            $event_id = $save_data->id;

                            $filename1 = FileHelp::getfilename($single_image_array);
                            
                            $single_image_array->move(UPLOAD_PATH . '/event_images/',$filename1);
                            
                            $image_save->event_id = $event_id;
                            
                            $image_save->image = $filename1;
                            
                        }
                        $image_save->save();
                    }
                }
            }    
        } 
        return redirect()->route('multiple-image.index')->with('message', 'Record updated successfully')
            ->with('message_type', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->get('id');

        if (!is_array($id)) {

            $id = array($id);
        }

        $delete_event = Event::whereIn('id', $id)->delete();

        $delete_event_images = EventImage::whereIn('event_id', $id)->delete();

        return response()->json(array('success' => true), 200);
    }
    public function deleteImage(Request $request)
    {
        $image_data = $request->all();
        $image = $image_data['name'];
        $image_unlink_path = IMAGE_PATH . "/event_images/";
        $unlink_old_image = FileHelp::UnlinkImage($image_unlink_path, $image);
        $image_id = EventImage::where('image', $image)->delete();

        return response()->json(array('success' => true), 200);
    }
    
}