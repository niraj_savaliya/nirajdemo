<?php

namespace App\Http\Controllers\dynamic_ajax_dependency;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Http\Requests\DynamicAjaxDependencyRequest;
use Event;
use App\Events\DynamicAjaxDependencyEvent;
use App\Models\CountrySave;
use App\Models\EmployeeSave;

class DynamicAjaxDependencyController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {

            $where_str = "1 = ?";
            $where_params = array(1);

            if (!empty($request->input('sSearch'))) {
                $search = $request->input('sSearch');
                $where_str .= " and ( name like \"%{$search}%\""
                          . " or  email like \"%{$search}%\""
                    . ")";
            }
            $columns = array('id','created_at','name','email');

            $employee_count = EmployeeSave::select($columns)
                ->whereRaw($where_str, $where_params)
                ->count();

            $employee = EmployeeSave::select($columns)
                ->whereRaw($where_str, $where_params);

            if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
                $employee = $employee->take($request->input('iDisplayLength'))
                    ->skip($request->input('iDisplayStart'));
            }

            if ($request->input('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $employee = $employee->orderBy($column, $request->input('sSortDir_' . $i));
                }
            }
            $employee = $employee->get();
            $response['iTotalDisplayRecords'] = $employee_count;
            $response['iTotalRecords'] = $employee_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $employee->toArray();

            return $response;
        }
        return view('dynamic_ajax_dependency.index');
    }
    public function create(){
    	$country_list = Country::pluck('name','name')->toArray();
    	return view('dynamic_ajax_dependency.create',compact('country_list'));
    } 
    public function store(DynamicAjaxDependencyRequest $request){
    	$all_data = $request->all();
        $employee_details = [
            'employee_detail' => $all_data
        ];
        // dd($employee_details);
    	Event::fire(new DynamicAjaxDependencyEvent($employee_details));

        if($request->save_button == 'save_new'){
            return back()->with('message','Record Added Successfully')->with('message_type','success');
        }
        return redirect()->route('dynamic_ajax_dependency.index')->with('message','Record Added Successfully')->with('message_type','success');
    }
    public function destroy(Request $request){
        $employee_id = $request->get('id');
            
        if(!is_array($employee_id))
        {
            $employee_id = [$employee_id];
        }

        $employee_delete = EmployeeSave::whereIn('id',$employee_id)->delete();
        $country_delete = CountrySave::whereIn('employee_id',$employee_id)->delete();
        
        return response()->json(array('success' => true),200);
    }
    public function edit($id){
        $edit_exist_employee = EmployeeSave::find($id);
        // dd($edit_exist_employee);
        $employee_details = CountrySave::where('employee_id',$id)->get();
        // dd($employee_details);
        $employee_info = [];
        $country_name = '';

        foreach ($employee_details as $key => $single_employee_info) {
            
            $get_details['shipping_id'] = $single_employee_info['employee_id'];
            $get_details['city'] = $single_employee_info['city'];
            $get_details['state'] = $single_employee_info['state'];
            $get_details['country'] = $single_employee_info['country'];
            $employee_info[] = $get_details;

            // $country_name = $single_employee_info['country'];
        }

        
        //temp

        // if ($country_name == '') {
        //     $country_name = 'india';
        // }

        // $country_id = Country::where('name',$country_name)->value('id');
        $country_list = Country::pluck('name','name')->toArray();
        // $country_list = [' ' => 'Select Country'] + $country_list;

        $city_list = City::pluck('name','name')->toArray();
        // dd($city_list);
        $state_list = State::pluck('name','name')->toArray();


        return view('dynamic_ajax_dependency.edit',compact('edit_exist_employee','state','city_id','state_id','employee_info','country_id','country_list','city_list','state_list'));
    }

    public function update(DynamicAjaxDependencyRequest $request,$id){
        $all_data = $request->all();
        $all_data['id'] = $id;

        $employee_details = [
            'employee_detail' => $all_data
        ];
        // dd($employee_details);
        Event::fire(new DynamicAjaxDependencyEvent($employee_details));

        if($request->save == 'Save')
        {
            return back()->with('message','Record Updated successfully')->with('message_type','success');
        }
        
        return redirect()->route('dynamic_ajax_dependency.index')->with('message', 'Record Updated successfully')->with('message_type', 'success');
    }
    public function getState(Request $request)
    {
        $get_state = $request->get('country');
        $state = Country::where('name',$get_state)->value('id');
        $get_stateIn = State::where('country_id',$state)->pluck('name','name')->toArray();

        $state_json = [];

        foreach ($get_stateIn as $single_state => $single_state_name)
        {
            $single_state_name = [
                'id' => $single_state,
                'text' => $single_state_name
            ];
            $state_json[] = $single_state_name;
        }

        return response()->json(['success'=>true,'type'=>'state','data' => $state_json]);
    }
    public function getCity(Request $request)
    {
        $get_state = $request->get('state');
        $state = State::where('name',$get_state)->value('id');
        $get_cityIn = City::where('state_id',$state)->pluck('name','name')->toArray(); 

        $city_json = [];
        foreach ($get_cityIn as $single_city => $single_city_name)
        {
            $single_city_name = [
                'id' => $single_city,
                'text' => $single_city_name
            ];
            $city_json[] = $single_city_name;
        }
        //dd($city_json);
        return response()->json(['success'=>true,'type'=>'city','data' => $city_json]);
    }
}
