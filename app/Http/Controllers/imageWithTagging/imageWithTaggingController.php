<?php

namespace App\Http\Controllers\imageWithTagging;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Image\ImageContract;
use App\Models\imageTagImage;
use App\Models\ImageTagTaggingImage;
use App\Repositories\Image\EloquentImageRepository as EIR;
use App\Jobs\ImageTaggingJob;
use Validator,Redirect;
class imageWithTaggingController extends Controller
{
	public function create()
	{
		return view('imageWithTagging.create');
	}
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $where_str = "1 = ?";
            $where_params = array(1);

            if (!empty($request->input('sSearch'))) {
                $search = $request->input('sSearch');
                $where_str .= " and ( image_caption like \"%{$search}%\""
                    . ")";
            }
            $columns = array('id','image_caption','image_name');

            $imagetag_count = ImageTagTaggingImage::select('*')
                ->whereRaw($where_str, $where_params)
                ->count();

            $imagetag = ImageTagTaggingImage::select($columns)
                ->whereRaw($where_str, $where_params);

            if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
                $imagetag = $imagetag->take($request->input('iDisplayLength'))
                    ->skip($request->input('iDisplayStart'));
            }

            if ($request->input('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $imagetag = $imagetag->orderBy($column, $request->input('sSortDir_' . $i));
                }
            }
            $imagetag = $imagetag->get();
            $response['iTotalDisplayRecords'] = $imagetag_count;
            $response['iTotalRecords'] = $imagetag_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $imagetag->toArray();

            return $response;
        }
        return view('imageWithTagging.index');
    }
    public function uploadFile(Request $request)
    {
        $image_details = $request->all();
        if(empty($image_details['images'])) 
        {
            $this->validate($request,[
                'images' => 'required',
            ],
            [
                'images.required' => 'At least One Image Is Require in png or jpg Format',
            ]);
        }
       $data = $this->validate($request,[
            "image_name"    => "required|array",
            "image_name.*"  => "required|unique:imagetag_taggingimages,image_caption",
            ],
            [
                'image_name.*.required' =>'Please fill the images name',
                'image_name.*.unique' =>'Image Caption Already Exist'
            ]
        );
        $image_id = $request['images'];
        $image_name = $request['image_name'];
    	$array_combine = array_combine($image_id, $image_name);
    	$image_tags = dispatch(new ImageTaggingJob($array_combine));

    	if($request->save_button == 'save_new')
        {
            return back()->with('message','Image Uploaded Successfully')->with('message_type','success');
        }
    	    return redirect()->route('image-tag.index')
                ->with('message', 'Image Uploaded Successfully')
                ->with('message_type', 'success');
    }
    public function edit($id)
    {
    	$tag_image =ImageTagTaggingImage::find($id);
    	return view('imageWithTagging.edit',compact('tag_image'));
    }
    public function getImage($id){
        if(isset($id))
        { 
    	   $tag_image = ImageTagTaggingImage::where('id',$id)
                        ->get(['image_caption','image_name','image_id']);
    	   return response()->json(['images'=>$tag_image]);
        }
    }
    public function update(Request $request)
    {
        $image_details = $request->all();
        if(empty($image_details['images'])) 
        {
            $this->validate($request,[
                'images' => 'required',
            ],
            [
                'images.required' => 'At least One Image Is Require in png or jpg Format',
            ]);
        }
        $data = $this->validate($request,[
            "image_name"    => "required|array",
            "image_name.*"  => "required",
            ],
            [
                'image_name.*.required' =>'Please fill the images name'
            ]
        );
    	$image_id = $request['images'];
        $image_name = $request['image_name'];

        $array_combine = array_combine($image_id, $image_name);
        $image_tags = dispatch(new ImageTaggingJob($array_combine));

        return redirect()->route('image-tag.index')->with('message', 'Image Updated Successfully')
                ->with('message_type', 'success');
    }
    public function delete(Request $request)
    {
        $image_id = $request->get('id');
        $images = ImageTagTaggingImage::whereIn('id', $image_id)->delete();
        return response()->json(['success'=>true,'message'=>'Data deleted successfully'],200);
    }
    public function deleteImage(Request $request)
    {
        // dd($request->all());
        $delete_image = imageTagImage::where('id',$request['id'])->forceDelete();
    }
    public function deleteEditPageImage(Request $request)
    {
        // dd('asd');
        $tag_image = imageTagImage::where('path_name','=',$request['file'])->forceDelete();
        $user_image = ImageTagTaggingImage::where('image_name','=',$request['file'])->forceDelete();

       return response()->json(['success'=>true,'message'=>'Image deleted successfully'],200);
    }
}
