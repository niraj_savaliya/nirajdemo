<?php

namespace App\Http\Controllers\imageWithTagging;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Image\ImageContract;
use App\Models\Image;
use Response;
use Validator;
use Redirect;
class ImageController extends Controller
{
    /**
	     * @param ImageContract $image
	     */
	  public function __construct(ImageContract $images) {
	        $this->image = $images;
	    }

	    public function move(Request $request) 
	    { 
	    	//dd($request->all());
		    $files = $request->only('file');
	      
	        foreach ($files['file'] as $key => $file) {
	        	$id = $this->image->moveToTemp($file);
	            // dd($id);
	            return Response::json(array('success' => true, 'image_id' => $id), 200);
	        }
	       // return response()->json(['success'=>true,'data'=>$data]);
	    }
	    
	    public function delete(Request $request) 
	    {
	        $this->image->delete($request->get('id'), $request->get('folder'));

	        return Response::json(array('success' => true), 200);
	    }
}
