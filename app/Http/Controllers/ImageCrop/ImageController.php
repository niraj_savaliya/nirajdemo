<?php

namespace App\Http\Controllers\ImageCrop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ImageCrop;
use App\Helpers\FileHelp;
use App\Helpers\ImageResize;

class ImageController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $where_str    = "1 = ?";
            $where_params = array(1); 
        
            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and ( title like \"%{$search}%\""
                            ." or image_name like \"%{$search}%\""
                            . ")";
            }
            $columns = ['id','title','image_name','created_at','updated_at'];
            
            $image_count = ImageCrop::select('id')
                                    ->whereRaw($where_str, $where_params)
                                    ->count();

            $image = ImageCrop::select($columns)
                              ->whereRaw($where_str, $where_params);
            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                            $image = $image->take($request->input('iDisplayLength'))
                                    ->skip($request->input('iDisplayStart'));
             }
            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $image = $image->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            }

            $image = $image->get();
            $response['iTotalDisplayRecords'] = $image_count;
            $response['iTotalRecords'] = $image_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $image->toArray();
            return $response;
        }
        return view('imagecroper.index');
    }
    public function create()
    {   
    	return view('imagecroper.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'profile_pic' => 'required',
            'title' =>'required',
        ], [
            'profile_pic.required' =>'Please select image',
            'title.required' =>'Please enter title',
        ]);

        $image = new ImageCrop();
        $image->image_name = $request->profile_pic;
        $image->title = $request->title;
        $image->save();

        return redirect()->route('imagecrop.index')->with('message', 'Record added successfully')
            ->with('message_type', 'success');
      
    }
    public function edit($id)
    {
        $exist_data = ImageCrop::find($id);
        return view('imagecroper.edit',compact('exist_data'));
    }
    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'title' =>'required',
        ], [
            'title.required' =>'Please enter title',
        ]);
        $data = $request->all();

        $image_data = ImageCrop::find($id);
        $image_data->title = $data['title'];
        $image_data->image_name = $data['profile_pic'];
        $image_data->save();

        return redirect()->route('imagecrop.index')->with('message', 'Record updated successfully')
            ->with('message_type', 'success');
    }

    public function destroy(Request $request)
    {
        $id = $request->get('id');

        if (!is_array($id)) {
            $id = array($id);
        }

        $image = ImageCrop::whereIn('id', $id)->delete();

        return response()->json(array('success' => true), 200);
    }
    public function saveImageBeforeCrop(Request $request){
        $this->validate($request, [
            'profile_pic' => 'required|mimetypes:image/jpeg,image/png,image/jpg|image|max:4096',
        ], [
            'profile_pic.required' => 'Please upload your image.',
            'profile_pic.mimetypes' => 'Please upload jpg, jpeg or png image.',
            'profile_pic.max' => 'Please upload image less than 4MB.',
        ]);
        $sourcePath = UPLOAD_PATH . "/temp/";

        FileHelp::checkDir($sourcePath);

        $destinationPath = UPLOAD_PATH . "/temp/before_crop/";
        FileHelp::checkDir($destinationPath);

        $image_data = $request->all();
        $image_request = $image_data['profile_pic'];

        $image_name = FileHelp::getfilename($image_request);
        
        $image_request->move($destinationPath, $image_name);
        $image_url = CROP_IMAGE_PATH . "/temp/before_crop/" . $image_name;

        return response(['success' => true, 'file_name' => $image_url, 'image_name' => $image_name ]);
    }

    public function saveImageAfterCrop(Request $request)
    {
        $destinationPath = UPLOAD_PATH . "/temp/before_crop/";


        $image_name = $request->get('file_name');
        $image_path = $destinationPath . $image_name;
        $crop_x = $request->input('cropdata.x');
        $crop_y = $request->input('cropdata.y');
        $crop_w = $request->input('cropdata.width');
        $crop_h = $request->input('cropdata.height');

        $outputfile = $destinationPath . "crop/";
        
        FileHelp::checkDir($outputfile);

        $resizeObj = new ImageResize($image_path);
       
        $resizeObj->cropCustomized($crop_x, $crop_y, $crop_w, $crop_h, $crop_w, $crop_h);
        $resizeObj->saveImage($outputfile . $image_name, 100);

       
        $finalprofilepath =  UPLOAD_PATH . "/temp/after_crop/";
       
        FileHelp::checkDir($finalprofilepath);
        $resizeObj = \Image::make($outputfile . $image_name);
        $resizeObj->resize(160, 160, function ($constraint) {
            $constraint->aspectRatio();
        });
        $resizeObj->save($finalprofilepath . $image_name);

        $profile_crop_image = $image_name;
        $profile_crop_image_url = CROP_IMAGE_PATH . "/temp/after_crop/" . $image_name;

        return response(['success' => true, 'profile_crop_image' => $profile_crop_image, 'profile_crop_image_url' => $profile_crop_image_url]);
    }
}
