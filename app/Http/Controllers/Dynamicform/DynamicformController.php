<?php

namespace App\Http\Controllers\Dynamicform;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Event;
use App\Models\Dynamicform;
use App\Models\Dynamicformdetail;
use App\Events\Dynamicform\DynamicformEvent;
use App\Http\Requests\DynamicformRequest;

class DynamicformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
          $where_str    = "1 = ?";
          $where_params = array(1); 
          
          if (!empty($request->input('sSearch')))
          {
              $search     = $request->input('sSearch');
              $where_str .= " and (fname like \"%{$search}%\""
                    . " or lname like \"%{$search}%\""
                    . " or address like \"%{$search}%\""
                    . " or email like \"%{$search}%\""
                    . ")";
          }                                            
          
           $columns = ['id','created_at','fname','lname','address','email','updated_at'];
          
          $student_columns_count = Dynamicform::select($columns)
                ->whereRaw($where_str, $where_params)
                ->count();
          
          $student_list = Dynamicform::select($columns)
                ->whereRaw($where_str, $where_params);
            
          if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $student_list = $student_list->take($request->input('iDisplayLength'))
                                    ->skip($request->input('iDisplayStart'));
          }          

          if($request->input('iSortCol_0')){
              $sql_order='';
              for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
              {
                  $column = $columns[$request->input('iSortCol_' . $i)];
                  if(false !== ($index = strpos($column, ' as '))){
                      $column = substr($column, 0, $index);
                  }
                  $student_list = $student_list->orderBy($column,$request->input('sSortDir_'.$i));   
              }
          } 

          
          $student_list = $student_list->get();

          $response['iTotalDisplayRecords'] = $student_columns_count;
          $response['iTotalRecords'] = $student_columns_count;
          $response['sEcho'] = intval($request->input('sEcho'));
          $response['aaData'] = $student_list->toArray();
          
          return $response;
        }

        return view('dynamicform.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dynamicform.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DynamicformRequest $request)
    {
        // dd($request->all());
        $student_data = $request->all();
        Event::fire(new DynamicformEvent($student_data));

        if($request->save_button == 'save_new') {
            return back()->with('message','Data added successfully..')
            ->with('message_type','success');
        }

        return redirect()->route('dynamicform.index')->with('message','Data added successfully..')
        ->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student_data = Dynamicform::find($id);
        //dd($student_data);
        $student_details = Dynamicformdetail::where('dynamicform_id',$id)->get();

        $student_data_info = [];
        foreach ($student_details as $key => $single_student_edit) 
        {
            //$get_student_details['id'] = $single_student_edit['id'];
            $get_student_details['birthdate'] = $single_student_edit['birthdate'];
            $get_student_details['city'] = $single_student_edit['city'];
            $get_student_details['mobile'] = $single_student_edit['mobile'];
            $get_student_details['pincode'] = $single_student_edit['pincode'];
            $student_data_info[] = $get_student_details;
        }
        //dd($student_data_info);
        return view('dynamicform.edit',compact('student_data','id','student_data_info'));           
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DynamicformRequest $request, $id)
    {
        $student_data = $request->all();
        $student_data['id'] = $id;

        Event::fire(new DynamicformEvent($student_data));
        if($request->save == 'Save')
        {
            return back()->with('message','Record Updated successfully')->with('message_type','success');
        }

        return redirect()->route('dynamicform.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $dynamicform_id = $request->get('id');
        if(!is_array($dynamicform_id))
        {
            $dynamicform_id = array($dynamicform_id);
        }
        
        $user = Dynamicform::whereIn('id', $dynamicform_id)->delete();
        $other_Details = Dynamicformdetail::whereIn('dynamicform_id',$dynamicform_id)->delete();
        return back()->with('message','Record Deleted Successfully');
        
    }
}
