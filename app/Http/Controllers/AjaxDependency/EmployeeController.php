<?php

namespace App\Http\Controllers\AjaxDependency;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\City;
use App\Models\State;
use Form;
use App\Models\Employee;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $where_str = "1 = ?";
            $where_params = array(1);

            if (!empty($request->input('sSearch'))) {
                $search = $request->input('sSearch');
                $where_str .= " and ( name like \"%{$search}%\""
                    . " or country like \"%{$search}%\""
                    . " or state like \"%{$search}%\""
                    . " or city like \"%{$search}%\""
                    . ")";
            }
            $columns = array('id','created_at as creation_date','name','city', 'country', 'state');

            $employee_count = Employee::select('*')
                ->whereRaw($where_str, $where_params)
                ->count();

            $employee = Employee::select($columns)
                ->whereRaw($where_str, $where_params);

            if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
                $employee = $employee->take($request->input('iDisplayLength'))
                    ->skip($request->input('iDisplayStart'));
            }

            if ($request->input('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $employee = $employee->orderBy($column, $request->input('sSortDir_' . $i));
                }
            }
            $employee = $employee->get();
            $response['iTotalDisplayRecords'] = $employee_count;
            $response['iTotalRecords'] = $employee_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $employee->toArray();

            return $response;
        }
        return view('ajax_dependency.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //fetch country ids which has states and cities
        $exists_country_ids = City::distinct()->pluck('country_id')->toArray();

        $country_list = ["" => "Select Country"] + Country::whereIn('id',$exists_country_ids)->pluck('name', 'id')->toArray();
      
        $state_list = (old('country')) ? [""=>"Select State"] + State::where("country_id",old('country'))->orderBy('name','asc')->pluck('name', 'id')->toArray() : array(""=>"Select State"); // 

        $city_list = (old('state')) ? [""=>"Select City"] + City::where("state_id",old('state'))->orderBy('name','asc')->pluck('name', 'id')->toArray() : array(""=>"Select City");

        return view('ajax_dependency.create',compact('city_list','state_list','country_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'name' =>'required',
        ], [
            'name.required' =>'Please enter name',
            'country.required' => 'Select country',
            'state.required' => 'Select state',
            'city.required' => 'Select city', 
        ]);

        $data = $request->all();
        $country = Country::where('id',$data['country'])->value('name');
        $state = State::where('id',$data['state'])->value('name');
        $city = City::where('id',$data['city'])->value('name');
        $save_data = new Employee();
        $save_data->fill([$save_data]);
        $save_data->name = $data['name'];
        $save_data->country = $country;
        $save_data->state = $state;
        $save_data->city = $city;
        $save_data->save();
       
        return redirect()->route('ajax-dependency.index')->with('message', 'Record added successfully')
            ->with('message_type', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exist_data = Employee::find($id);
        
        $country_id = Country::where('name',$exist_data['country'])->value('id');
        //fetch country ids which has states and cities
        $exists_country_ids = City::distinct()->pluck('country_id')->toArray();

        $state_id = State::where('name',$exist_data['state'])->value('id');

        $city_id = City::where('name',$exist_data['city'])->value('id');

        $country_list = ["" => "Select Country"] + Country::whereIn('id',$exists_country_ids)->pluck('name', 'id')->toArray();
      
        $state_list = (old('country')) ? [""=>"Select State"] + State::where("country_id",old('country'))->pluck("name","id")->toArray() :State::where('country_id', $country_id)->pluck("name","id")->toArray();

        $city_list = (old('state')) ? [""=>"Select City"] + City::where("state_id",old('state'))->pluck("name","id")->toArray() :["" =>"Select City"] + City::where('state_id',$state_id)->pluck('name', 'id')->toArray();

        return view('ajax_dependency.edit',compact('city_list','state_list','country_list','exist_data','country_id','state_id','city_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'name' =>'required',
        ], [
            'name.required' =>'Please enter name',
            'country.required' => 'Select country',
            'state.required' => 'Select state',
            'city.required' => 'Select city', 
        ]);

        $data = $request->all();
        $country = Country::where('id',$data['country'])->value('name');
        $state = State::where('id',$data['state'])->value('name');
        $city = City::where('id',$data['city'])->value('name');
        $save_data = Employee::firstOrNew(['id'=>$id]);
        $save_data->fill([$save_data]);
        $save_data->name = $data['name'];
        $save_data->country = $country;
        $save_data->state = $state;
        $save_data->city = $city;
        $save_data->save();

        return redirect()->route('ajax-dependency.index')->with('message', 'Record updated successfully')
            ->with('message_type', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->get('id');

        if (!is_array($id)) {

            $id = array($id);
        }

        $delete_employees = Employee::whereIn('id', $id)->delete();

        return response()->json(array('success' => true), 200);
    }
    //fetch states from country
    public function getState(Request $request)
    {
        $contry_id = $request->get('country_id');

         //fetch state ids which has cities
        $exists_state_ids = City::distinct()->pluck('state_id')->toArray();

        $get_stateIn = State::where('country_id', $contry_id)->whereIn('id',$exists_state_ids)->pluck('name', 'id')->toArray();

        $states = ['' => 'Select State'] + $get_stateIn;

        return Form::select('state_id', $states, array('id' => 'state_id'));
    }
    //fetch cities from state
    public function getCity(Request $request)
    {
        $state_id = $request->get('state_id');

        $get_cityIn = City::where('state_id', $state_id)->pluck('name', 'id')->toArray();

        $cities = ['' => 'Select City'] + $get_cityIn;

        return Form::select('city_id', $cities, array('id' => 'city_id'));
    }
}
