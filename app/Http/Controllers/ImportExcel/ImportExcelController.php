<?php

namespace App\Http\Controllers\ImportExcel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ImportExcel;
use Excel;
use Validator;

class ImportExcelController extends Controller
{
    public function index(Request $request){
    	if($request->ajax())
        {
            $where_str    = "1 = ?";
            $where_params = array(1); 
        
            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and ( name like \"%{$search}%\""
                            ." or email like \"%{$search}%\""
                            ." or mobile like \"%{$search}%\""
                            . ")";
            }
            $columns = ['id','name','email','mobile','is_active'];
            
            $institute_count = ImportExcel::select('institute.id')
                                    ->whereRaw($where_str, $where_params)
                                    ->count();
            
            $institute = ImportExcel::select($columns)
                              ->whereRaw($where_str, $where_params);
            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                            $institute = $institute->take($request->input('iDisplayLength'))
                                    ->skip($request->input('iDisplayStart'));
             }
            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $institute = $institute->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            }

            $institute = $institute->get();
            $response['iTotalDisplayRecords'] = $institute_count;
            $response['iTotalRecords'] = $institute_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $institute->toArray();
            return $response;
        }
    	return view('import_excel.index');
    }

    //Import excel 
    public function importExcel(Request $request){
        if($request->file('sample_file'))
        {
            // $this->validate($request, [
            //     "sample_file" => 'required|mimes:xls',
            // ]);
            $err_msg_array = array();
            
            $path = $request->file('sample_file')->getRealPath();
            
            // Import excle 
            $import_data = Excel::load($path, function($reader) {
                $reader->select(array('name', 'email','mobile','is_active'))->get();

            })->get();
    
            $import_data_filter = array_filter($import_data->toArray());
                 
            if(!empty($import_data_filter && sizeof($import_data_filter)))
            { 
                $export_error_data = $import_data_filter;
                $err_msg_array = [];
                $datacount = sizeof($export_error_data);
                $user_data = ImportExcel::all()->toArray();
              for($i=0;$i<$datacount;$i++)
                {    
                    // for name and unique validation
                    if(isset($export_error_data[$i]['name'])) {
                        foreach ($user_data as $key => $value) {
                            if ($value['name'] == $export_error_data[$i]['name']) {
                                $err_msg_array[$i][] = 'The name is already exist';   
                            }
                            else
                            {
                                $first_name = $export_error_data[$i]['name'];
                            }
                        }
                    } else {
                        $first_name = '';
                    }
                    
                    // for email
                    if(isset($export_error_data[$i]['email'])) {
                        $last_name = $export_error_data[$i]['email'];
                    } else {
                        $last_name = '';
                    }

                    // for status
                    if(isset($export_error_data[$i]['is_active'])) {
                        $active = $export_error_data[$i]['is_active'];
                     } else {
                        $active = '';
                    }

                    // for mobile
                    if(isset($export_error_data[$i]['mobile'])) {
                        $type = $export_error_data[$i]['mobile'];
                    } else {
                        $state = '';
                    }
                    $rules = [
                        'name'=>'required',
                        'email'=>'required',
                        'is_active'=>'required',
                        'mobile'=>'required',
                    ];

                    $v = Validator::make($export_error_data[$i],$rules);
                    $messages = $v->messages();
            
                    foreach ($messages->all() as $message)
                    {
                       $err_msg_array[$i][] = $message;
                    }
                    $errormessage = [];
                    $errorrow = [];
                    foreach($err_msg_array as  $key => $value) {
                        for($k=0; $k<count($value); $k++) {
                          $errorlist= $value[$k].' in row ' . ($key+2) . "\r\n";
                          array_push($errormessage, $errorlist);
                          array_push($errorrow, $key+2);
                        }
                    }
            
                    if(empty($err_msg_array[$i]))
                    {

                      $dataImported = $export_error_data[$i];

                      // unset data with error
                      unset($export_error_data[$i]);

                      // insert data in database
                        ImportExcel::insert($dataImported);
                        
                    }
                }   
                       
                   
                    if (empty($errormessage)) {
                    
                         return redirect()->route('import.index')->with('message', 'Data added successfully')
                        ->with('message_type', 'success');
                        
                    }
                    else
                    {   
                        // remove space and convert into array
                        $error_list = preg_replace( "/\r|\n/", "", htmlentities(implode(" , ",$errormessage)));

                        // explode data with comma saprate
                        $errors = explode(',',$error_list);

                        // convert array in json format 
                        $download_data = json_encode($export_error_data);

                        // html rendering for error messages
                        $get_html = view('import_excel.error_list',compact('errors','download_data'))->render();
                      
                        // remove space from above html render
                        $error_html_list = preg_replace( "/\r|\n/", "", $get_html);

                        return redirect()->back()->withInput()->with('get_html',$error_html_list)->with('download_data',$download_data);
                        
                    }
            }
        }     
    }

    // Export data with errors
    public function getExport(Request $request){

        $request_data = $request->all();

        $data = json_decode($request_data['download_data'],true);
        
        $sheet_name = 'active users data';

        $user_excel_name = 'error_data_from_' . date('Y_m_d_H_i_s');

        Excel::create($user_excel_name, function ($excel) use ($data,$sheet_name) {
            $excel->sheet($sheet_name, function ($sheet) use ($data) {

                $percounting = 0;
                foreach ($data as $key => $value) {
                    if($value['is_active'] == '')
                    {
                        $value['is_active'] = '0';
                    }
                    else{
                        $value['is_active'] = $value['is_active'];
                    }
                    $data[$percounting]['is_active'] = $value['is_active'];
                   $percounting++;
                }

                $sheet->fromarray($data);
                
                //set first row freeze
                $sheet->freezeFirstRow();

                //set name of the column data
                $sheet->row(1, array(
                    'Name','Email','Mobile','Is Active'
                ));

                //A to D rows set fonts
                $sheet->cell('A1:D1', function($cell) {
                   
                    $cell->setBackground('#ff0000');
                    // Set font
                    $cell->setFont(array(
                        'bold' => true
                    ));
                });

            });


         })->export('xls');
    }

    public function downloadExcel(Request $request){
        
        $sheet_name = 'users data';

        $user_data =['Name','Email','Mobile','Is Active'];

        $user_excel_name = 'user_data_from_' . date('Y_m_d_H_i_s');

        Excel::create($user_excel_name, function ($excel) use ($user_data,$sheet_name) {
            $excel->sheet($sheet_name, function ($sheet) use ($user_data) {

                $sheet->row(1, array(
                    'Name','Email','Mobile','Is Active'
                ));
                //A to D rows set fonts
                $sheet->cell('A1:D1', function($cell) {
                    // Set font
                    $cell->setFont(array(
                        'bold' => true
                    ));
                });

            });


         })->export('xls');
    }

    public function destroy(Request $request){
        
        $user_id = $request->get('id');
        if(!is_array($user_id))
        {
            $user_id = array($user_id);
        }

        $user_delete = ImportExcel::whereIn('id',$user_id)->delete();
        
        return response()->json(array('success' => true),200);
    }
}
