<?php

namespace App\Http\Controllers\Progressbar;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Progressbar;
use App\Http\Requests\ProgressbarRequest;

class ProgressbarController extends Controller
{
    public function index()
    {
        return view('progressbar.progressform');
    }
    public function store(ProgressbarRequest $request){
        $name = $request->file('image');
        $filename = $name->getClientOriginalName();
        // dd($filename);
        $image_file = new Progressbar(['image'=>$filename]);
        $name->move(IMAGE_PATH.'/progressbar/',$filename);
        $image_file->save();
        return response()->json(['message'=>$filename]);
    }
}
