<?php

namespace App\Http\Controllers\emailAttachment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\EmailAttachmentJob;
use Mail;
use Validator;
use Session,Redirect,Image,Storage;

class EmailWithAttachmentController extends Controller
{
    public function index()
    {
    	return view('emailAttachment.create');
    }
    public function store(Request $request)
    {
       
        $this->validate($request,[
            
            'to' => 'required',
            'subject' => 'required|min:3',
            'textmessage' => 'required',
        ]);
      
        $data = array(
            'to' => $request->to,
            'subject' => $request->subject,
            'bodyMessage' => $request->textmessage,
            'attachment' => $request->attachment,
            'cc' => $request->cc,
            'bcc' => $request->bcc
        );
        // dd($data);
        $email_job = dispatch(new EmailAttachmentJob($data));
        return redirect()->route('email-attachment.index')->with('message', 'Mail Send Successfully.')
                ->with('message_type', 'success');
    }
}