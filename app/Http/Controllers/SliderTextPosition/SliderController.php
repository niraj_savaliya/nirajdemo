<?php

namespace App\Http\Controllers\SliderTextPosition;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SliderTextPositionRequest;
use Event;
use App\Events\SliderTextPositionEvent;
use App\Models\SliderTextPosition;

class SliderController extends Controller
{
    public function index(Request $request){
    	$slider_data = SliderTextPosition::select('id','title','image','position','enable_title')->get();
    	return view('SliderTextPosition.index',compact('slider_data'));
    }
    public function create(){

    	return view('SliderTextPosition.create');
    }
    public function store(SliderTextPositionRequest $request){
    	$slider_data = $request->all();
    	Event::fire(new SliderTextPositionEvent($slider_data));
    	if($request->save_new == 'Save & New'){
    		return back()->with('message','Record Added Successfully')->with('message_type','success');
    	}
    	return redirect()->route('slider.index')->with('message','Record Added Successfully')->with('message_type','success');
    }
    public function edit($id){
        $slider_data = SliderTextPosition::find($id);
        $id = $slider_data['id'];

        return view('SliderTextPosition.edit',compact('slider_data','id'));
    }
    public function update(SliderTextPositionRequest $request,$id){
        $slider_data = $request->all();
        $slider_data['id'] = $id;

        Event::fire(new SliderTextPositionEvent($slider_data));
        if($request->save == 'Save')
        {
            return back()->with('message','Record Updated successfully')->with('message_type','success');
        }
        
        return redirect()->route('slider.index')->with('message', 'Record Updated successfully')->with('message_type', 'success');
    }
    public function delete(Request $request){
        $id = $request->id;
        $image = SliderTextPosition::select('image')->where('id',$id)->first()->toArray();
        $i = $image['image'];
        $path = public_path()."/slider/".$i;
        if(file_exists($path))
        {
            unlink($path);
        }

        SliderTextPosition::destroy($id);
        return back();
    }
}
