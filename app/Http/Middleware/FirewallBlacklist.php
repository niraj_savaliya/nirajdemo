<?php

namespace App\Http\Middleware;

use PragmaRX\Firewall\Filters\Blacklist;
use App\Models\Firewall;

class FirewallBlacklist extends FilterMiddleware
{
    protected $blacklist;

    public function __construct(Blacklist $blacklist)
    {
        $this->blacklist = $blacklist;
    }

    /**
     * Filter Request.
     *
     * @return mixed
     */
    public function filter()
    {
        $ip = \Request::ip();

        $check_value = Firewall::where('ip_address',$ip)->value('whitelisted');
        
        if($check_value == 0)
        {
            return redirect()->route('custome.error');
        }       
    

        return $this->blacklist->filter();
    }
}
