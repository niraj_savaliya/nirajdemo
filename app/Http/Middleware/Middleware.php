<?php

namespace App\Http\Middleware;

abstract class Middleware
{
    public function enabled()
    {
        return config('firewall.enabled');
    }
}
