<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ClearanceMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {        
        if (Auth::user()->hasPermissionTo('allPermission')) //If user has this //permission
    {
            return $next($request);
        }
        if ($request->isMethod('index')) //If user is editing a post
         {
            if (!Auth::user()->hasPermissionTo('viewPost')) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        if ($request->is('create'))//If user is creating a post
         {
            if (!Auth::user()->hasPermissionTo('createpost'))
         {
                abort('401');
            } 
         else {
                return $next($request);
            }
        }

        if ($request->is('edit')) //If user is editing a post
         {
            if (!Auth::user()->hasPermissionTo('editpost')) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        if ($request->isMethod('Delete')) //If user is deleting a post
         {
            if (!Auth::user()->hasPermissionTo('deletePost')) {
                abort('401');
            } 
         else 
         {
                return $next($request);
            }
        }

        return $next($request);
    }
}