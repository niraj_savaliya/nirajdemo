<?php

namespace App\Jobs\Dynamicform;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Dynamicform;
use App\Models\Dynamicformdetail;
use Illuminate\Http\Request;

class DynamicformJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $student_data;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($student_data)
    {
        $this->student_data = $student_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $student_data= $this->student_data;
        // dd($student_data);
        $id = $request->id;
        if(isset($student_data['id']))
        {
            $id = $student_data['id'];  
        }

        $save_detail =Dynamicform::firstOrNew(['id' => $id]);
        $save_detail->fill($student_data);

        $save_detail->save();

        $student_other_delete = Dynamicformdetail::where('dynamicform_id',$save_detail->id)->delete();
        $student_details = $request->input('student.student');

        foreach($student_details as $key=>$single_student_details)
        {
            $save_student_details = new Dynamicformdetail();
            $save_student_details->dynamicform_id = $save_detail->id;
            $save_student_details->birthdate = $single_student_details['birthdate'];
            $save_student_details->city = $single_student_details['city'];
            $save_student_details->mobile = $single_student_details['mobile'];
            $save_student_details->pincode = $single_student_details['pincode'];
            $save_student_details->save();
        }
        return;
    }
}
