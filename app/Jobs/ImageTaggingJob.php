<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Repositories\Image\ImageContract;
use App\Models\imageTagImage;
use App\Models\ImageTagTaggingImage;
use App\Repositories\Image\EloquentImageRepository as EIR;

class ImageTaggingJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $image_tags;
    public function __construct($image_tags)
    {
        $this->image_tags =$image_tags;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $array_combine = $this->image_tags;
        $tagging_images = [];
        foreach ($array_combine as $key => $tag_image) {
            
            EIR::moveToPermanent($key,  'permenent/',[150,88]);
            $tagging_images = imageTagImage::where('id',$key)->get()->toArray();
            $image_names = [];
            foreach ($tagging_images as $key => $value) {
                $image_names['id'] = $value['id'];
                $image_names['path_name'] = $value['path_name'];
            }
            $image_id = $image_names['id'];
            $tagging_image = ImageTagTaggingImage::firstOrNew(['image_id'=>$image_id]);
            $tagging_image->image_id = $image_names['id'];
            $tagging_image->image_caption = $tag_image;
            $tagging_image->image_name = $image_names['path_name'];
            $tagging_image->save();
        }
    }
}
