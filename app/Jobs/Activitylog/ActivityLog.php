<?php

namespace App\Jobs\Activitylog;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\User;
use Activity;

class ActivityLog implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    public $fetch_contentType;
    public $fetch_action;
    public $fetch_description;

    public function __construct($fetch_contentType, $fetch_action, $fetch_description)
    {
        $this->fetch_contentType = $fetch_contentType;
        $this->fetch_action = $fetch_action;
        $this->fetch_description = $fetch_description;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fetch_contentType = $this->fetch_contentType;
        $fetch_action = $this->fetch_action;
        $fetch_description = $this->fetch_description;
        $id = auth()->user()->id;
     
        $user_info = User::where('id', $id)->first();
        $user_name = $user_info->name;

      
        $log_array = [
            'content_id' => $id,
            'contentType' => $fetch_contentType,
            'action' => $fetch_action,
            'description' => $fetch_description,
            'details' => 'Username: ' . $user_name,
            'user_id ' => $id,
        ];
        // dd($log_array);
        Activity::log($log_array);
    }
}
