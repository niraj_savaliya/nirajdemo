<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\EmailAttachment;
use Illuminate\Http\Request;
use App\Helpers\FileHelp;
use Mail;
class EmailAttachmentJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data)
    {

        $this->data =$data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {

        $data = $this->data;
        $save_detail = new EmailAttachment;
        $to = json_encode($data['to']);
        $save_detail->email = $to;   
        // foreach ($data['to'] as $key => $to) {
        //     $save_detail->email = $to;  
        // }
        if($data['cc'] != '')
        {
            $cc = json_encode($data['cc']);
            $save_detail->cc = $cc;
            // foreach ($data['cc'] as $key => $cc) {
            //     $save_detail->email = $cc;
            // }
        }
        if($data['bcc'] != '')
        {
            $bcc = json_encode($data['bcc']);
            $save_detail->bcc = $bcc;
            // foreach ($data['bcc'] as $key => $bcc) {
            //     $save_detail->email = $bcc;
            //     if($data['attachment'] != "")
            //     {
            //         foreach ($data['attachment'] as $key => $value) {
            //             $filename = FileHelp::getfilename($value);
            //             $save_detail->attachment = $filename;
            //         }
            //     }
            //     $save_detail->subject = $data['subject'];
            //     $save_detail->message = $data['bodyMessage'];
            //     $save_detail->save();

            // }
        }
        $filename = [];
        if($data['attachment'] != "")
        {
            foreach ($data['attachment'] as $key => $value) {
                $filename[] = FileHelp::getfilename($value);
            }
        }
        $file_json = json_encode($filename);
        $save_detail->attachment = $file_json;

        $save_detail->subject = $data['subject'];
        $save_detail->message = $data['bodyMessage'];
        $save_detail->save();   
        Mail::send('emailAttachment.sendmail',$data,function ($message) use ($data){

            foreach ($data['to'] as $key => $value) {

                $message->to($value);
            }
            if($data['cc'] != '')
            {
                foreach ($data['cc'] as $key => $cc) {
                    $message->cc($cc);
                }
            }
            if($data['bcc'] != '')
            {
                foreach ($data['bcc'] as $key => $bcc) {
                    $message->bcc($bcc);
                }
            }
            $message->subject($data['subject']);
            $message->from('noreply@thinktanker.in');
            $message->setBody($data['bodyMessage']);
            if($data['attachment'] != "")
            {
                foreach ($data['attachment'] as $key => $value) {

                    $message->attach($value->getRealPath(),array(
                    'as' => 'file.'.$value->getClientOriginalExtension(),
                    'mime' => $value->getMimeType())
                    );
                }
            } 
        });
        
    }
}
