<?php

namespace App\Jobs\Customer;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $customer_data;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($customer_data)
    {
        $this->customer_data =$customer_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $customer_data= $this->customer_data;
        $id = $request->id;
        if(isset($customer_data['id']))
        {
            $id = $customer_data['id'];  
        }

        $save_detail =Customer::firstOrNew(['id' => $id]);
        $save_detail->fill($customer_data['customer_data']);

        $save_detail->save();
        return;
    }
}
