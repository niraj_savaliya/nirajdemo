<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\CountrySave;
use Illuminate\Http\Request;
use App\Models\EmployeeSave;

class DynamicAjaxDependencyJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $employee_details;
    public function __construct($employee_details)
    {
        $this->employee_details = $employee_details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $employee_details = $this->employee_details;
        // dd($employee_details);
        $id = $request->id;
        if(isset($all_data['id'])){
            $id = $all_data['id'];
        }

        $employee_save = EmployeeSave::firstornew(['id'=>$id]);

        $employee_save->fill($employee_details['employee_detail']);
        // dd($employee_save);
        $employee_save->save();

        $deatils = CountrySave::where('employee_id',$employee_save->id)->delete();
        $all_details = $request->input('shipping.shipping');
        foreach($all_details as $key=>$single_detail){
            $save_detail = new CountrySave();
            $save_detail->employee_id = $employee_save->id;
            $save_detail->country = $single_detail['country'];
            $save_detail->state = $single_detail['state'];
            $save_detail->city = $single_detail['city'];
            $save_detail->save();
        }
        return;
    }
}
