<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\SliderTextPosition;
use Illuminate\Http\Request;

class SliderTextPositionJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $slider_data;
    public function __construct($slider_data)
    {
        $this->slider_data = $slider_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $slider_data = $this->slider_data;
        $id = $request->id;
        if(isset($slider_data['id'])){
            $id = $slider_data['id'];
        }
        $save_slider_data = SliderTextPosition::firstorNew(['id'=>$id]);
        $old_image = $save_slider_data['image'];

        $save_slider_data->fill($slider_data);
        if(!empty($slider_data['image'])){
            $folder_name = "slider";
            if(!empty($old_image)){
                $path = public_path()."/".$folder_name."/".$old_image;
                if(file_exists($path)){
                    unlink($path);
                }
            }
            if(isset($slider_data['image'])){
                $image = $slider_data['image'];
                $image_name = time()."_".$image->getClientOriginalName();
                $image->move(public_path()."/".$folder_name,$image_name); 
            }
            $save_slider_data->image = $image_name;
        }
        if($slider_data['enable_title'] == '0'){
            $save_slider_data->position = null;
        }
        $save_slider_data->save(); 
    }
}
