<?php
namespace App\Helpers;

class FileHelp{
	public static function getfilename($file){

        $ext = last(explode('.',$file->getClientOriginalName()));
        $microtime       = microtime();
        $search          = array('.',' ');
        $microtime       = str_replace($search, "_", $microtime);
        $fileName        = sha1($microtime).".".$ext;

        return $fileName;
    }
    public static function getfolderpathexit($filepath,$imagename){
        $file_exit = $filepath.'/'.$imagename;
        if (empty($imagename)) {
            return "no";
        }else{
            if (file_exists($file_exit)) {
                return "yes";
            }else{
                return "no";
            }
        }
    }

    public static function UnlinkImage($filepath,$fileName){
        $old_image = $filepath.$fileName;

        if (file_exists($old_image)) {
            @unlink($old_image);
        }
    }

    public static function checkDir($destinationPath){
        if (!is_dir($destinationPath)) {
           mkdir($destinationPath, 0777);
        }
    }
}
