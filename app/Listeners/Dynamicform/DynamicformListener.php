<?php

namespace App\Listeners\Dynamicform;

use App\Events\Dynamicform\DynamicformEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Dynamicform\DynamicformJob;
use Illuminate\Foundation\Bus\DispatchesJobs;


class DynamicformListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StudentEvent  $event
     * @return void
     */
    public function handle(DynamicformEvent $event)
    {
        $student_data = $event->student_data;
        $save_detail= $this->dispatch(new DynamicformJob($student_data));
    }
}
