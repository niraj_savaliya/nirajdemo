<?php

namespace App\Listeners\Customer;

use App\Events\Customer\CustomerEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Customer\CustomerJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class CustomerListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerEvent  $event
     * @return void
     */
    public function handle(CustomerEvent $event)
    {
        $customer_data = $event->customer_data;
        $save_detail= $this->dispatch(new CustomerJob($customer_data));
    }
}
