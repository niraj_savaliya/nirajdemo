<?php

namespace App\Listeners;

use App\Events\SliderTextPositionEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\SliderTextPositionJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class SliderTextPositionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SliderTextPositionEvent  $event
     * @return void
     */
    public function handle(SliderTextPositionEvent $event)
    {
        $slider_data = $event->slider_data;

        $save_detail = $this->dispatch(new SliderTextPositionJob($slider_data));
    }
}
