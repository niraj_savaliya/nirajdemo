<?php

namespace App\Listeners;

use App\Events\DynamicAjaxDependencyEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\DynamicAjaxDependencyJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class DynamicAjaxDependencyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CountryEvent  $event
     * @return void
     */
    public function handle(DynamicAjaxDependencyEvent $event)
    {
        $employee_details = $event->employee_details;

        $save_detail = $this->dispatch(new DynamicAjaxDependencyJob($employee_details));
    }
}
