<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dynamicformdetail extends Model
{
    protected $table = "dynamicformdetails";
    protected $fillable = ['id','student_id','birthdate','city','mobile','pincode'];
}
