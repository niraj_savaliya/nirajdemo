<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderTextPosition extends Model
{
    protected $table = 'slider_text_positions';
    protected $fillable = ['title','enable_title','position','image'];

    public function settitleAttribute($title){
    	$this->attributes['title'] = ucfirst($title);
    }
}
