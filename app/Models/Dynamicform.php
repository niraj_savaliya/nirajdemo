<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dynamicform extends Model
{
    protected $table = "dynamicforms";
    protected $fillable = ['id','fname','lname','email','address'];
}
