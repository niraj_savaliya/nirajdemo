<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class imageTagImage extends Model
{
	protected $table = 'imagetag_images';
    protected $fillable = ['name', 'path_name', 'type', 'size'];
}
