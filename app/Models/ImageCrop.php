<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageCrop extends Model
{
    protected $table = 'image_crop';
    protected $fillable = ['id','title','image_name'];
}
