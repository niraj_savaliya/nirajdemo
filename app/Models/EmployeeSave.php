<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeSave extends Model
{
    protected $tabel = 'employee_saves';
    protected $fillable = ['id','name','email'];

    public function setNameAttribute($name){
    	$this->attributes['name'] = ucfirst($name);
    }
}
