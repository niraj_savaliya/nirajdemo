<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class SocialProvider extends  Authenticatable
{
	protected $table = 'social_providers';
	protected $fillable =['id','user_id','provider_id','provider','profile','remember_token'];
	
    function user()
	{
		return $this->belongsTo(User::class);
	}
}
