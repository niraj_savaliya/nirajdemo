<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountrySave extends Model
{
    protected $table = 'country_save';
    protected $fillable = ['id','employee_id','country','state','city','created_at'];
}
