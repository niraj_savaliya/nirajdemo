<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    protected $table = 'activity_log';

    protected $fillable = ['user_id','content_type','action','description','details','ip_address','user_agent'];
}	
