<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailAttachment extends Model
{
    protected $table = 'email_attachment';
	protected $fillable =['id','email','subject','message','attachment'];
}
