<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Progressbar extends Model
{
    protected $table = 'progressbar';
    protected $fillable = ['id','image','created_at','updated_at']; 
}
