<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageTagTaggingImage extends Model
{
	protected $table = 'imagetag_taggingimages';
    protected $fillable = ['image_id', 'image_caption', 'image_name'];	
}
