<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapInboundRoutes();
        $this->mapActivityLogRoutes();
        $this->mapAjaxDependencyRoutes();
        $this->mapSocialiteLoginRoutes();
        $this->mapProgressbarRoutes();
        $this->mapSlidertextPositionRoutes();
        $this->ajaxRoutes();
        $this->emailAttachment();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    protected function ajaxRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/ajax_crudroutes.php'));
    }
    protected function emailAttachment()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/email_attachment.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
    protected function mapInboundRoutes()
    {
        Route::prefix('inbound')
             ->middleware('web')
             ->namespace($this->namespace."/inbound")
             ->group(base_path('routes/inbound.php'));
    }
    protected function mapActivityLogRoutes()
    {
        Route::prefix('activitylog')
             ->middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/activitylog.php'));
    }
    protected function mapAjaxDependencyRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/ajax_dependency.php'));
    }
    protected function mapSocialiteLoginRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/socialite_login.php'));
    }
    protected function mapProgressbarRoutes()
    {
        Route::prefix('progressbar')
             ->middleware('web')  
             ->namespace($this->namespace)
             ->group(base_path('routes/progressbar.php'));
    }

    protected function mapSlidertextPositionRoutes()
    {
        Route::prefix('slidertextposition')
             ->middleware('web')  
             ->namespace($this->namespace)
             ->group(base_path('routes/slidertextposition.php'));
    }
}
