<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapInboundRoutes();
        $this->mapActivityLogRoutes();
        $this->mapAjaxDependencyRoutes();
        $this->mapSocialiteLoginRoutes();
        $this->mapProgressbarRoutes();
        $this->mapSlidertextPositionRoutes();
        $this->ajaxRoutes();
        $this->mapImportExcelRoutes();
        $this->mapIpBlockRoutes();
        $this->mapdynamicformRoutes();
        $this->mapPDFGenerateRoutes();
        $this->emailAttachment();
        $this->imageWithTagging();
        $this->mapImageCropRoutes();
        $this->mapMultipleImageRoutes();
        $this->mapIpBlockgRoutes();
        $this->mapdynamicAjaxDependencyRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    protected function ajaxRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/ajax_crudroutes.php'));
    }
    protected function emailAttachment()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/email_attachment.php'));
    }
    protected function imageWithTagging()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/imageWithTagging.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
    protected function mapInboundRoutes()
    {
        Route::prefix('inbound-mail')
             ->middleware('web')
             ->namespace($this->namespace."\InboundMail")
             ->group(base_path('routes/inbound_mail.php'));
    }
    protected function mapActivityLogRoutes()
    {
        Route::prefix('activitylog')
             ->middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/activitylog.php'));
    }
    protected function mapAjaxDependencyRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/ajax_dependency.php'));
    }
    protected function mapSocialiteLoginRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/socialite_login.php'));
    }
    protected function mapProgressbarRoutes()
    {
        Route::prefix('progressbar')
             ->middleware('web')  
             ->namespace($this->namespace)
             ->group(base_path('routes/progressbar.php'));
    }

    protected function mapSlidertextPositionRoutes()
    {
        Route::prefix('slidertextposition')
             ->middleware('web')  
             ->namespace($this->namespace)
             ->group(base_path('routes/slidertextposition.php'));
    }
    protected function mapImportExcelRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/import_excel.php'));
    }
    protected function mapIpBlockRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/ipblock.php'));
    }

    protected function mapdynamicformRoutes()
    {
        Route::prefix('dynamicform')
             ->middleware('web')  
             ->namespace($this->namespace)
             ->group(base_path('routes/dynamicform.php'));
    }

    protected function mapPDFGenerateRoutes()
    {
        Route::middleware('web')  
             ->namespace($this->namespace)
             ->group(base_path('routes/pdf_generate.php'));
    }
    protected function mapImageCropRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/imagecrop.php'));
    }
    protected function mapMultipleImageRoutes()
    {
        Route::middleware('web')  
             ->namespace($this->namespace)
             ->group(base_path('routes/multiple_image.php'));

    }
    protected function mapdynamicAjaxDependencyRoutes(){
            Route::middleware('web')  
                    ->namespace($this->namespace)
                    ->group(base_path('routes/dynamic_ajax_dependency.php'));
    }
}
