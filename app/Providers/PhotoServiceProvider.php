<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class PhotoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Image\ImageContract',
            'App\Repositories\Image\EloquentImageRepository'
        );
    }
}
