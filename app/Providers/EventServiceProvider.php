<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\CustomerEvent' => [
            'App\Listeners\CustomerListener',
        ],
        'App\Events\SliderTextPositionEvent' => [
            'App\Listeners\SliderTextPositionListener',
        ],

        'App\Events\Customer\CustomerEvent' => [
            'App\Listeners\Customer\CustomerListener',
        ],

        'App\Events\Dynamicform\DynamicformEvent' => [
            'App\Listeners\Dynamicform\DynamicformListener',
        ],
        'App\Events\DynamicAjaxDependencyEvent' => [
            'App\Listeners\DynamicAjaxDependencyListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
