@extends('layout.layout')
@section('title', '| Create Permission')
@section('content')
<div class='col-md-4 col-md-offset-4'>
    <h1><i class='fa fa-key'></i> Create New Permission</h1>
    <br>
    {{ Form::open(array('url' => 'permissions')) }}
    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', '', array('class' => 'form-control')) }}
    </div><br>
    @if(!$roles->isEmpty()) 
        <h3>Assign Permission to Roles</h3>
        @foreach ($roles as $role) 
            {{ Form::checkbox('roles[]',  $role->id ) }}
            {{ Form::label($role->name, ucfirst($role->name)) }}<br>
        @endforeach
    @endif
    <br>
    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
    <a href="{{ url('permissions') }}" class="btn btn-primary">Back</a>
    {{ Form::close() }}
</div>
@endsection