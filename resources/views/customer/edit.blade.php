@extends('layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-list"></i>Customer</h4>
    </div>
</nav>
@stop
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <?= Form::model($customer_data,['method' => 'PATCH','id' => 'customer_form_data']) ?>
            <div class="col-md-6">
                <div class="form-group @if($errors->has('fname')) {{ 'has-error' }} @endif">
                    <label>FirstName<sup class="text-danger">*</sup></label>
                    <?=Form::text('fname', $customer_data['fname'], ['class' => 'form-control', 'placeholder' => 'Enter FirstName']);?>
                    <span id="fname_error" class="help-inline text-danger"><?=$errors->first('fname')?></span>
                </div></br>
                <div class="form-group @if($errors->has('birthdate')) {{ 'has-error' }} @endif">
                    <label>Date of Birth<sup class="text-danger">*</sup></label>
                    <?=Form::date('birthdate',$customer_data['birthdate'], ['class' => 'form-control']);?>
                    <span id="birthdate_error" class="help-inline text-danger"><?=$errors->first('birthdate')?></span>
                </div></br>
                <div class="form-group @if($errors->has('department_name')) {{ 'has-error' }} @endif">
                    <label>Department<sup class="text-danger">*</sup></label>
                    <?=Form::select('department_name',Config::get('department_name'),
                    $customer_data['department_name'],['class' => 'form-control']);?>
                    <span id="department_name" class="help-inline text-danger"><?=$errors->first('department_name')?></span>
                </div></br>
                <div class="form-group @if($errors->has('email')) {{ 'has-error' }} @endif">
                    <label>Email<sup class="text-danger">*</sup></label>
                    <?=Form::email('email', $customer_data['email'], ['class' => 'form-control', 'placeholder' => 'Enter email']);?>
                    <span id="email_error" class="help-inline text-danger"><?=$errors->first('email')?></span>
                </div></br>
            </div>
            <div class="col-md-6">
                <div class="form-group @if($errors->has('lname')) {{ 'has-error' }} @endif">
                    <label>LastName<sup class="text-danger">*</sup></label>
                    <?=Form::text('lname',$customer_data['lname'], ['class' => 'form-control full', 'data-width' => '100%', 'id' => 'lname_error','placeholder' => 'Enter LastName']);?>
                    <span id="lname" class="help-inline text-danger"><?=$errors->first('lname')?></span>
                </div><br>
                <div class="form-group @if($errors->has('salary')) {{ 'has-error' }} @endif">
                    <label>Salary<sup class="text-danger">*</sup></label>
                    <?=Form::text('salary', old('salary'), ['class' => 'form-control', 'placeholder' => 'Enter Salary']);?>
                    <span id="salary_error" class="help-inline text-danger"><?=$errors->first('salary')?></span>
                </div></br>
                <div class="form-group @if($errors->has('salary')) {{ 'has-error' }} @endif">
                    <label>Gender<sup class="text-danger">*</sup></label>
                    <?=Form::radio('gender','male',old('gender'));?>
                    <span id="gender_error" class="help-inline text-danger"><?=$errors->first('gender')?></span>Male
                    <?=Form::radio('gender','female',old('gender'));?>
                    <span id="gender_error" class="help-inline text-danger"><?=$errors->first('gender')?></span>Female
                </div></br>
                <div class="form-group @if($errors->has('address')) {{ 'has-error' }} @endif" style="margin-top: -15px">
                    <label>Address<sup class="text-danger">*</sup></label>
                    <?= Form::textarea('address', $customer_data['address'], ['class' => 'form-control', 'placeholder' => 'Enter address','rows'=>"4"]); ?>
                    <span id="address_error" class="help-inline text-danger"><?= $errors->first('address') ?></span>
                </div><br>
            </div>
          <?= Form::close() ?>
          <div class="text-right mb-20">
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_data" title="Save & Exit">Update</button>
            <a href="{{route('Customer.index')}}" class="btn btn-border btn-sm disabled-btn" title="Cancel">Cancel</a>
          </div> 
        </div>
    </div>
</div>
@stop
@section('script')
<?= Html::script('backend/js/jquery.form.min.js',[],IS_SECURE) ?>
<script type="text/javascript">
    $('.save_data').click(function(e,ele)
    {
        var token = "<?=csrf_token()?>";
        var id = "<?=$customer_data['id']?>";
        var url = "<?=route('Customer.update',':id')?>";
        url = url.replace(':id',id);
        
        $('#customer_form_data').ajaxSubmit({
            url: url,
            type: 'post',
            data: { "_token" : token },
            dataType: 'json',            
            beforeSubmit : function()
            {
               $("[id$='_error']").empty();
            },            
            success : function(resp)
            {                
                if(resp.success == true)
                {
                    toastr.success(resp.message);
                } 
                window.location.href = "<?= route('Customer.index') ?>";              
            },
            error : function(respObj){ 
                $.each(respObj.responseJSON, function(k,v){
                    $('#'+k+'_error').text(v);
                });
            }
        });
        return false;
    });
</script>
@include('layout.alert')
@stop