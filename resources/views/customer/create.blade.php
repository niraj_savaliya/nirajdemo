@extends('layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-list"></i>Customer</h4>
    </div>
</nav>
@stop
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <?= Form::open(['method' => 'post','id' => 'customer_form_data']) ?>
            <div class="col-md-6">
                <div class="form-group @if($errors->has('fname')) {{ 'has-error' }} @endif">
                    <label>FirstName<sup class="text-danger">*</sup></label>
                    <?=Form::text('fname', null, ['class' => 'form-control', 'placeholder' => 'Enter First Name']);?>
                    <span id="fname_error" class="help-inline text-danger"><?=$errors->first('fname')?></span>
                </div>
                </br>
                <div class="form-group @if($errors->has('birthdate')) {{ 'has-error' }} @endif">
                    <label>Date of Birth<sup class="text-danger">*</sup></label>
                    <?=Form::date('birthdate', null, ['class' => 'form-control']);?>
                    <span id="birthdate_error" class="help-inline text-danger"><?=$errors->first('birthdate')?></span>
                </div>
                </br>
                <div class="form-group @if($errors->has('department_name')) {{ 'has-error' }} @endif">
                    <label>Department<sup class="text-danger">*</sup></label>
                    <?=Form::select('department_name',Config::get('department_name'),old('department_name'),['class' => 'form-control','placeholder' => '---select department_name---']);?>
                    <span id="department_name_error" class="help-inline text-danger"><?=$errors->first('department_name')?></span>
                </div>
                </br>
                <div class="form-group @if($errors->has('email')) {{ 'has-error' }} @endif">
                    <label>Email<sup class="text-danger">*</sup></label>
                    <?=Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter email']);?>
                    <span id="email_error" class="help-inline text-danger"><?=$errors->first('email')?></span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group @if($errors->has('lname')) {{ 'has-error' }} @endif">
                    <label>LastName<sup class="text-danger">*</sup></label>
                    <?=Form::text('lname', null, ['class' => 'form-control', 'placeholder' => 'Enter Last Name']);?>
                    <span id="lname_error" class="help-inline text-danger"><?=$errors->first('lname')?></span>
                </div>
                </br>
                <div class="form-group @if($errors->has('salary')) {{ 'has-error' }} @endif">
                    <label>Salary<sup class="text-danger">*</sup></label>
                    <?=Form::text('salary', null, ['class' => 'form-control number_only', 'placeholder' => 'Enter Salary']);?>
                    <span id="salary_error" class="help-inline text-danger"><?=$errors->first('salary')?></span>
                </div>
                </br>
                <div class="form-group @if($errors->has('salary')) {{ 'has-error' }} @endif">
                    <label>Gender</label>
                    <?=Form::radio('gender', 'male', ['class' => 'form-control']);?>
                    <span id="gender_error" class="help-inline text-danger"><?=$errors->first('gender')?></span>Male
                    <?=Form::radio('gender', 'female', ['class' => 'form-control']);?>
                    <span id="gender_error" class="help-inline text-danger"><?=$errors->first('gender')?></span>Female
                </div>
                </br>
                <div class="form-group @if($errors->has('address')) {{ 'has-error' }} @endif" style="margin-top: -15px">
                    <label>Address<sup class="text-danger">*</sup></label>
                    <?= Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Enter address','rows'=>"4"]); ?>
                    <span id="address_error" class="help-inline text-danger"><?= $errors->first('address') ?></span>
                </div>
            </div>
          <?= Form::close() ?>
          <div class="text-right mb-20">
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_data" title="Save & Exit">Save</button>
            <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn save_data" title="Save & New">Save & New</button>
            <a href="{{route('Customer.index')}}" class="btn btn-border btn-sm disabled-btn" title="Cancel">Cancel</a>
          </div> 
        </div>
    </div>
</div>
@stop

@section('script')
<?= Html::script('backend/js/jquery.form.min.js',[],IS_SECURE) ?>
<script type="text/javascript">
    $('.save_data').click(function(e,ele)
    {
        var btn_val = $(this).val();
        var token = "<?=csrf_token()?>";
        $('#customer_form_data').ajaxSubmit({
            url: "<?=route('Customer.store')?>",
            type: 'post',
            data: { "_token" : token},
            dataType: 'json',            
            beforeSubmit : function()
            {
               $("[id$='_error']").empty();
            },            
            success : function(resp)
            {            
                if(resp.success == true)
                {
                    toastr.success(resp.message);
                } 
                if(btn_val == 'save_exit')
                {
    
                    window.location.href = "<?= route('Customer.index') ?>";              
                }
                else
                {
                     location.reload();  
                }
            },
            error : function(respObj){ 
                console.log(respObj.responseJSON.errors);
                $.each(respObj.responseJSON.errors, function(k,v){
                    $('#'+k+'_error').text(v);
                });
                $('.disabled-btn').removeAttr('disabled','enabled');
                $(".overlay").hide();
            }
        });
        return false;        
    });
    $(".number_only").keypress(function(h){if(8!=h.which&&0!=h.which&&(h.which<48||h.which>57))return!1});
</script>
@include('layout.alert')
@stop
