@extends('layout.layout')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Products</h1>
        <h1 class="pull-right">
        @can('createpost')
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('posts.create') !!}">Add New</a>
        @endcan
           </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-responsive" id="products-table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th colspan="4">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <td>{!! $post->title !!}</td>
                            <td>
                                {!! Form::open(['route' => ['posts.destroy',  $post->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    @can('viewPost')
                                    <a href="{!! route('posts.show', [$post->id]) !!}" class='btn btn-info btn-ms'><i class="fa fa-eye"></i></a>
                                    @endcan
                                    @can('editPost')
                                    <a href="{!! route('posts.edit', [$post->id]) !!}" class='btn btn-default btn-ms'><i class="fa fa-pencil-square-o"></i></a>
                                    @endcan
                                    @can('deletePost')
                                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-ms', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                    @endcan
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-heading">Page {{ $posts->currentPage() }} of {{ $posts->lastPage() }}</div>
                    <div class="text-center">
                        {!! $posts->links() !!}
                    </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection