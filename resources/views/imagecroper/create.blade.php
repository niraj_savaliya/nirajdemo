@extends('layout.layout')
@section('style')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('backend/css/bootstrap-fileupload.css')?>
    <?=Html::style('backend/css/image_custom.css')?>
    <?=Html::style('backend/css/style_custom.css')?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style>
        .modal-dialog .modal-content{
            padding: 40px;
        }
        .blue_invalid {
            background: #e4efef;
        }
        .modal-content {
            max-width: 1000px;
            width: 90%;
            padding: 30px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            box-shadow: 0px 2px 6px black;
            border-radius: 5px;
            background: #fff;
        }
         .editcoverpopup .editcover_upload .cropper-container span{
            padding: 0px !important;
        }
        .progress { position:relative; width:100%; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
        .bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px; }
        .percent { position:absolute; display:inline-block; top:3px; left:48%; }
    </style>
@stop
<?=Form::open(['route' => ['imagecrop.store'],'files' => true])?>
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4></h4>
    </div>
    <div class="text-right mb">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save">Save</button>
        <a href="<?= route('imagecrop.index')?>" class="btn btn-border btn-sm disabled-btn" title="Cancel">Cancel</a>
    </div>
</nav>
@stop
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group @if($errors->has('title')) {{ 'has-error' }} @endif">
                    <label>Title<sup class="text-danger">*</sup></label>
                    <?=Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title']);?>
                    <span id="title_error" class=""><?=$errors->first('title')?></span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="photo_container">
                    <div class="profile_pic_thumb" id="profile_pic_upload">
                        <img src="<?=asset('backend/images/no_image.png')?>" alt="" class="profile_pic mdl-button--raised" id="profile_crop_image_url">
                        <input type="hidden" name="profile_pic" value="" id="profile_crop_image">
                        <span class="mdl-textfield__error profile_pic_error"><?= $errors->first ('profile_pic') ?></span>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
    <div class="text-right mb">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save">Save</button>
        <a href="<?= route('imagecrop.index')?>" class="btn btn-border btn-sm disabled-btn" title="Cancel">Cancel</a>
    </div>
    <?=Form::close()?>
    @include('imagecroper.upload_form_models')
@stop
@section('script')
<?= Html::script('backend/js/bootstrap-fileupload.js') ?>
<?= Html::script('backend/js/jquery.fileuploader.min.js')?>
<?= Html::script('backend/js/jquery.form.min.js')?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.js"></script>
<script>
    $(document).ready(function(){
        $('#profile_pic_modal').modal('hide');
    });
</script>
<script>
        var token = '<?=csrf_token()?>';
        var $image = $('#frm_crop_photo_modal_src');

        $(function () {
            var cropBoxData;
            var canvasData;

            $('#frm_crop_photo_modal').on('shown.bs.modal', function () {
                $image.cropper({
                    aspectRatio: 1 / 1,
                    autoCropArea: 0,
                    minCropBoxWidth: 160,
                    zoomable: false,
                    minCropBoxHeight: 160,
                    ready: function () {
                        $image.cropper('setCanvasData', canvasData);
                        $image.cropper('setCropBoxData', cropBoxData);
                    }
                });
            });
        });

    function closeModel(modal_id){
            var modal_id_hide = '#'+modal_id;
            if (modal_id == 'profile_pic_modal_cover') {
                $(modal_id_hide).css('display','none');
            }else{
                $(modal_id_hide).modal('hide');
            };
        }
        
    $('#profile_pic_upload').click(function(){
        $('.progress1').css('display','none');
        $('.profile_pic_error').text('');
        $('#profilepic_file_upload').val('');
        $('#profile_pic_modal').modal('show');
        $image.cropper('destroy');
    });

    var bar1 = $('#bar1');
    var percent1 = $('#percent1');

    var bar2 = $('#bar2');
    var percent2 = $('#percent2');

    $('#profilepic_file_upload').change(function(e,ele) {

        var profile_pic_check = $('#profilepic_file_upload').val();

        if (profile_pic_check == '') {
            $('#profile_pic_error').text('Please upload the Image');
        }else{
            $('#frm_crop_photo').ajaxSubmit({
                url: "<?=route('image-before-crop.post')?>",
                type: 'post',
                dataType: 'json',
                beforeSubmit : function()
                {
                    $('.progress1').show();
                    var percentVal = '0%';
                    bar1.width(percentVal)
                    percent1.html(percentVal);
                    $("[id$='_error']").empty();
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    var percentVal = percentComplete + '%';
                    bar1.width(percentVal)
                    percent1.html(percentVal);
                },
                success : function(respObj)
                {
                    var file_name = respObj.file_name;

                    $("#frm_crop_photo_modal_src").attr("src",file_name);
                    $('#file_name').val(respObj.image_name);
                    $('#profile_pic_modal').modal('hide');
                    $('#frm_crop_photo_modal').modal('show');
                    $('.progress1').css('display','none');
                },
                error : function(respObj){
                    $('#profile_pic_message').hide();
                    $('.progress1').css('display','none');
                    $.each(respObj.responseJSON.errors, function(k,v){
                        $('#'+k+'_error').show().text(v);
                    });
                }
            });
        };
        return false;
    });

    $('#save_image_crop').click(function(e,ele) {
            cropBoxData = $image.cropper('getData');

            $('#frm_crop_photo_modal_form').ajaxSubmit({
                url: "<?=route('image-after-crop.post')?>",
                type: 'post',
                data: { "_token" : token, cropdata : cropBoxData },
                dataType: 'json',
                beforeSubmit : function()
                {
                    $('.progress2').show();
                    var percentVal = '0%';
                    bar2.width(percentVal)
                    percent2.html(percentVal);
                    $("[id$='_error']").empty();
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    var percentVal = percentComplete + '%';
                    bar2.width(percentVal)
                    percent2.html(percentVal);
                },
                success : function(resp)
                {
                    $('#frm_crop_photo_modal').modal('hide');
                    $('#profile_crop_image').val(resp.profile_crop_image);
                    $('#profile_crop_image_url').attr('src',resp.profile_crop_image_url);
                    $image.cropper('destroy');
                },
                error : function(respObj){
                }
            });
            return false;
        });
</script>
@stop