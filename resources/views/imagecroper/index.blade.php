@extends('layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4></h4>
    </div>
    <div class="top_filter"></div>
    <div class="pl-10">
        <a href="<?= route('imagecrop.create')?>" class="btn btn-primary btn-sm" title="Add Image"> Add image</a>
    </div>
</nav>
@stop
@section('content')
<div class="row">
<div class="col-md-12">
    <div class="card">
        <div class="card-body table-responsive">
            <div class="text-right">
                <div class="number-delete">
                    <ul>
                        <li>
                            <p class="mb-0"><span class="num_item"></span>Item Selected.</p>
                        </li>
                        <li class="bulk-dropdown"><a href="javascript:;">Bulk actions<span class="caret"></span></a>
                            <div class="bulk-box">
                                <div class="bulk-tooltip"></div>
                                <ul class="bulk-list">
                                    <li><a href="javascript:void(0);" id="delete" class="delete-btn">Delete selected Institute</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            <table id="image_table" class="table">
                <thead>
                    <tr>
                        <th class="select-all no-sort">
                            <div class="animated-checkbox">
                                <label class="m-0">
                                    <input type="checkbox" id="checkAll"/>
                                    <span class="label-text"></span>
                                </label>
                            </div>
                        </th>
                        <th>Title</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
</div>
@stop
@section('script')
<?= Html::script('backend/js/jquery.dataTables.min.js') ?>
<?= Html::script('backend/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('backend/js/fnstandrow.js') ?>
<?= Html::script('backend/js/delete_script.js') ?>
<?= Html::script('backend/js/jquery.form.min.js') ?>
<?= Html::script('backend/js/sweetalert.min.js') ?>

<script type="text/javascript">
    var table = "image_table";
    var title = "Are you sure to delete this Institute?";
    var text = "You will not be able to recover this record";
    var type = "warning";
    var delete_path = "<?= route('imagecrop.delete') ?>";
    var token = "<?= csrf_token() ?>";

    var datatable_url = "<?=route('imagecrop.index');?>";

    $(function(){
        
        $('#delete').click(function(){
            var delete_id = $('#'+table+' tbody input[type=checkbox]:checked');
            checkLength(delete_id);
        });

        image_table = $('#'+table).dataTable({
            "bProcessing": false,
            "bServerSide": true,                              
            "autoWidth": false,
            "sAjaxSource": datatable_url,
            "aaSorting": [ 1,"desc"],
            "aoColumns": [

            {
                mData: "id",
                bSortable:false,
                sWidth:"5%",
                sClass:"text-center",
                mRender: function (v, t, o) {
                    return '<div class="animated-checkbox"><label><input class="checkbox" type="checkbox" id="chk_'+v+'" name="special_id[]" value="'+v+'"/><span class="label-text"></span></label></div>';
                },
            },
            {
                mData: "title",
                bSortable:false,
                sWidth:"25%",
                sClass:"text-center",
                mRender: function(v, t, o) {
                    var edit_path = "<?=URL::route('imagecrop.edit', array(':id'))?>";
                    edit_path = edit_path.replace(':id',o['id']);

                    var act_html  = "<a title='Edit "+o['title']+"' href='"+edit_path+"'>"+ v +"</a>"

                    return act_html;
                }
            },
            {
                mData: "image_name",
                bSortable:false,
                sWidth:"25%",
                sClass:"text-center",
                mRender:function(v,t,o){
                    if(v == '')
                    {
                        return '<img src="backend/images/no_image.png" height="100px" width="70px">';
                    }
                    else
                    {
                        return '<img src="<?=CROP_IMAGE_PATH?>/temp/after_crop/'+v+'" height="100px" width="70px">';
                    }
                },
            },
            
            ],
            fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
            fnDrawCallback : function (oSettings) {
                $("div.overlay").hide();
            }
        });
        
    });
</script>
@include('layout.alert')
@stop