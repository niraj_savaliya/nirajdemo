<div class="editcoverpopup">
    <div class="modal_wrap" id="profile_pic_modal">
        <?=Form::open(['id' => 'frm_crop_photo', 'files' => true])?>
            <div class="modal_inner">
                <div class="editcover_upload">
                    <input name="profile_pic" type="file" id="profilepic_file_upload">
                    <label for="profilepic_file_upload" class="custom_file_upload">
                        <i class="li li_upload"></i>
                        <span>Upload a photo / Drag & Drop here</span>
                    </label>
                </div>
                <div class="file_msg_error_wrap">
                  <span class="blue_invalid" id="profile_pic_message">Image size should be in jpg or png format and be less than 2MB in size.</span>
                  <span id="profile_pic_error" class="red_invalid" style="display:none"></span>
                </div>
                <div class="progress progress1" style="display:none">
                    <div class="bar" id="bar1"></div>
                    <div class="percent" id="percent1">0%</div>
                </div>
                <!-- <button class="floatR mdl-button mdl-js-button mdl-js-ripple-effect primary_color" id="save_image">Upload</button> -->
                <button class="close" type="button" onclick="closeModel('profile_pic_modal')">
                    <i class="li li_cancel"></i>
                </button>
                <div class="clearfix_right"></div>
            </div>
        <?=Form::close()?>
    </div>
</div>
<div class="modal fade" id="frm_crop_photo_modal">
    <form id="frm_crop_photo_modal_form">
        <?=csrf_field()?>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="editcover_upload">
                    <img src="" id="frm_crop_photo_modal_src">
                    <input type="hidden" name="file_name" id="file_name">
                </div>
                <div class="progress progress2" style="display:none">
                  <div class="bar" id="bar2"></div >
                    <div class="percent" id="percent2">0%</div>
                  </div>
                  <button class="floatR mdl-button mdl-js-button mdl-js-ripple-effect primary_color" id="save_image_crop">Save</button>
                    <button class="close" type="button" onclick="closeModel('frm_crop_photo_modal')">
                      <i class="li li_cancel"></i>
                    </button>
                    <div class="clearfix_right"></div>
                </div>
            </div>
        </div>
    </form>
</div>
