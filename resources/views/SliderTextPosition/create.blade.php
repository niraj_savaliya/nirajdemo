@extends('layout.layout')
@section('top_fixed_content')
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4>Create Slider with text Position</h4>
        </div>
    </nav>
@stop
@section('content')
<?=Form::open(['route'=>'slider.store','role'=>'form','class'=>'m-0','files'=>true,'method'=>'POST'])?>
	<div class="card">
        <div class="card-body">
                <div class="form-group row">
                	<div class="col-sm-2 text-right">
                    	<?= Form::label('title','',['class'=>'control-label'])?><span class="text-danger"> *</span>
                    </div>
                    <div class="col-sm-8  @if($errors->has('title')) has-error @endif">
                    	<?= Form::text('title','',['class'=>'form-control','id'=>'title','placeholder'=>'Enter Title','maxlength'=>'10'])?>
                    	<span class='text-danger error'>{{ $errors->first('title') }}</span>
                    </div>
                </div>
                <div class="form-group row">
			    	<label class="col-md-2">Enable/Disable Title</label>
				    <div class="col-md-10">
				        <div class="animated-radio-button pull-left mr-10">
				            <label for="is_active_true">
				                <input type="radio" name="enable_title" value="1" id="is_active_true" {{old('enable_title') == '1' ? 'checked' : ''}} checked="checked">
				                <span class="label-text"></span> Enable
				            </label>
				        </div>
				        <div class="animated-radio-button pull-left">
				            <label for="is_active_false">
				                <input type="radio" name="enable_title" value="0" id="is_active_false" {{old('enable_title') == '0' ? 'checked' : ''}}>
				                <span class="label-text"></span> Disable
				            </label>
				        </div>
				    </div>
				</div>
                <div class="form-group row" id="position">
            		<label for="name" class="control-label col-sm-2 ">Title  Position<span class="text-danger"> *</span></label>
            		<div class="col-sm-8  @if($errors->has('position')) has-error @endif">
            			<div class="slider-positioner">
							<div class="srow">
								<div class="position">
									<label>
										<input type="radio" name="position" value="top-left" {{old('position') == 'top-left' ? 'checked' : ''}}>
										<div class="pos-box">TOP LEFT</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="top-center" {{old('position') == 'top-center' ? 'checked' : ''}}>
										<div class="pos-box">TOP CENTER</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="top-right" {{old('position') == 'top-right' ? 'checked' : ''}}>
										<div class="pos-box">TOP RIGHT</div>
									</label>
								</div>
							</div>
							<div class="srow">
								<div class="position">
									<label>
										<input type="radio" name="position" value="center-left" {{old('position') == 'center-left' ? 'checked' : ''}}>
										<div class="pos-box">CENTER LEFT</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="center-center" {{old('position') == 'center-center' ? 'checked' : ''}}>
										<div class="pos-box">CENTER CENTER</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="center-right" {{old('position') == 'center-right' ? 'checked' : ''}}>
										<div class="pos-box">CENTER RIGHT</div>
									</label>
								</div>
							</div>
							<div class="srow">
								<div class="position">
									<label>
										<input type="radio" name="position" value="bottom-left" {{old('position') == 'bottom-left' ? 'checked' : ''}}>
										<div class="pos-box">BOTTOM LEFT</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="bottom-center" {{old('position') == 'bottom-center' ? 'checked' : ''}}>
										<div class="pos-box">BOTTOM CENTER</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="bottom-right" {{old('position') == 'bottom-right' ? 'checked' : ''}}>
										<div class="pos-box">BOTTOM RIGHT</div>
									</label>
								</div>
								<span class='text-danger error'>{{ $errors->first('position') }}</span>
							</div>
						</div>
            		</div>
                </div>
                <div class="form-group row">
					<div class="col-sm-2 text-right">
						<?= Form::label('featured image','',['class'=>'control-label'])?><span class="text-danger"> *</span>
					</div>
					<div class="form-group col-sm-6">
						<div class="fileupload fileupload-new col-md-8" data-provides="fileupload">
							<div class="fileupload-new thumbnail img-responsive" style="width: 300px">
								<img src="https://via.placeholder.com/1110x400" alt="" style="width:100%;height:100%" />
							</div>
							<div class="fileupload-preview fileupload-exists thumbnail " style="width: 300px; line-height: 20px;">
							</div>
							<div>
								<label class="btn btn-file control-label btn-xs">
									<?=Form::file('image', ['class' => 'control-label col-md-2'])?>

									<span id="" class="fileupload-new btn-lg"><i class="fa fa-paper-clip"></i>Select Image</span>
									<span id="" class="fileupload-exists btn-lg"><i class="fa fa-undo"></i>Change</span>
								</label>
								<a href="" class="btn btn-danger fileupload-exists btn-md" data-dismiss="fileupload"><i class="fa fa-trash"></i>Remove</a>
								<br/><span class='text-danger error'>{{ $errors->first('image') }}</span>
							</div>
							<strong><small id="passwordHelpBlock" class="form-text text-muted">
								The image dimension must be 1110x400.
							</small></strong>
						</div>
					</div>
				</div>
        @include('layout.overlay')
        </div>
        <div class="text-right">
			<?= Form::submit('Save',['class'=>'btn btn-primary btn-sm','title'=>'Save Record'])?>
			<?=Form::submit('Save & New',['class'=>'btn btn-primary btn-sm','name'=>'save_new','title'=>'Save & Create New Record'])?>
			<a href="<?=URL::route('slider.index')?>" title='Back'><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
        </div>
    </div>
<?=Form::close()?>
@stop
@section('style')
	<?=Html::style('backend/css/positioner.css',[],IS_SECURE)?>
	<?= Html::style("backend/css/bootstrap-fileupload.css",[],IS_SECURE)?>
@stop
@section('script')
	<?= Html::script("backend/js/bootstrap-fileupload.js",[],IS_SECURE)?>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#is_active_true").click(function(){
				$("#position").show();
			});
			$("#is_active_false").click(function(){
				$("#position").hide();
			});
			if($("#is_active_false").is(":checked")){
				$("#position").hide();
			}
			else{
				$("#position").show();
			}
		});
	</script>
@include('layout.alert')
@stop