@extends('layout.layout')
@section('top_fixed_content')
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4>Slider with text Position - Edit {{$slider_data['title']}}</h4>
        </div>
    </nav>
@stop
@section('content')
<?=Form::model($slider_data,['route'=>['slider.update',$id],'role'=>'form','class'=>'m-0','files'=>true,'method'=>'PATCH'])?>
	<div class="card">
        <div class="card-body">
                <div class="form-group row">
                	<div class="col-sm-2 text-right">
                    	<?= Form::label('title','',['class'=>'control-label'])?><span class="text-danger"> *</span>
                    </div>
                    <div class="col-sm-8  @if($errors->has('title')) has-error @endif">
                    	<?= Form::text('title',null,['class'=>'form-control','id'=>'title','placeholder'=>'Enter Title','maxlength'=>'10'])?>
                    	<span class='text-danger error'>{{ $errors->first('title') }}</span>
                    </div>
                </div>
                <div class="form-group row">
			    	<label class="col-md-2">Enable/Disable Title</label>
				    <div class="col-md-10">
				        <div class="animated-radio-button pull-left mr-10">
				            <label for="is_active_true">
				                <input type="radio" name="enable_title" value="1" id="is_active_true" {{old('enable_title',$slider_data['enable_title']) == '1' ? 'checked' : ''}}>
				                <span class="label-text"></span> Enable
				            </label>
				        </div>
				        <div class="animated-radio-button pull-left">
				            <label for="is_active_false">
				                <input type="radio" name="enable_title" value="0" id="is_active_false" {{old('enable_title',$slider_data['enable_title']) == '0' ? 'checked' : ''}}>
				                <span class="label-text"></span> Disable
				            </label>
				        </div>
				    </div>
				</div>
				<div class="form-group row" id="position">
					<label for="name" class="col-sm-2 control-label">Title Position</label>
					<div class="col-md-8">
						<div class="slider-positioner">
							<div class="srow">
								<div class="position">
									<label>
										<input type="radio" name="position" value="top-left" @if(old('position',$slider_data['position']) == 'top-left') checked @endif>
										<div class="pos-box">TOP LEFT</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="top-center"@if(old('position',$slider_data['position']) == 'top-center') checked @endif>
										<div class="pos-box">TOP CENTER</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="top-right"@if(old('position',$slider_data['position']) == 'top-right') checked @endif>
										<div class="pos-box">TOP RIGHT</div>
									</label>
								</div>
							</div>
							<div class="srow">
								<div class="position">
									<label>
										<input type="radio" name="position" value="center-left"@if(old('position',$slider_data['position']) == 'center-left') checked @endif>
										<div class="pos-box">CENTER LEFT</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="center-center"@if(old('position',$slider_data['position']) == 'center-center') checked @endif>
										<div class="pos-box">CENTER CENTER</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="center-right"@if(old('position',$slider_data['position']) == 'center-right') checked @endif>
										<div class="pos-box">CENTER RIGHT</div>
									</label>
								</div>
							</div>
							<div class="srow">
								<div class="position">
									<label>
										<input type="radio" name="position" value="bottom-left"@if(old('position',$slider_data['position']) == 'bottom-left') checked @endif>
										<div class="pos-box">BOTTOM LEFT</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="bottom-center"@if(old('position',$slider_data['position']) == 'bottom-center') checked @endif>
										<div class="pos-box">BOTTOM CENTER</div>
									</label>
								</div>
								<div class="position">
									<label>
										<input type="radio" name="position" value="bottom-right"@if(old('position',$slider_data['position']) == 'bottom-right') checked @endif>
										<div class="pos-box">BOTTOM RIGHT</div>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-2 text-right">
						<?= Form::label('featured image','',['class'=>'control-label'])?><span class="text-danger"> *</span>
					</div>
					<div class="form-group col-sm-8">
						<div class="fileupload fileupload-new col-md-8" data-provides="fileupload">
							<div class="fileupload-new thumbnail" style="width: 300px; height: 136px;">
								@if(empty($slider_data['image']))
								<img src="https://via.placeholder.com/1110x400" alt="" style="width:100%;height:100%" />
								@else
								<img src="<?=LOCAL_IMAGE_PATH."/slider/".$slider_data['image']?>" alt="" style="width:100%;height:100%" />
								@endif
							</div>
							<div class="fileupload-preview fileupload-exists thumbnail " style="width: 300px; line-height: 20px;">
							</div>
							<div>
								<label class="btn btn-file control-label btn-xs">
									<?=Form::file('image', ['class' => 'control-label col-md-2'])?>
									<span id="" class="fileupload-new btn-lg"><i class="fa fa-paper-clip"></i>Select Image</span>
									<span id="" class="fileupload-exists btn-lg"><i class="fa fa-undo"></i>Change</span>
								</label>
								<a href="" class="btn btn-danger fileupload-exists btn-md" data-dismiss="fileupload"><i class="fa fa-trash"></i>Remove</a>
								<br/><span class='text-danger error'>{{ $errors->first('image') }}</span>
							</div>
							<strong><small id="passwordHelpBlock" class="form-text text-muted">
								The image dimension must be 1110x400.
							</small></strong>
						</div>
					</div>
				</div>
        @include('layout.overlay')
        </div>
        <div class="text-right">
            <?= Form::submit('Save',['class'=>'btn btn-primary btn-sm','name'=>'save','title'=>'Save Record'])?>
            <?= Form::submit('Save & Exit',['class'=>'btn btn-primary btn-sm','title'=>'Save & Exit'])?>
            <a href="<?=URL::route('slider.index')?>" title="Back"><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
        </div>
    </div>
<?=Form::close()?>
@stop
@section('style')
	<?=Html::style('backend/css/positioner.css',[],IS_SECURE)?>
	<?=Html::style("backend/css/bootstrap-fileupload.css",[],IS_SECURE) ?>
@stop
@section('script')
	<?= Html::script("backend/js/bootstrap-fileupload.js",[],IS_SECURE)?>
	<script type="text/javascript">
		$(document).ready(function(){
			if($("#is_active_false").is(":checked")){
				$("#position").hide();
			}
			else{
				$("#position").show();
			}
			$("#is_active_false").click(function(){
				$("#position").hide();
			});
			$("#is_active_true").click(function(){
				$("#position").show();
			});
		});
	</script>
@include('layout.alert')
@stop