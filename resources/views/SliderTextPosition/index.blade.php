@extends('layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
    <div class="title">
        <h4>Slider With text Position</h4>
    </div>
    <div class="pl-10">
        <a href="<?=route('slider.create')?>" class="btn btn-primary btn-sm" title="Add City">Add New</a>
    </div>
</nav>
@stop
@section('content')
<div class="row bannersection">
    <div class="col-md-12">
        <div class="card">
            <div class="row">
        	@foreach($slider_data as $data)
	            <div class="col-md-12" id="{{$data->id}}">
	                <div class="box blue-border icon-upper">
	                    <div class="box-header">
	                        <span class="controls pull-right">
	                            <a data-showloading="yes" href="<?=route('slider.edit',['id'=>$data->id])?>" type="button" class="control fa fa-pencil js__card_plus" title="Edit Slider"></a>
	                            <button href='javascript:void(0);' onclick="DeleteRecord(<?=$data->id?>)" type="button" class="control fa fa-minus js__card_remove remove-bac-color" title="Delete Slider"></button>
	                        </span>
	                    </div>
	                    <div class="box-body no-padding">
	                        <img class="img-responsive" src="<?= LOCAL_IMAGE_PATH."/slider/".$data->image?>">
                            @if($data['enable_title'] == '1')
                                <div class="banner-con <?= $data['position']?>">
                                    <h4>{{$data['title']}}</h4>
                                </div>
                            @endif
	                    </div>
	                </div>
	            </div>
            @endforeach  
            </div>
        </div>
    </div>
</div>
@stop
@section('style')
    <style type="text/css">
        .box.blue-border.icon-upper .box-header{
            background-color: #ebeef0;
            float: left;
            width: 100%;
            padding: 10px
        }
        .box.blue-border.icon-upper .box-header a{
            color: #0078bd;
        }
        .box.blue-border.icon-upper .box-header button{
            background-color: transparent;
            border: medium none;
            color: #0078bd;
        }
        .box.blue-border.icon-upper .box-body.no-padding {
            float: left;
            position: relative;
        }
    </style>
	<?= Html::style('backend/css/jquery-ui.min.css', [], IS_SECURE) ?>
	<?= Html::style('backend/css/jquery-ui.structure.min.css', [], IS_SECURE)?>
	<?= Html::style('backend/css/jquery-ui.theme.min.css', [], IS_SECURE) ?>
	<?= Html::style('backend/css/sweetalert.min.css', [], IS_SECURE) ?>
    <?= Html::style('frontend/css/slidertextposition.css', [], IS_SECURE) ?>
@stop
@section('script')
    <?=Html::script('backend/js/jquery.min.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/sweetalert.min.js', [], IS_SECURE)?>
    <script type="text/javascript">
    	var token = "<?=csrf_token()?>";
    function DeleteRecord(id){
        swal({
            title: "Are you sure to delete selected record(s)?",
            text: "It will delete this Slider record and can not be reverted",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, Cancel it!",
            closeOnConfirm: true,
            closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: "{{ URL::route('slider.delete')}}",
                        type:'post',
                        dataType:'json',
                        data:{ id : id,_token: token},
                        beforeSend:function(){
                            $('#spin').show();
                            window.location.reload();

                        },
                        complete:function(){
                            $('#spin').hide();
                        },
                        success: function (respObj) {
                            if(respObj){
                                toastr.success('Slider Successfully Deleted','Success!');
                                window.location.reload();
                            }
                        }
                    });
                }
            });
    }
    </script>
@include('layout.alert')
@stop