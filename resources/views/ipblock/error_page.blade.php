<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Error page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

	<style type="text/css">
	.error-template {padding: 40px 15px;text-align: center;}
	.error-actions {margin-top:15px;margin-bottom:15px;}
	.error-actions .btn { margin-right:10px; }
	</style>

  </head>
  <body>
  	<div class="container">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="error-template">
	                 <h1>Access denied</h1>
	                <div class="error-details">
	                	<h3>
		                    This request was blocked by the security rules. <br>
		                    Kindly contact with system administrator.
	                	</h3>
	                </div>

	            </div>
	        </div>
	    </div>
	</div>
   
		
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
