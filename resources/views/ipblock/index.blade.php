@extends('layout.layout')
@section('top_fixed_content')
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>
        <div class="text-right mb">
            <a href="javascript:void(0)" class="btn btn-primary btn-sm" title="Add Ipaddress" data-toggle="modal" onclick="AddIpdata()">Add Ipaddress</a>
        </div>
    </nav>
@stop

@section('style')
<?= Html::style('backend/css/toastr.min.css') ?>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card p-0">
            <div class="card-body table-responsive">
                <ul class="nav nav-tabs">
                    <li @if((Request::get('type')) == 'whitelist') class="active" @endif>
                        <a href="<?=URL::route('ipblock.index',['type'=>'whitelist'])?>">Whitelist</a>
                    </li>
                    <li  @if((Request::get('type')) == 'blacklist') class="active" @endif>
                        <a href="<?=URL::route('ipblock.index',['type'=>'blacklist'])?>">Blacklist</a>
                    </li>
                </ul>
                <br>
                <div class="text-right">
                    <div class="number-delete">
                        <ul>
                            <li>
                                <p class="mb-0"><span class="num_item"></span>Item Selected.</p>
                            </li>
                            <li class="bulk-dropdown"><a href="javascript:;">Bulk actions<span class="caret"></span></a>
                                <div class="bulk-box">
                                    <div class="bulk-tooltip"></div>
                                    <ul class="bulk-list">
                                        <li><a href="javascript:void(0);" id="delete" class="delete-btn" title="Delete Single Or Multiple brands">Delete Selected Ip</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="p-20">
                    <table class="table" id="ipaddress_table">
                    <thead>
                        <tr>
                            <th class="select-all no-sort"><div class="animated-checkbox"><label class="m-0"><input type="checkbox" id="checkAll" /><span class="label-text"></span></label></div></th>
                            <th></th>
                            <th>Ip address</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    </table>
                </div>
            </div>
            @include('layout.overlay')
        </div>
    </div>
</div>
<!--Start Add Ipaddress Form -->
<div id="add_data" class="modal fade">
<div class="modal-dialog">
    <div class="modal-content">
        <form id="status_form_data" method="post">
        {{csrf_field()}}
        <input type="hidden" name="id" id="id" value="/">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="m-0 header_title">Add Ipaddress</h4>
        </div>
        <div class="modal-body relative">
            <div class="bg-white p-10">
                <div class="form-group @if($errors->has('ip_address')) {{ 'has-error' }} @endif">
                    <label>IP<sup class="text-danger">*</sup></label>
                    <?=Form::text('ip_address', null, ['class' => 'form-control', 'placeholder' => 'Enter Ip Address']);?>
                    <span id="ip_address_error" class="text-danger error"><?=$errors->first('ip_address')?></span>
                </div>
                <div class="form-group row">
                    <label class="col-md-2">Status</label>
                    <div class="col-md-10">
                        <div class="animated-radio-button pull-left mr-10">
                            <label for="whitelist">
                                <?=Form::radio('status', '1', true, ['id' => 'whitelist'])?>
                                <span class="label-text"></span> Whitelist
                            </label>
                        </div>
                        <div class="animated-radio-button pull-left">
                            <label for="blacklist">
                                <?=Form::radio('status', '0', false, ['id' => 'blacklist'])?>
                                <span class="label-text"></span> Blacklist
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">  
            <div class="text-right" style="margin-right:15px">
                <button type="submit" id="save_data" name="save_status" value="save_new" class="btn btn-primary btn-sm disabled-btn">Save</button>
                <a href="javascript:void(0);" onclick="closeModel()" data-dismiss="modal" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</a>
            </div>
            @include('layout.overlay')
        </div>          
        <?= Form::close() ?>
    </div>
</div>    
</div>
<!-- End Ipaddress Form -->
@stop

@section('script')
    <?=Html::script('backend/js/jquery.dataTables.min.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/dataTables.bootstrap.min.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/jquery.form.min.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/fnstand.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/sweetalert.min.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/delete_script.js', [], IS_SECURE)?>

     <script type="text/javascript">

        var table = "ipaddress_table";
        var title = "Are you sure to Delete this Record?";
        var text = "You will not be able to recover, once deleted";
        var type = "warning";
        var token = "<?=csrf_token()?>";
        var delete_path = "<?=route('ipblock.delete')?>";
        var page_type = "<?=Request::get('type')?>";

        if(page_type == 'whitelist')
        {
           var url =  "<?=URL::route('ipblock.index',['type'=>'whitelist'])?>";
        }
        else
        {
            var url =  "<?=URL::route('ipblock.index',['type'=>'blacklist'])?>";
        }
        $(function(){
            $('#delete').click(function(){
                var delete_id = $('#'+table+' tbody input[type=checkbox]:checked');
                checkLength(delete_id);
            });
            var ipaddress_table = $('#'+table).dataTable({
                "bProcessing": false,
                "bServerSide": true,
                "autoWidth": false,
                "sAjaxSource": url,
                "aaSorting": [[ 1, "asc" ]],
                "orderSequence":["asc"],
                "aoColumns":[
                    {
                        mData: "id",
                        bSortable:false,
                        sWidth:"2%",
                        sClass:"text-center",
                        mRender: function (v, t, o) {
                            return '<div class="animated-checkbox"><label class="m-0"><input class="checkbox" type="checkbox" id="chk_'+v+'" name="special_id[]" value="'+v+'"/><span class="label-text"></span></label></div>';
                        },
                    },
                    {
                        mData: "id",
                        bSortable:false,
                        sWidth:"10%",
                        sClass:"text-center",
                        bVisible:false,
                        mRender: function (v, t, o) {
                            return '<div class="animated-checkbox"><label class="m-0"><input class="checkbox" type="checkbox" id="chk_'+v+'" name="id[]" value="'+v+'"/><span class="label-text"></span></label></div>';
                        },
                    },
                    {mData:"ip_address",bSortable:true,sClass:"text-left",sWidth:"10%"},
                    
                    {
                    	mData:"whitelisted",
                    	bSortable : true,
                    	sClass : 'text-left',
                    	sWidth:"10%",
                    	mRender: function(v, t, o) {
                            
                    		if(v == '1'){
                                var act_html  = '<span class="badge bg-info">Whitelist</span>'
                            }
                            else{
                                var act_html  = '<span class="badge bg-default">Blackllist</span>'
                            }

                            return act_html;
                        }
                    },
                    {
                    	mData:"whitelisted",
                    	bSortable : true,
                    	sClass : 'text-left',
                    	sWidth:"10%",
                    	mRender: function(v, t, o) {
                    		if(v == '1')
                    		{
                            	return "<button onclick='ChangeStatus("+o.id+","+v+")' class='btn btn-danger btn-sm' style='font-size: 11px;padding: 6px;'>Blackllist</button>";
                    		}else{
                    			return "<button onclick='ChangeStatus("+o.id+","+v+")' class='btn btn-success btn-sm' style='font-size: 11px;padding: 6px;'>Whitellist</button>";
                    		}
                           
                        }
                    },
                ],
                fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
                fnDrawCallback : function (oSettings) {
                    $("div.overlay").hide();
                }
            });
        });
        function AddIpdata()
        {
            $("[id$='_error']").empty();
            $('#status_form_data')[0].reset();
            $('#add_data').modal('show');
        }
        $('#save_data').click(function(e,ele)
        {
            $(".overlay").show();
            var method_type = 'POST';
            url = "<?= URL::route('ipblock.store') ?>";

            $('#status_form_data').ajaxSubmit({
                url: url,
                type: method_type,
                data: { "_token" : token },
                dataType: 'json',
                
                beforeSubmit : function()
                {
                   $('.disabled-btn').attr('disabled',true);
                   $("[id$='_error']").empty();
                },
                
                success : function(resp)
                {                  
                    $(".overlay").hide();
                    $('.disabled-btn').attr('disabled',false);
                    if (resp.success == true) {

                        $('#add_data').modal('hide');
                        $('#status_form_data')[0].reset();
                        toastr.success(resp.message);
                    }
                    var redrawtable = $('#'+table).dataTable();
                    redrawtable.fnStandingRedraw();

                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                   
                },
                error : function(respObj){ 
                    $.each(respObj.responseJSON.errors, function(k,v){
                        $('#'+k+'_error').text(v);
                    });
                    $('.disabled-btn').removeAttr('disabled','enabled');
                    $(".overlay").hide();
                }
            });
            return false;
        });
        function ChangeStatus(id,status) {

        	var title = "Are you sure to change the status?";
          	var action_path = "<?=route('ipblock.changestatus')?>";
           	swal({
                title: title,
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, Change Status!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
                    if (isConfirm) {
                    updateStatus(action_path,id,token,status);
                    }
            });
        }
        function updateStatus(act_path,id,token,status)
        {
            $.ajax({
                type: "GET",
                url: act_path,
                data: {'id':id, 'status':status,'token':token},
                success: function (data) {
                        if(data.success == true)
                        {
                            toastr.success(data.message,{
                                        "showDuration":"1000",
                                        "timeOut" : "8000",
                                        "iconClass" : "toast toast-success",
                                        "closeButton": true,
                                        "positionClass": "toast-top-right"
                                        });
                        }
                      
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
            });
        }
    </script>
    @include('layout.alert')
@stop
