@extends('layout.layout')
@section('start_form')
    <?= Form::model($edit_exist_employee,['route'=>['dynamicform.update',$edit_exist_employee->id],'role'=>'form','method'=>'PATCH','class'=>'m-0']) ?>
@stop

@section('top_fixed_content')
    <nav class="navbar navbar-static-top">
        <div class="title">
             <h4>Edit</h4>
        </div>
    </nav>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('name')) {{ 'has-error' }} @endif">
                            <input type="hidden" name="id" value="<?= $edit_exist_employee->id ?>">
                            <label>Name <sup class="text-danger">*</sup></label>
                            <?= Form::text('name',old('name'),['class' => 'form-control','placeholder'=>'Name']); ?>
                            <span id="name_error" class="help-inline text-danger"><?= $errors->first('name') ?></span>
                        </div>                       
                    </div>
                    <div class="col-md-6">    
                        <div class="form-group @if($errors->has('email')) {{ 'has-error' }} @endif">
                            <label>Email <sup class="text-danger">*</sup></label>
                            <?= Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Pincode']); ?>
                            <span id="email_error" class="help-inline text-danger"><?= $errors->first('email') ?></span>
                         </div>
                    </div>  
                </div>
            </div>
            @include('layout.overlay')
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-title-w-btn">
                <h4 class="title">Shipping Address</h4>
            </div>
            <hr>
            <div class="card-body">
                <div class="form-group">
                    <table class="table m-0 v-top">
                        <thead>
                            <tr>
                                <th style="border:none">Country<sup class="text-danger">*</sup></th>
                                <th style="border:none">State<sup class="text-danger">*</sup></th>
                                <th style="border:none">City<sup class="text-danger">*</sup></th>
                                <th style="border:none"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="shipping">
                                <input type="hidden" name="shipping_id" id="shipping_id">
                                <td class="col-md-2" style="border:none">
                                    <?= Form::select('country',$country_list,old('country'), ['class' => 'form-control select2 country','id'=>'country']); ?>
                                    <span id="country_error" class="help-inline text-danger"><?= $errors->first('shipping.shipping.*.country') ?></span>
                                </td>
                                <td class="col-md-2" style="border:none">
                                 <?= Form::select('state',[''=>'Select State'],old('state'),['class' => 'form-control select_state','id'=>'state']); ?>
                                    <span id="state_error" class="help-inline text-danger"><?= $errors->first('shipping.shipping.*.state') ?></span>
                                </td>
                                <td class="col-md-2" style="border:none">
                                    <?= Form::select('city',[''=>'Select City'],old('city'), ['class' => 'form-control select2 select_city','id'=>'city']); ?>
                                    <span id="city_error" class="help-inline text-danger"><?= $errors->first('shipping.shipping.*.city') ?></span>
                                </td>
                                <td class="col-md-1" style="border:none">
                                    <a id="shipping_remove" class="pt-10 pull-left btn-remove"><i class="fa fa-minus-circle fa-small pull-left"></i></a>
                                    <a id="shipping_add" class="pt-10 pull-left btn-add"><i class="fa fa-plus-circle fa-small pull-left" ></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                 </div>
            </div>
        </div>
        @include('layout.overlay')
    </div>
</div>         
<div class="text-right form-group">
    <button type="submit" name="save_button" value="save" class="btn btn-primary btn-sm disabled-btn" title="Update <?= $edit_exist_employee->name ?>">Save</button>
    <a href="<?= URL::route('dynamicform.index') ?>" class="btn btn-default btn-sm" title="Back to Customer Page">Cancel</a>
</div>
@stop

@section('end_form')
<?= Form::close() ?>
@stop

@section('script')
<?= Html::script('backend/plugins/dynamicform/dynamicform.js',[],IS_SECURE) ?>
<?= Html::script('backend/plugins/select2/js/select2.full.min.js',[],IS_SECURE) ?>
    
    <script type="text/javascript">
        // $(document).ready(function(){
        // });
    
        function getCountry(country){
            var result = '';

            $.ajax({
                url: "<?= URL::route('dynamicform.getstate') ?>",
                type: 'post',
                dataType: 'json',
                async : false,
                data: {
                    country: country,
                    _token:'<?= csrf_token()?>'
                },
                beforeSend: function() {
                    $('div.overlay').show();
                },
                complete: function() {
                    $('div.overlay').hide();
                },
                success: function(resp) {
                    result = resp.data;
                    // $('.select_state').closest('td').html('').select2({'data':resp.data});
                    // var state = $('.select_state').closest('td').attr('id');
                }, 
            });

            return result;
        }

        $('body').on('change','.country',function (){
            var select_html  = $(this).parent('td').parent('tr').find('.select_state');
            var country = $(this).val();
            resp = getCountry(country);
            select_html.html('').select2({'data':resp});
        });

        $('body').on('change','.select_state',function (){
            var select_html  = $(this).parent('td').parent('tr').find('.select_city');
            var state = $(this).val();
            $.ajax({
                url: "<?= URL::route('dynamicform.getcity') ?>",
                type: 'post',
                dataType: 'json',
                async : false,
                data: {
                    state: state,
                    _token:'<?= csrf_token()?>'
                },
                beforeSend: function() {
                    $('#spin').show();
                },
                complete: function() {
                    $('#spin').hide();
                },
                success: function(resp) {
                   select_html.html('').select2({'data':resp.data});
                },
            });
        });

        var customer_shipping_detail =  $("#shipping").dynamicForm("#shipping_add", "#shipping_remove", {
            limit: 10,
            normalizeFullForm : false,
        });

        var old_data = <?= json_encode(old('shipping.shipping',$employee_info)) ?>;

        customer_shipping_detail.inject(old_data);

        var select_list = $('tbody tr');

        $.each(select_list,function(k,v){
            var country = old_data[k]['country'];
            var country_test = getCountry(country);
            $(v).find('.select_state').html('').select2({'data':country_test}).select2('val',old_data[k]['state']);
            $(v).find('.select_city').select2('val',old_data[k]['city']);
        });

        //console.log(customer_shipping_detail.inject);
        @if($errors)
            var detail_Errors = <?= json_encode($errors->toArray()) ?>;
            
            $.each(detail_Errors, function(id,msg){
                var id_arr = id.split('.');
                if (id_arr[3] == 'city') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'state') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'country') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
            });
        @endif

        $('.country').select2({
            placeholder : "Select Country"
        });
        $('.select_state').select2({
            placeholder : "Select State"
        });
        $('.select_city').select2({
            placeholder : "Select City"
        });
    </script>
    @include('layout.alert')
@stop