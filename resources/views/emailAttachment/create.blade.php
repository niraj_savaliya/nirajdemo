@extends('layout.layout')

@section('content')	
<?= Form::open(['method'=>'POST','route'=>'email.attachment.store','class'=>'form-horizontal','files'=>true])?>
			<h2 align="center">Mail Sending</h2>
			<div class="form-group  @if($errors->has('type')) has-error @endif">
				<label class="col-md-3 control-label">To</label>
				<div class="col-md-8">
					<select class="select2_2 form-control" id="to_mail" name="to[]" multiple="multiple"></select>
					<span class='text-danger error'>{{ $errors->first('to') }}</span>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">CC</label>
				<div class="col-md-8">
					<select class="select2_2 form-control" id="cc_mail" name="cc[]" multiple="multiple"></select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-md-3 control-label">Bcc</label>
				<div class="col-md-8">
					<select class="select2_2 form-control" id="bcc_mail" name="bcc[]" multiple="multiple"></select>
				</div>
			</div>
			<div class="form-group @if($errors->has('type')) has-error @endif">
				<label class="col-md-3 control-label">Subject</label>
				<div class="col-md-8">
					<?= Form::text('subject',old('subject'),['class'=>'form-control', 'placeholder'=>'please enter Subject']) ?>
					<span class='text-danger error'>{{ $errors->first('subject') }}</span>
				</div>
			</div>
			<div class="form-group @if($errors->has('type')) has-error @endif">
				<label class="col-md-3 control-label">Message</label>
				<div class="col-md-8">
					<?= Form::textarea('textmessage',old('address'),['class'=>'form-control', 'placeholder'=>'compose mail']) ?>
					<span class='text-danger error'>{{ $errors->first('textmessage') }}</span>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Attach File</label>
				<div class="col-md-8">
					<input type="file" name="attachment[]" class="form-control" multiple="multiple" >
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3" style="margin-left: 13px;"></div>
					<?= Form::submit('Send',['class'=>'btn btn-primary','name'=>'sendemail'])?>
				</div>
			
			<?= Form::close()?>
		
	</body>
</html>
</html>
@stop
@section('style')
<?= Html::style('backend/js/toastr-master/toastr.min.css')?>
@stop
@section('script')


<?= Html::script('backend/js/select2.min.js') ?>
<script type="text/javascript">
	var to_mail = $('#to_mail').select2({
		placeholder : "enter mail id"
	});
	@if(old('to'))
        var to = {!! json_encode(old('to')) !!};
        
        $.each(to,function(k,v){
            $("#to_mail").append($('<option>', {value: v, text: v}));
        })
        to_mail.val(to).trigger('change');
    @endif
  </script>
  <script type="text/javascript">  
    var cc_mail = $('#cc_mail').select2({
		placeholder : "enter mail id"
	});
	@if(old('cc'))
        var to = {!! json_encode(old('cc')) !!};
        
        $.each(to,function(k,v){
            $("#cc_mail").append($('<option>', {value: v, text: v}));
        })
        cc_mail.val(to).trigger('change');
    @endif	
  </script>
  <script type="text/javascript">  
    var cc_mail = $('#bcc_mail').select2({
		placeholder : "enter mail id"
	});
	@if(old('bcc'))
        var to = {!! json_encode(old('bcc')) !!};
        
        $.each(to,function(k,v){
            $("#bcc_mail").append($('<option>', {value: v, text: v}));
        })
        cc_mail.val(to).trigger('change');
    @endif

</script>

	<?= Html::script('backend/js/form.demo.min.js') ?>
	<?= Html::script('backend/js/toastr-master/toastr.min.js') ?>
	
	

@if ($errors->any())

<script type="text/javascript">
	$(document).ready(function(){
        toastr.error
        ("<b>There were some errors.</b>");
	});
</script>
@endif
@include('layout.alert')
@stop