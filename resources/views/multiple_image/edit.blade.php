@extends('layout.layout')
@section('style')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
@stop
<?=Form::model($event_data, ['method' => 'PATCH', 'route' => ['multiple-image.update', $event_data->id], 'files' => true])?>
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
       <h4> </h4>
    </div>
    <div class="text-right mb">
       <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save & Exit">Save</button>
        <a href="<?=URL::route('multiple-image.index')?>" class="btn btn-border btn-sm disabled-btn" title="Cancel">Cancel</a>
    </div>
</nav>
@stop
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <br>
                <div class="form-group @if($errors->has('title')) {{ 'has-error' }} @endif">
                    <label class="col-md-2">Name<sup class="text-danger">*</sup></label>
                    <div class="col-md-10">
                    <?=Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title']);?>
                    <span id="title_error" class="text-danger"><?=$errors->first('title')?></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <br>
            <br>
            <div class="col-md-10">
                <div class="form-group @if($errors->has('files')) {{ 'has-error' }} @endif">
                    &nbsp;<label>Image (1500*500) (Select 4 images)<sup class="text-danger">*</sup></label>
                    <input type="file" name="files" id="fileupload" data-fileuploader-files='<?=$image_data?>'>
                    <span id="files_error" class="text-danger"><?=$errors->first('files')?></span>
                </div> 
            </div>
        </div>
    </div>
</div>
<div class="text-right">
   <button type="submit" name="save_button" id="savesubmit" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save & Exit">Save</button>
    <a href="<?=URL::route('multiple-image.index')?>" class="btn btn-border btn-sm disabled-btn" title="Cancel">Cancel</a>
</div>
<?=Form::close()?>
@stop
@section('script')
<?=Html::script('backend/js/select2.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/jquery.fileuploader.min.js', [], IS_SECURE)?>

<script type="text/javascript">

    /*$("button[type='submit']").click(function(){
       
        var no_of_photos=$("ul.fileuploader-items-list li").length - 1;

        if(no_of_photos <  4 ){
         
          alert("Please upload 4 photo");
          return false;
        }
     });*/

    var token = "{{ csrf_token() }}";
    // enable fileuploader plugin
    $(document).ready(function() {
        // enable fileuploader plugin
        $('input[name="files"]').fileuploader({
            extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
            changeInput: ' ',
            theme: 'thumbnails',
            enableApi: true,
            addMore: true,
            limit:5,
            minFilesize: 4,

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                          '<ul class="fileuploader-items-list">' +
                              '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner">+</div></li>' +
                          '</ul>' +
                      '</div>',
                item: '<li class="fileuploader-item">' +
                           '<div class="fileuploader-item-inner">' +
                               '<div class="thumbnail-holder">${image}</div>' +
                               '<div class="actions-holder">' +
                                   '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
                               '</div>' +
                               '<div class="progress-holder">${progressBar}</div>' +
                           '</div>' +
                       '</li>',
                item2: '<li class="fileuploader-item">' +
                           '<div class="fileuploader-item-inner">' +
                               '<div class="thumbnail-holder">${image}</div>' +
                               '<div class="actions-holder">' +
                                   '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
                               '</div>' +
                           '</div>' +
                       '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'

                },
                onItemShow: function(item, listEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input');

                    plusInput.insertAfter(item.html);

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                }
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = $.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },
             onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {

                name = item.name;
                var id = "<?=Request::segment(4)?>";

                $.ajax({
                    url: "<?=route('multiple-image.image.delete')?>",
                    type: "post",
                    data: { "_token" : token ,"name" : name,"event_id":id},
                    dataType: 'json',
                    beforeSubmit : function()
                    {
                        $('.disabled-btn').attr('disabled',true);
                        $("[id$='_error']").empty();
                    },
                    success : function(resp)
                    {
                        if (resp.success == true) {
                            toastr.success('Image delete successfully');
                            window.location.reload();
                        }
                    },
                });
                return false;
            },
        });
    });
</script>
@include('layout.alert')
@stop