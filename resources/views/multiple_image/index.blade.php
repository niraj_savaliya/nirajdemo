@extends('layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
       <h4><i class="fa fa-calendar"></i> Event</h4>
    </div>
    <div class="pl-10">
        <a href="<?=route('multiple-image.create')?>" class="btn btn-primary btn-sm" title="Add Event" > Add Event</a>
    </div>
</nav>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body table-responsive">
                <div class="text-right">
                    <div class="number-delete">
                        <ul>
                            <li>
                                <p class="mb-0"><span class="num_item"></span>Item Selected.</p>
                            </li>
                            <li class="bulk-dropdown"><a href="javascript:;">Bulk actions<span class="caret"></span></a>
                                <div class="bulk-box">
                                    <div class="bulk-tooltip"></div>
                                    <ul class="bulk-list">
                                        <li><a href="javascript:void(0);" id="delete" class="delete-btn" title="Delete Single Or Multiple Event">Delete Selected Event</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <table class="table" id="event_table">
                    <thead>
                        <tr>
                            <th class="select-all no-sort"><div class="animated-checkbox"><label class="m-0"><input type="checkbox" id="checkAll" /><span class="label-text"></span></label></div></th>
                            <th></th>
                            <th>Event Title</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
    <?=Html::script('backend/js/jquery.dataTables.min.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/dataTables.bootstrap.min.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/jquery.form.min.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/fnstand.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/delete_script.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/sweetalert.min.js',[],IS_SECURE)?>

    <script type="text/javascript">
        var table = "event_table";
        var title = "Are you sure to Delete this Record?";
        var text = "You will not be able to recover, once deleted";
        var type = "warning";
        var delete_path = "<?=route('multiple.delete')?>";
        var token = "<?=csrf_token()?>";

        $('[data-toggle="tooltip"]').tooltip();

        $(function(){
            $('#delete').click(function(){
                var delete_id = $('#'+table+' tbody input[type=checkbox]:checked');
                checkLength(delete_id);
            });
            event_table = $('#'+table).dataTable({
                "bProcessing": false,
                "bServerSide": true,
                "autoWidth": false,
                "sAjaxSource": "<?=URL::route('multiple-image.index')?>",
                "aaSorting": [[ 2, "desc" ]],
                "aoColumns":[
                    {
                        mData: "id",
                        bSortable:false,
                        sWidth:"2%",
                        sClass:"text-center",
                        mRender: function (v, t, o) {
                            return '<div class="animated-checkbox"><label class="m-0"><input class="checkbox" type="checkbox" id="chk_'+v+'" name="special_id[]" value="'+v+'"/><span class="label-text"></span></label></div>';
                        },
                    },
                    {
                        mData: "creation_date",
                        bVisible:false,
                    },
                    {
                        mData: "title",
                        bSortable:true,
                        sWidth:"25%",
                        sClass:"text-left",
                        mRender: function(v, t, o) {
                            var edit_path = "<?=URL::route('multiple-image.edit', array(':id'))?>";
                            edit_path = edit_path.replace(':id',o['id']);

                            var act_html  = "<a title='Edit "+o['title']+"' href='"+edit_path+"'>"+ o['title'] +"</a>"

                            return act_html;
                        }
                    },
                ],
                fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
                fnDrawCallback : function (oSettings) {
                    $("div.overlay").hide();
                }
            });
        });
    </script>
    @include('layout.alert')
@stop