@extends('layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-list"></i>User</h4>
    </div>
    <div class="top_filter"></div>
    <div class="pl-10">
    
    <?=Form::open(['route' => ['users.import'], 'files' => true])?>   
    <div>
        <div class="fileUpload btn btn-primary btn-sm">
            <label>Import</label>
            <?=Form::file('sample_file', ['class' => 'form-control upload','id'=>'uploaded_file'])?>
        </div>
        <input id="uploaded" hidden="hidden" />
        <button type="submit" name="save_button" id="upload" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save & New" hidden="hidden">Submit</button>
        </div>

    </div>
    <?=Form::close()?>
    
    <div class="pl-10">
        <a href="<?=route('export.download');?>" class="btn btn-primary btn-md" title="Export user">Download Template</a>
    </div>
    
</nav>
@stop
@section('content')
<style type="text/css">
    .no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.fileUpload {
    position: relative;
    overflow: hidden;
    margin: 0px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
#uploaded{
    border: none;
}
</style>
<div class="row">
<div class="col-md-12">
    <div class="card">
        <div class="card-body table-responsive">
            <div class="text-right">
                <div class="number-delete">
                    <ul>
                        <li>
                            <p class="mb-0"><span class="num_item"></span>Item Selected.</p>
                        </li>
                        <li class="bulk-dropdown"><a href="javascript:;">Bulk actions<span class="caret"></span></a>
                            <div class="bulk-box">
                                <div class="bulk-tooltip"></div>
                                <ul class="bulk-list">
                                    <li><a href="javascript:void(0);" id="delete" class="delete-btn">Delete selected User</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            <table id="institute_table" class="table">
                <thead>
                    <tr>
                        <th class="select-all no-sort">
                            <div class="animated-checkbox">
                                <label class="m-0">
                                    <input type="checkbox" id="checkAll"/>
                                    <span class="label-text"></span>
                                </label>
                            </div>
                        </th>
                        <th>User Name</th>
                        <th>Email Address</th>
                        <th>Contact</th>
                        <th>Active</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        @include('layout.overlay')
    </div>
</div>
<div id="error_data" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close" class="close" data-dismiss="modal"  >&times;</button>
                    <h4 class="m-0 header_title">Error list</h4>
            </div>
            <div class="modal-body" >
                <div class="card bg-gray">
                    <div class="row" id="error_show">
                    </div><hr>
                    <?=Form::open(['route' => ['users.export'],'method'=>'post'])?> 
                      <input value="" hidden="hidden" id="download_data" name="download_data">
                      <button type="submit" name="save_button" id="upload" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Show errors">Download error file</button>
                    <?=Form::close()?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@stop
@section('script')
<?= Html::script('backend/js/jquery.dataTables.min.js') ?>
<?= Html::script('backend/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('backend/js/fnstandrow.js') ?>
<?= Html::script('backend/js/delete_script.js') ?>
<?= Html::script('backend/js/jquery.form.min.js') ?>

<script type="text/javascript">
    $('.visible').hide();
    var table = "institute_table";
    var title = "Are you sure to delete this User?";
    var text = "You will not be able to recover this record";
    var type = "warning";
    var delete_path = "<?=route('import.delete');?>";
    var token = "<?= csrf_token() ?>";

    var datatable_url = "<?=route('import.index');?>";

    $(function(){
        
        $('#delete').click(function(){
            var delete_id = $('#'+table+' tbody input[type=checkbox]:checked');
            checkLength(delete_id);
        });

        institute_table = $('#'+table).dataTable({
            "bProcessing": false,
            "bServerSide": true,                              
            "autoWidth": false,
            "sAjaxSource": datatable_url,
            "aaSorting": [ 1,"desc"],
            "aoColumns": [

            {
                mData: "id",
                bSortable:false,
                sWidth:"2%",
                sClass:"text-center",
                mRender: function (v, t, o) {
                    return '<div class="animated-checkbox"><label><input class="checkbox" type="checkbox" id="chk_'+v+'" name="special_id[]" value="'+v+'"/><span class="label-text"></span></label></div>';
                },
            },
            {
                mData:'name',
                bSortable:true,
                sWidth:"10%",
                sClass:"text-center",
                
            },
            {   
                mData:'email',
                bSortable:false,
                sWidth:"10%",
                sClass:"text-center",

            },
            {   
                mData:'mobile',
                bSortable:false,
                sWidth:"10%",
                sClass:"text-center",
            },
            {   
                mData:"is_active",
                bSortable : false,
                sClass : 'text-center',
                sWidth:"10%",
                mRender: function (v, t, o) {
                    var active_string;
                    if (v == '1') {
                        active_string = "<span class='badge bg-info'>Yes</span>";
                    }else{
                        active_string = "<span class='badge bg-default'>No</span>";
                    }
                    return active_string;
                },
            },
            ],
            fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
            fnDrawCallback : function (oSettings) {
                $("div.overlay").hide();
            }
        });
        
    });

    $('#uploaded_file').on('change', function(){
    $("#upload").show();
    $('#uploaded').show();
    })
</script>
@if(Session::has('get_html'))
    <script type="text/javascript">
        $(document).ready(function(){
         var download_data = '<?= Session::get("download_data") ?>';
         var get_html = '<?= Session::get("get_html") ?>';
         $('#error_show').html(get_html);
         toastr.error('There were some errors',{
                    "showDuration":"100",
                    "timeOut" : "800",
                    "iconClass" : "toast toast-success",
                    "closeButton": true,
                    "positionClass": "toast-top-right"
                    });
            setTimeout(function() {
              $('#error_data').modal('show');
              $('#download_data').val(download_data);
            }, 3000);
            setTimeout(function() {
              $('#error_data').modal('hide');
               var redrawtable = $('#'+table).dataTable();
                redrawtable.fnStandingRedraw();
                location.reload();
            }, 15000);
        });
    </script>
@endif
<script type="text/javascript">
$(function(){
    document.getElementById("uploaded_file").onchange = function () {
    document.getElementById("uploaded").value = this.value;
                };
            });
                </script>

@include('layout.alert')
@stop