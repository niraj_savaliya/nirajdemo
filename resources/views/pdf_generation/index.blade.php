@extends('layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
    <div class="title">
        <h4><h4>PDF Generation</h4></h4>
    </div>
    <div class="pl-10">
        <button type="button" name="download" onclick="generate_pdf();" class="btn btn-primary btn-sm" title="Download pdf" data-toggle="modal">download</button>
    </div>
</nav>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body table-responsive">
                <div class="text-right">
                    <div class="number-delete">
                        <ul>
                            <li>
                                <p class="mb-0"><span class="num_item"></span>Item Selected.</p>
                            </li>
                            <li class="bulk-dropdown"><a href="javascript:;">Bulk actions<span class="caret"></span></a>
                                <div class="bulk-box">
                                    <div class="bulk-tooltip"></div>
                                    <ul class="bulk-list">
                                        <li><a href="javascript:void(0);" id="delete" class="delete-btn" title="Delete Single Or Multiple City">Delete</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <table class="table" id="city_table">
                    <thead>
                        <tr>
                            <th class="select-all no-sort"><div class="animated-checkbox"><label class="m-0"><input type="checkbox" id="checkAll" /><span class="label-text"></span></label></div></th>
                            <th></th>
                            <th>Employee Name</th>
                            <th>Country</th>
                            <th>State</th>
                            <th>City</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!--Start otp verify Form -->
<div id="generate_pdf" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
             <?=Form::open(['route'=>'download.pdf','method'=>'post','id'=>'PDF_form'])?>
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"  onclick="closeModel()">&times;</button>
                    <h4 class="m-0 header_title">Download PDF</h4>
                </div>
                <div class="modal-body relative">
                    <div class="bg-white p-10">
                        <input type="hidden" name="total_record" value="<?=$total_record?>">
                        <div class="form-group row">
                            <label class="col-md-6">Generate custom pdf?</label>
                            <div class="col-md-6">
                                <div class="animated-radio-button pull-left mr-10">
                                    <label for="is_custome_true">
                                        <?=Form::radio('custome', 1, null, ['id' => 'is_custome_true','class'=>'custom_pdf'])?>
                                        <span class="label-text"></span> Yes
                                    </label>
                                </div>
                                <div class="animated-radio-button pull-left">
                                    <label for="is_custome_false">
                                        <?=Form::radio('custome', 0, true, ['id' => 'is_custome_false','class'=>'custom_pdf'])?>
                                        <span class="label-text"></span> No
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group @if($errors->has('total_number')) {{ 'has-error' }} @endif row enter_record"><br>
                            <label class="col-md-6" style="padding-top: 10px;">Enter number for per page record</label>
                            <div class="col-md-6">
                            <?=Form::number('total_number', old('total_number'), ['class' => 'form-control has-error col-md-6', 'placeholder' => 'Enter Number', 'id' => 'total_number']);?>
                        </div>
                    </div>
                    <center><b><span id="total_number_error" class="text-danger"></span></b><center>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="text-right">
                    <span class="text-success text-left" id="success_otp"></span>
                        <button type="submit" id="pdf_download" name="download" value="save_new" class="btn btn-primary btn-sm disabled-btn">Download PDF</button>
                    </div>
                </div>
            <?=Form::close()?>
        </div>
    </div>
</div>
<!-- End otp verify Form -->
@stop
@section('script')
<?=Html::script('backend/js/jquery.dataTables.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/dataTables.bootstrap.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/jquery.form.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/fnstand.js', [], IS_SECURE)?>
<?=Html::script('backend/js/delete_script.js', [], IS_SECURE)?>
<!--Script for datatable -->
<script type="text/javascript">
    var table = "city_table";
    var title = "Are you sure to Delete this Record?";
    var text = "You will not be able to recover, once deleted";
    var type = "warning";
    var delete_path = "";
    var token = "<?=csrf_token()?>";

    $('[data-toggle="tooltip"]').tooltip();

    $(function(){
        $('#delete').click(function(){
            var delete_id = $('#'+table+' tbody input[type=checkbox]:checked');
            checkLength(delete_id);
        });
        vendor_table = $('#'+table).dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "autoWidth": false,
            "sAjaxSource": "<?=route('pdf-generate.index')?>",
            "aaSorting": [[ 1, "desc" ]],
            "aoColumns":[
                {
                    mData: "id",
                    bSortable:false,
                    sWidth:"2%",
                    sClass:"text-center",
                    bVisible:false,
                    mRender: function (v, t, o) {
                        return '<div class="animated-checkbox"><label class="m-0"><input class="checkbox" type="checkbox" id="chk_'+v+'" name="special_id[]" value="'+v+'"/><span class="label-text"></span></label></div>';
                    },
                },
                {
                    mData:"creation_date",
                    bVisible : false
                },
                {
                    mData: "name",
                    bSortable:true,
                    sWidth:"20%",
                    sClass:"text-left",
                    mRender: function(v, t, o) {
                        var edit_path = "<?=URL::route('ajax-dependency.edit', array(':id'))?>";
                        edit_path = edit_path.replace(':id',o['id']);

                        var type_name = o['name'].slice(0,30);
                        var act_html  = "<a title='Edit "+o['name']+"' href='"+edit_path+"'>"+ type_name +"</a>"

                        return act_html;
                    }
                },
                {
                    mData:"country",
                    sWidth:"20%",
                    sClass:"text-left",
                    bSortable : false
                },
                {
                    mData:"state",
                    sWidth:"20%",
                    sClass:"text-left",
                    bSortable : false
                },
                {
                    mData:"city",
                    sWidth:"20%",
                    sClass:"text-left",
                    bSortable : false
                },


            ],
            fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
            fnDrawCallback : function (oSettings) {
                $("div.overlay").hide();
            }
        });
    });
</script>
<!--Script for pdf modal -->
<script type="text/javascript">

    $(document).ready(function(){
        $('.enter_record').hide();
    });

    function generate_pdf(){
        $('#generate_pdf').modal('show');
        $('#pdf_download').prop('disabled',false);
    }

    //script for check validation of input value
    $('.custom_pdf').on('change',function(){
        var custome_val = $(this).val();
        if(custome_val == 1)
        {
            $('.enter_record').show();
            $('#pdf_download').prop('disabled',true);
        }
        else
        {
            $('#pdf_download').prop('disabled',false);
            $('.enter_record').hide();
        }
    });

    //script for check validation of input value
    $('#total_number').on('keyup',function(){
        var value = $(this).val();
        var total_record = "<?=$total_record?>";
        $('#pdf_download').prop('disabled',false);
        if(+value > +total_record){
            $('#pdf_download').prop('disabled',true);
            $('#total_number_error').text('Input value is greater than total number of records');
        }
        else
        {
            $('#pdf_download').prop('disabled',false);
            $('#total_number_error').text('');
        }
    });

    //after 15 second hide modal
    setTimeout(function() {
        $('#PDF_form')[0].reset();
        $('.enter_record').hide();
        $('#generate_pdf').modal('hide');
        location.reload();
    }, 15000);

    //script for hide modal
    function closeModel(){
        $('#PDF_form')[0].reset();
        $('.enter_record').hide();
        $('#generate_pdf').modal('hide');
    }
</script>
@stop