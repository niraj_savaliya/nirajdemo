<!DOCTYPE html>
<html lang="en">
<head>
  <title>Employee List</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    .table-border{
        border:1px solid blue
    }
    h2
    {
       font-family: "Open Sans", sans-serif;
    }
    #header_css
    {
      padding :20px !important;
      color:grey;
    }
    .data
    {
      padding: 8px;
      line-height: 1.42857143;
      vertical-align: top;
    }
    .header
    {
      padding: 8px;
      line-height: 1.42857143;
      vertical-align: top;
      color:black;
    }
  </style>
</head>
<body>
  <div class="container">
    <h2>Employees</h2>
    @foreach($user_details as $key => $single)
    <table class="table table-bordered">
      <thead>
        <tr class="success abc">
          <th class="header">Name</th>
          <th class="header">Country</th>
          <th class="header">State</th>
          <th class="header">City</th>
        </tr>
      </thead>
      <tbody>
          @foreach($single as $user)
          <tr>  
            <td class="data"><?=$user['name']?></td>
            <td class="data"><?=$user['country']?></td>
            <td class="data"><?=$user['state']?></td>
            <td class="data"><?=$user['city']?></td>
          </tr>
          @endforeach
      </tbody>
    </table>
    <pagebreak />
    @endforeach
  </div>
</body>
</html>

