@extends('layout.layout')
@section('top_fixed_content')
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4><?=Breadcrumbs::render('edit_employee',$exist_data)?></h4>
        </div>
    </nav>
@stop
@section('style')
<?=Html::script('backend/plugins/select2/css/select2.min.css', [], IS_SECURE)?>
@stop
@section('content')
<?= Form::model($exist_data,['route'=>['ajax-dependency.update',$exist_data['id']],'role'=>'form','method' => 'patch','class'=>'m-0','files'=>true]) ?>
    <div class="card">
        <h3 class="card-title">Edit</h3>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group @if($errors->has('name')) {{ 'has-error' }} @endif">
                        <label>Employee Name<sup class="text-danger">*</sup></label>
                            <?= Form::text('name',old('name'), array('id' => 'name', 'placeholder'=>'Enter Name','class' => 'form-control', 'data-width' => '100%')) ?>
                            <span class='text-danger error'>{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group @if($errors->has('country')) {{ 'has-error' }} @endif">
                        <label>Country<sup class="text-danger">*</sup></label>
                            <?= Form::select('country',$country_list,old('country',$country_id), array('id' => 'country_id', 'class' => 'form-control', 'data-width' => '100%')) ?>
                            <span class='text-danger error'>{{ $errors->first('country') }}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group @if($errors->has('state')) {{ 'has-error' }} @endif">
                        <label>State<sup class="text-danger">*</sup></label>
                            <?= Form::select('state', $state_list, old('state',$state_id), array('id' => 'state_id', 'class' => 'form-control', 'data-width' => '100%') ) ?>
                            <span class='text-danger error'>{{ $errors->first('state') }}</span>
                    </div>
                    <div class="form-group @if($errors->has('city')) {{ 'has-error' }} @endif">
                        <label>City<sup class="text-danger">*</sup></label>
                            <?= Form::select('city',$city_list, old('city',$city_id), array('id' => 'city_id', 'class' => 'form-control', 'data-width' => '100%') ) ?>
                            <span class='text-danger error'>{{ $errors->first('city') }}</span>
                    </div>
                </div>
            </div>
        @include('layout.overlay')
        </div>
        <div class="text-right">
            <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save & Add New Staff">Save</button>
        </div>
    </div>
<?= Form::close() ?>
@stop
@section('script')
<?=Html::script('backend/js/select2.min.js', [], IS_SECURE)?>
<script type="text/javascript">
    $("#country_id").select2({
         placeholder: "Select Country"
    });
    $("#state_id").select2({
        placeholder: "Select State"
    });
    $("#city_id").select2({
        placeholder: "Select City"
    });
</script>
<!-- script for get state and city with ajax --> 
<script type="text/javascript">
    var state_value = $('#state_id :selected').text();
    
    var errors_val = JSON.parse('<?=$errors?>');
    
    if(errors_val.hasOwnProperty('state'))
    { 
        var city_val = '<?=Form::select("city_id",[], array("id" => "city_id"
        ))?>';
        $('#city_id').html(city_val);
    }
   
    $(document).ready(function() {

        var country_id = $('#country_id');

        $("#country_id").on('change',function() {
            loadState($(this).val());
            $("#city_id").val("").trigger('change');
        });

        $("#state_id").on('change',function() {
            loadCity($(this).val());
        });
        function loadState(val){
            var token = "{{csrf_token()}}";
            $.ajax({
                url        : "<?=URL::route('state.get')?>",
                type       : 'post',
                data       : { "country_id": val , '_token' : token},
                dataType   : 'html',
                cache      : false,
                beforeSend : function() {
                   
                },
                complete   : function() {
                
                },
                success    : function(states) {

                    var state_options = $(states).html();
                    $("#state_id").html(state_options);
                    $("#state_id").select2({placeholder: 'Select State'});
                    $("#state_id").val("").trigger('change');
                    loadCity({{ old('state') }});
                }
            });
        }
       
        function loadCity(val){

            var token = "{{csrf_token()}}";
            $.ajax({
            url        : "<?=URL::route('city.get')?>",
            type       : 'post',
            data       : { "state_id": val , '_token' : token },
            dataType   : 'html',
            cache      : false,
            beforeSend : function() {
            },
            complete   : function() {
            },
            success    : function(cities) {
                    var city_options = $(cities).html();
                    $("#city_id").html(city_options);
                    $("#city_id").select2({placeholder: 'Select City'});
                    $("#city_id").val("").trigger('change');
                    
                }
            });
        }
    });
</script>
@stop
