<header class="main-header hidden-print"> 
    <a href="" class="logo">    	
    <p class="small">Thinktanker Lab</p>
	<div data-toggle="offcanvas" class="sidebar-toggle"></div>
    </a>
	@yield('top_fixed_content')
</header>
