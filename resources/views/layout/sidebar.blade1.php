<aside class="main-sidebar hidden-print">
    <section class="sidebar">
        <a href="" class="logo"></a>
        <div class="search-panel">
            <div class="p-10">
                <div class="input-group">
                    <input type="text" id="search_string" name="query-string" class="form-control" placeholder="Search">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="sidebar-content">
             <ul class="sidebar-menu">
                <li class="@if(Request::segment(2) == 'dashboard') active @endif">
                    <a href="<?= route('dashboard') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?=route('activity.index')?>" class="active">
                        <i class="fa fa-list"></i>
                        <span>Activity</span>
                    </a>
                </li>
                <li class="@if(Request::segment(1) == 'ajax-dependency') active @endif">
                    <a href="<?= route('ajax-dependency.index') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Country State City</span>
                    </a>
                </li>
                <li class="@if(Request::segment(2) == 'auth') active @endif">
                    <a href="<?= route('social.index') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Socialite Login</span>
                    </a>
                </li>
                <li class="@if(Request::segment(2) == 'progressbar') active @endif">
                    <a href="<?= route('progress_upload.index') ?>">
                    <i class="fa fa-list"></i>
                    <span>Progressbar</span>
                    </a>
                </li>
                <li class="@if(Request::segment(1) == 'slidertextposition') active @endif">
                    <a href="<?= route('slider.index') ?>">
                    <i class="fa fa-list"></i>
                    <span>SliderTextPosition</span>
                    </a>
                </li>
                <li class="@if(Request::segment(2) == 'customer') active @endif">
                    <a href="<?= route('Customer.index') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Customer</span>
                    </a>
                </li>
                <li class="@if(Request::segment(2) == 'email-attachment') active @endif">
                    <a href="<?= route('email-attachment.index') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>EmailAttachment</span>
                    </a>
                </li>
                <li class="spacer"></li>
                <li class="logout">
                    <a href="javascript:;">
                        <i class="fa fa-user"></i>
                        <span><?=auth('web')->user()->name?></span>
                         <i class="fa fa-angle-right"></i>
                    </a>
                    <ul class="logout-modal">
                        <li><a href="<?=route('logout')?>"> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div> 
    </section>   
</aside>
