@section('style_toastr')
    <?=Html::style('backend/toastr/toastr.min.css', [], IS_SECURE)?>
@stop
@section('script_toastr')
    <?=Html::script('backend/toastr/toastr.min.js', [], IS_SECURE)?>
@stop
@if(Session::has('message'))
    @if( 'danger' == Session::get('message_type') )
    <script>
        $(document).ready(function(){
            toastr.error
            ("<b>{{ Session::get('message') }}</b>");
        });
    </script>
    @elseif( 'success' == Session::get('message_type') )
    <script>
        $(document).ready(function(){
            toastr.success
            ("<b>{{ Session::get('message') }}</b>");
        });
    </script>
    @elseif( 'warning' == Session::get('message_type') )
    <script>
        $(document).ready(function(){
            toastr.warning
            ("<b>{{ Session::get('message') }}</b>");
        });
        </script>
    @endif
@endif
@if($errors->any())
    <script type="text/javascript">
        $(document).ready(function(){
            toastr.error("<b>There were some errors.</b>");
        });
    </script>
@endif