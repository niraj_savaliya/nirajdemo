<aside class="main-sidebar hidden-print">
    <section class="sidebar">
        <a href="" class="logo"></a>
        <div class="search-panel">
            <div class="search-box">
                <div class="icon pull-left"><i class="fa fa-search"></i></div>
                <input type="text" placeholder="Search" class="text">
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="sidebar-content">
             <ul class="sidebar-menu">
                <li class="@if(Request::segment(2) == 'dashboard') active @endif">
                    <a href="<?= route('dashboard') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?=route('activity.index')?>" class="active">
                        <i class="fa fa-list"></i>
                        <span>Activity</span>
                    </a>
                </li>
                <li class="@if(Request::segment(1) == 'ajax-dependency') active @endif">
                    <a href="<?= route('ajax-dependency.index') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Country State City</span>
                    </a>
                </li>
                <li class="@if(Request::segment(2) == 'auth') active @endif">
                    <a href="<?= route('social.index') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Socialite Login</span>
                    </a>
                </li>
                <li class="@if(Request::segment(1) == 'progressbar') active @endif">
                    <a href="<?= route('progress_upload.index') ?>">
                    <i class="fa fa-list"></i>
                    <span>Progressbar</span>
                    </a>
                </li>
                <li class="@if(Request::segment(1) == 'slidertextposition') active @endif">
                    <a href="<?= route('slider.index') ?>">
                    <i class="fa fa-list"></i>
                    <span>SliderTextPosition</span>
                    </a>
                </li>
                <li class="@if(Request::segment(2) == 'customer') active @endif">
                    <a href="<?= route('Customer.index') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Ajax Crud</span>
                    </a>
                </li>
                <li class="@if(Request::segment(2) == 'dynamicform') active @endif">
                    <a href="<?= route('dynamicform.index') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Dynamic Form</span>
                    </a>
                </li>
                <li class="@if(Request::segment(2) == 'inbound-mail') active @endif">
                    <a href="<?= route('inbound_mail.index') ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Inbound mail</span>
                    </a>
                </li>
                <li class="@if(Request::segment(2) == 'ImportDetails') active @endif">
                    <a href="<?= route('import.index') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Import Excel</span>
                    </a>
                </li>
                <li class="@if(Request::segment(1) == 'ipblock-list') active @endif">
                    <a href="<?= route('ipblock.index',['type'=>'whitelist']) ?>">
                    <i class="fa fa-list"></i>
                    <span>Ipblock</span>
                    </a>
                </li>
                <li class="@if(Request::segment(1) == 'pdf-generate') active @endif">
                    <a href="<?= route('pdf-generate.index') ?>">
                    <i class="fa fa-file-pdf-o"></i>
                    <span>PDF Generate</span>
                    </a>
                </li>
                <li class="@if(Request::segment(2) == 'multi-select') active @endif">
                    <a href="<?= route('multi-select.index') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Multi select</span>
                    </a>
                </li>
                <li class="@if(Request::segment(2) == 'multi-select') active @endif">
                    <a href="<?= route('multi-select.index') ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Multi select</span>
                    </a>
                </li>
                <li class="@if(Request::segment(1) == 'email-attachment') active @endif">
                    <a href="<?= route('email-attachment.index') ?>">
                    <i class="fa fa-envelope"></i>
                    <span>EmailAttachment</span>
                    </a>
                </li>
                <li class="@if(Request::segment(1) == 'image-tag') active @endif">
                    <a href="<?= route('image-tag.index') ?>">
                    <i class="fa fa-tags"></i>
                    <span>ImageWithTagging</span>
                    </a>
                </li>
                <li class="@if(Request::segment(1) == 'imagecrop') active @endif">
                    <a href="<?= route('imagecrop.index') ?>">
                    <i class="fa fa-list"></i>
                    <span>Image Crop</span>
                    </a>
                </li>

                <li class="@if(Request::segment(1) == 'multiple-image') active @endif">
                    <a href="<?= route('multiple-image.index') ?>">
                    <i class="fa fa-file-image-o"></i>
                    <span>Image</span>
                    </a>
                </li>
                <li class="@if(Request::segment(1) == 'dynamicform') active @endif">
                    <a href="<?= route('dynamic_ajax_dependency.index') ?>">
                    <i class="fa fa-list"></i>
                    <span>Dynamic Ajax Dependency</span>
                    </a>
                </li>
                @can('allPermission')
                <li class="sub-menu @if(Request::segment(1) == '') active @endif">
                    <a href="javascript:;">
                        <i class="fa fa-user"></i>
                        <span>ACL Permission</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <ul class="treeview-menu @if((Request::segment(1) == 'permissions') || (Request::segment(1) == 'roles')) menu-open @endif">
                        <li>
                            <a href="<?=route('roles.index')?>" class="@if(Request::segment(1) == 'roles') active @endif">
                                <i class="fa fa-calendar"></i>
                                <span>Roles</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?=route('permissions.index')?>"  class=" @if(Request::segment(1) == 'permissions') active @endif">
                            <i class="fa fa-cogs"></i>
                            <span>Permissions</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?=route('users.index')?>"  class=" @if(Request::segment(1) == 'users') active @endif">
                            <i class="fa fa-cogs"></i>
                            <span>Users</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan
                <li class="@if(Request::segment(1) == 'posts') active @endif"s>
                    <a href="<?=route('posts.index')?>">
                    <i class="fa fa-cogs"></i>
                    <span>Posts Master</span>
                    </a>
                </li>
                <li class="spacer"></li>
                <li class="logout">
                    <a href="javascript:;">
                        <i class="fa fa-user"></i>
                        <span><?=auth('web')->user()->name?></span>
                         <i class="fa fa-angle-right"></i>
                    </a>
                    <ul class="logout-modal">
                        <li><a href="<?=route('logout')?>"> Logout</a></li>
                        <li><a href="<?=route('changepassword.post')?>">Change Password</a></li>
                    </ul>
                </li>
            </ul>
        </div> 
    </section>   
</aside>
