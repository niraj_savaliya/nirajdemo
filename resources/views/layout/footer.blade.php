<!-- javascript -->
<?= Html::script('backend/js/jquery-2.1.4.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/essential-plugins.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/bootstrap.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/plugins/pace.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/plugins/jquery.slimscroll.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/plugins/waves.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/timeout.js',[],IS_SECURE) ?>
@yield('script_toastr')
@yield('script')

<?=Html::script('backend/js/sweetalert.min.js')?>
<?= Html::script('backend/js/main.js',[],IS_SECURE) ?>