<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="theme-color" content="#ffffff">
        <?= Html::style('backend/font-awesome/css/font-awesome.min.css',[],IS_SECURE) ?>
        @yield('inbound_mail')
        <?= Html::style('backend/css/main.css',[],IS_SECURE) ?>
        <title>Thinktanker Lab</title>
        <!-- File to add css files to page -->
        <!-- CSS-->
        @yield('style_toastr')
        @yield('style')
    </head>
    <body id="body-collapse"  class="sidebar-mini fixed">
        <div class="wrapper">
            @yield('start_form')
            <!-- Header file -->
            @include('layout.header')
            <!-- Side-Bar-->
            @include('layout.sidebar')
            <!-- Contant Start here -->
            <div class="content-wrapper">
                @yield('content')
            </div>
            <!-- footer contant -->
            @yield('end_form')
        </div>
        <!-- Js file -->
        @include('layout.footer')
        @yield('modal')
        
    </body>
</html>