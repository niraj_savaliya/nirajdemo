@extends('layout.layout')
@section('top_fixed_content')
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4><i class="fa fa-list"></i>student</h4>    
        </div>
        @include('layout.header')
    </nav>
@stop
@section('content')
    <div class="row relative">
        <?=Form::open(['route'=>'dynamicform.store', 'class' => 'form-horizontal', 'method' => 'POST'])?>
        <div class="col-md-12">
            <div class="card">
                <div class="card-title-w-btn">
                    <h4 class="title">Create Student Detail</h4>
                </div>
                <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class=" @if($errors->has('fname')) {{ 'has-error' }} @endif">
                                <label>FirstName<sup class="text-danger">*</sup></label>
                                <?=Form::text('fname', null, ['class' => 'form-control', 'placeholder' => 'Enter First Name']);?>
                                <span id="fname_error" class="help-inline text-danger"><?=$errors->first('fname')?></span>
                            </div>
                            </br>
                            <div class=" @if($errors->has('email')) {{ 'has-error' }} @endif">
                                <label>Email<sup class="text-danger">*</sup></label>
                                <?=Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Email']);?>
                                <span id="email_error" class="help-inline text-danger"><?=$errors->first('email')?></span>
                            </div></br>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @if($errors->has('lname')) {{ 'has-error' }} @endif">
                                <label>LastName<sup class="text-danger">*</sup></label>
                                <?=Form::text('lname', null, ['class' => 'form-control', 'placeholder' => 'Enter Last Name']);?>
                                <span id="lname_error" class="help-inline text-danger"><?=$errors->first('lname')?></span>
                            </div>
                            </br>
                            <div class="form-group @if($errors->has('address')) {{ 'has-error' }} @endif"  style="margin-top: -15px">
                                <label>Address<sup class="text-danger">*</sup></label>
                                <?= Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Enter Address','rows'=>"2"]); ?>
                                <span id="address_error" class="help-inline text-danger"><?= $errors->first('address') ?></span>
                            </div><br>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-title-w-btn">
                        <h4 class="title">Other Detail</h4>
                    </div>
                    <hr>
                    <div class="card-body">
                        <div class="form-group">
                            <table class="table m-0 v-top">
                                <thead>
                                    <tr>
                                        <th style="border:none">Date of Birth<sup class="text-danger">*</sup></th>
                                        <th style="border:none">City<sup class="text-danger">*</sup></th>
                                        <th style="border:none">Mobile No<sup class="text-danger">*</sup></th>
                                        <th style="border:none">Pincode<sup class="text-danger">*</sup></th>
                                        <th style="border:none"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="student">
                                        <td class="col-md-2" style="position: relative">
                                            <?= Form::text('birthdate',old('birthdate'), ['class' => 'form-control','placeholder'=>'yyyy/mm/dd','id'=>'birthdate']); ?>
                                            <i class="fa fa-calendar" style=" position: absolute;right: 16px;top: 22px; "></i>
                                            <span id="birthdate_error" class="help-inline text-danger"><?= $errors->first('student.student.birthdate') ?></span>
                                        </td>
                                        <td class="col-md-3">
                                            <?= Form::text('city',old('city'), ['class' => 'form-control','placeholder'=>'City','id'=>'city']); ?>
                                            <span id="city_error" class="help-inline text-danger"><?= $errors->first('student.student.city') ?></span>
                                        </td> 
                                        <td class="col-md-3">
                                            <?= Form::text('mobile',old('mobile'), ['class' => 'form-control','placeholder'=>'Mobile','id'=>'mobile']); ?>
                                            <span id="mobile_error" class="help-inline text-danger"><?= $errors->first('student.student.mobile') ?></span>
                                        </td>
                                        <td class="col-md-3">
                                            <?= Form::text('pincode',old('pincode'), ['class' => 'form-control','placeholder'=>'Pincode','id'=>'pincode']); ?>
                                            <span id="pincode_error" class="help-inline text-danger"><?= $errors->first('student.student.pincode') ?></span>
                                        </td>
                                        <td class="col-md-2" style="border:none">        
                                            <a href="javascript:void(0)" id="student_add" class="pt-10 pull-left btn-add"><i class="fa fa-plus-circle fa-small pull-left" ></i>&nbsp;&nbsp;</a>
                                            <a href="javascript:void(0)" id="student_remove" class="pt-10 pull-left btn-remove"><i class="fa fa-minus-circle fa-small pull-left"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group text-right">
                    <button data-showloading="yes" type="submit" name="save" value="exit" class="btn btn-primary btn-xs waves-effect waves-light">Save</button>
                    <button  data-showloading="yes"  type="submit" class="btn btn-primary btn-xs waves-effect waves-light" name="save_button" value="save_new" >Save & new</button>
                    <a data-showloading="yes" href="<?=URL::route('dynamicform.index')?>" class="btn btn-default btn-xs waves-effect">Back</a>
                </div>
            </div>
        </div>
        <?=Form::close();?>
    </div>
@stop

@section('script')
<?= Html::script('backend/js/dynamicform.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/jquery.form.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/plugins/bootstrap-datepicker.min.js',[],IS_SECURE) ?>
<script type="text/javascript">

    $("input[name='birthdate']").click(function(){
        select_list = $('tbody tr');
        
        $(this).datepicker({
            autoclose: true,
            format: "yyyy/mm/dd",
        }).focus();
        // $.each(select_list,function(k,v){
        //     var datepicker = $(v).find('#birthdate1'+k);
        //     datepicker.datepicker({
        //         autoclose: true,
        //         format: "yyyy/mm/dd",
        //     }).focus();
        //     //datepicker.trigger('dp.show');
        // })
    });

    $(document).ready(function(){       

        var dynamic_form1 = $("#student").dynamicForm("#student_add", "#student_remove", {
            limit:5,
            normalizeFullForm: false
        });

        @if(old('student.student'))
            dynamic_form1.inject( <?= json_encode(old('student.student')) ?> );
        @endif

        @if($errors)
        
        var detail_Errors = <?= json_encode($errors->toArray()) ?>;

        $.each(detail_Errors, function(id,msg){
            var id_arr = id.split('.');
            if (id_arr[3] == 'birthdate') {
                $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
            }
            if (id_arr[3] == 'city') {
                $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
            }
            if (id_arr[3] == 'mobile') {
                $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
            }
            if (id_arr[3] == 'pincode') {
                $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
            }
        });
        @endif
    });
</script>
@stop
