@extends('layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4></h4>
    </div>
</nav>
@stop
@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-title-w-btn">
                    <h3 class="title">Change Password</h3>
                </div>
                <hr>
                <div class="card-body">
                    <?=Form::open(array('url' => route('changepassword.post'), 'class' => 'form-horizontal'))?>
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Old Password<sup class="text-danger">*</sup></label>
                                    <?=Form::password('old_password', ['placeholder' => 'Old Password', 'class' => 'form-control'])?>
                                    <span class="text-danger"><?=$errors->first('old_password')?></span>
                                </div>
                                <div class="form-group">
                                    <label>New Password<sup class="text-danger">*</sup></label>
                                    <?=Form::password('password', ['class' => 'form-control', 'placeholder' => "New Password"])?>
                                    <span class="text-danger"><?=$errors->first('password')?></span>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password<sup class="text-danger">*</sup></label>
                                    <?=Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => "Confirm New Password"])?>
                                    <span class="text-danger"><?=$errors->first('password_confirmation')?></span>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="clearfix"></div>
                        @include('layout.overlay')
                        <div class="card-footer">
                           <div class="row">
                                <div class="col-md-4 col-md-offset-6">
                                    <button type="submit" class="btn btn-primary btn-sm" title="Save change password">Save</button>&nbsp;&nbsp;&nbsp;
                                    <a href="<?=URL::route('dashboard')?>" class="btn btn-default btn-sm" title="close without change password">Close</a>
                                </div>
                           </div>
                        </div>
                    <?=Form::close()?>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
	@include('layout.alert')
@stop

