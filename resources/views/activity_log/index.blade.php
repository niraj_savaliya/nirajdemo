@extends('layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
    <div class="title">
       <h4><i class="fa fa-list"></i> Activity</h4>
    </div>
    
</nav>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body table-responsive">
                <div class="text-right">
                    <div class="number-delete">
                        <ul>
                            <li>
                                <p class="mb-0"><span class="num_item"></span>Item Selected.</p>
                            </li>
                            <li class="bulk-dropdown"><a href="javascript:;">Bulk actions<span class="caret"></span></a>
                                <div class="bulk-box">
                                    <div class="bulk-tooltip"></div>
                                    <ul class="bulk-list">
                                        <li><a href="javascript:void(0);" id="delete" class="delete-btn" title="Delete Single Or Multiple brands">Delete Selected Brand</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <table class="table" id="activity_table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Content type</th>
                            <th>Action</th>
                            <th>Description</th>
                            <th>Detail</th>
                        </tr>
                    </thead>
                </table>
            </div>
            @include('layout.overlay')
        </div>
    </div>
</div>
@stop
@section('script')
    <?=Html::script('backend/js/jquery.dataTables.min.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/dataTables.bootstrap.min.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/jquery.form.min.js', [], IS_SECURE)?>
    <?=Html::script('backend/js/fnstand.js', [], IS_SECURE)?>

     <script type="text/javascript">
        var table = "activity_table";

        $(function(){
           
            var activity_table = $('#'+table).dataTable({
                "bProcessing": false,
                "bServerSide": true,
                "autoWidth": false,
                "sAjaxSource": "<?=URL::route('activity.index')?>",
                "aaSorting": [[ 1, "asc" ]],
                "orderSequence":["asc"],
                "aoColumns":[
                {
                        mData: "id",
                        bSortable:false,
                        sWidth:"10%",
                        sClass:"text-center",
                        bVisible:false,
                        mRender: function (v, t, o) {
                            return '<div class="animated-checkbox"><label class="m-0"><input class="checkbox" type="checkbox" id="chk_'+v+'" name="id[]" value="'+v+'"/><span class="label-text"></span></label></div>';
                        },
                    },
                    {mData:"content_type",bSortable:true,sClass:"text-left",sWidth:"10%"},
                    {mData:"action",bSortable : true,sClass : 'text-left',sWidth:"10%"},
                    {mData:"description",bSortable : true,sClass : 'text-left',sWidth:"10%"},
                    {mData:"details",bSortable : true,sClass : 'text-left',sWidth:"10%"},
                ],
                fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
                fnDrawCallback : function (oSettings) {
                    $("div.overlay").hide();
                }
            });
        });
    </script>
    @include('layout.alert')
@stop