@extends('layout.layout')
@section('content')
	<div class="text-center">
		<h2>Progressbar</h2>
		<div style="border: 1px solid #a1a1a1;text-align: center;width: 500px;padding:30px;margin:0px auto">
			<div class="card">
				<div id="img" style="display: none;width: 100%;height: 200px"></div><br>
				<?= Form::open(['method'=>'POST','files'=>'true','class'=>'form-horizontal'])?>
					<div class="preview"></div>
					<div class="progress" style="display:none;height: auto;">
					  <div class="progress-bar" role="progressbar" aria-valuenow="0"
					  aria-valuemin="0" aria-valuemax="100" style="width:0%">
					    0%
					  </div>
					</div>
					<div class=" ">
						<input type="file" name="image" class="form-control" id="im"/>
						<div class="text-danger" id="image_error"></div>
					</div>
					<button class="btn btn-primary upload-image mt-20">Upload Image</button>
					<button class="btn btn-primary back-btn mt-20"><a href="<?= route('progress_upload.index')?>">Back</a></button>
			</div>
			<?= Form::close()?>
		</div>
	</div>
@stop
@section('style')
	<style type="text/css">
		#img img{
			width: 100%;
			height: 100%
		}
		.back-btn a{
			color: #fff;
		}
	</style>
	<?=Html::style('backend/css/toastr.min.css',[],IS_SECURE)?>
@stop
@section('script')
<?=Html::script('backend/js/jquery.form.js',[],IS_SECURE)?>
<?=Html::script('backend/js/plugins/toastr.min.js',[],IS_SECURE)?>
<script> 
    $(document).ready(function() { 
    	$(".back-btn").hide();
     	var progressbar     = $('.progress-bar'); 
        $(".upload-image").click(function(){
        	var name = $("#im").val();
            	var token = "<?php csrf_field();?>";
         		var url = "<?= route('progress_upload.store')?>";
            	$(".form-horizontal").ajaxForm(
				{
					url : url,
					type : 'POST',
					data : {
						'token' : token,
						'name' : name
			 		},
					target: '.preview',
					beforeSend: function() {
						if(name != ""){
							$(".progress").css("display","block");
							progressbar.width('0%');
							progressbar.text('0%');
						}
					},
					uploadProgress: function (event, position, total, percentComplete) {
					progressbar.width(percentComplete + '%');
					var t =  Math.floor((event.loaded / event.total) *100) + '%';
					progressbar.text(t);
					},
					success : function(data){
						$("#img").css("display","block");
						$('#img').html('<img src=/progressbar/'+data.message+'/>');
						$(".upload-image").hide();
						$(".back-btn").show();
						toastr.success('image uploaded sauccessfully');
					},
					error : function(respObj){
						$(".progress").css("display",'none');
						$.each(respObj.responseJSON.errors,function(k,v){
							$('#'+k+'_error').text(v);
							toastr.error('There was some error');
						});
					},
				})
            });
        return false;
    }); 
</script>
@stop