@extends('layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-list"></i>Image Tagging</h4>
    </div>
</nav>
@stop
@section('content')    
<div class="container1">
    <div class="box-content card white fileUpMain-box">
        <div class="card-body relative uploadMain">
            <div class="row">
                <?= Form::model($tag_image,['route'=>['image-tag.update'],'method' => 'patch','form-horizontal','files'=>true]) ?> 
                     <div class="col-md-12 fileUpMain">
                        <div class="pdfMain ">   
                            <div class="dropPdfFile">
                                <div class="row">
                                    <label class="control-label col-md-12"></label>
                                    <div class="col-md-12">
                                        <div class="dropzone" id="images"></div>
                                        <div id="images_data" style="display:none"></div>
                                        <div id="images_datas" style="display:none"></div>
                                        <div id="images_name"></div>
                                    </div>
                                </div>          
                            </div>
                        </div>
                        <div class="col-md-5"></div>
                        <span class="text-danger">{{ $errors->first('images')}}</span>
                        <span class="text-danger">{{ $errors->first('image_name.*')}}</span>
                        
                    </div>
                    <div class="text-right mb-20">
                        <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_data" title="Save & Exit">Save</button>
                        <a href="{{route('image-tag.index')}}" class="btn btn-border btn-sm disabled-btn" title="Cancel">Cancel</a>
                    </div>
                <?= Form::close()?>
            </div>    
        </div>
    </div>
</div>
<?php
$images = $tag_image['image_name'];
?>
@stop
@section('style')

{{ Html::style('backend/css/dropzone.css') }}
{{ Html::style('backend/css/style.css') }}
{{ Html::style('backend/css/custom.css') }}

@stop
@section('script')

{{ Html::script('backend/js/bootstrap-fileupload.js') }}
{{ Html::script('backend/js/dropzone.js') }}
{{ Html::script('backend/js/jquery-ui.min.js') }}
{{ Html::script('backend/js/image.js') }}


<script type="text/javascript">
    Dropzone.options.images = {
        maxFilesize         :       3,
        acceptedFiles: ".jpeg,.png,.jpg", 
        init:function(){

            myDropzone = this;
            $.get('/image-tag/editImage/<?= $tag_image['id'] ?>',function(data){
                $.each(data.images, function (key, value) {
                         
                    var file = {name: value.image_name};
                    myDropzone.options.addedfile.call(myDropzone, file);
                    myDropzone.options.thumbnail.call(myDropzone, file, '../../upload/permenent/' + value.image_name);
                    myDropzone.emit("complete", file);           
                    $('#images_name').append('<div id="input'+value.image_id+'">    <input type="text" name="image_name[]" value="'+value.image_caption+'" placeholder="enter image name"></div>')
                    $('#images').append('<input type="hidden" name="images[]" value="'+value.image_id+'">');
                });     
            });     
        }
       
    };
    var myDropzone = buildEditDropZone("div#images", 10, "{{ URL::to('/') }}", 'images', 'property', "{{ csrf_token() }}","<?=$tag_image['image_name']?>");

    
</script>
@include('layout.alert')
@stop