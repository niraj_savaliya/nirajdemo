@extends('layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-list"></i>Image With Tagging</h4>
    </div>
    <div class="top_filter"></div>
    <div class="pl-10">
         <a href="<?=route('image-tag.create');?>" class="btn btn-primary btn-sm" title="Add New">Add New</a>
    </div>
</nav>
@stop
@section('content')
<div class="row">
   <div class="col-md-12">
      <div class="card">
         <div class="card-body table-responsive">
            <div class="text-right">
               <div class="number-delete">
                  <ul>
                     <li>
                        <p class="mb-0"><span class="num_item"></span>Item Selected.</p>
                     </li>
                     <li class="bulk-dropdown">
                        <a href="javascript:;">Bulk actions<span class="caret"></span></a>
                        <div class="bulk-box">
                           <div class="bulk-tooltip"></div>
                           <ul class="bulk-list">
                              <li><a href="javascript:void(0);" id="delete" class="delete-btn">Delete selected Images</a></li>
                           </ul>
                        </div>
                     </li>
                  </ul>
               </div>
               <div class="clearfix"></div>
            </div>
            <table id="imagetag_table" class="table table-hover">
               <thead>
                  <tr>
                     <th class="select-all no-sort">
                        <div class="animated-checkbox">
                           <label class="m-0">
                           <input type="checkbox" id="checkAll" />
                           <span class="label-text"></span>
                           </label>
                        </div>
                     </th>
                     <th>Image Caption</th>
                     <th>Image</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody></tbody>
            </table>
         </div>
         @include('layout.overlay')
      </div>
   </div>
</div>
@stop
@section('script')
<?= Html::script('backend/js/jquery.dataTables.min.js') ?>
<?= Html::script('backend/js/dataTables.bootstrap.min.js') ?>
<?= Html::script('backend/js/fnstandrow.js') ?>
<?= Html::script('backend/js/delete_script.js') ?>
<?= Html::script('backend/js/jquery.form.min.js') ?>
<?= Html::script('backend/js/sweetalert.min.js') ?>

<script type="text/javascript">
  var table = "imagetag_table";
  var title = "Are you sure to delete this Image?";
  var text = "You will not be able to recover this record";
  var type = "warning";
  var delete_path = "<?= route('image-tag.delete') ?>";
  var token = "<?= csrf_token() ?>";  
  
  $(function(){    
        $('#delete').click(function(){
            var delete_id = $('#'+table+' tbody input[type=checkbox]:checked');
            checkLength(delete_id);
        });
    imagetag_table = $('#'+table).dataTable({
            "bProcessing": false,
            "bServerSide": true,                              
            "autoWidth": false,
            "sAjaxSource": "<?= URL::route('image-tag.index') ?>",
            "aaSorting": [[ 1,"desc"]],
            "aoColumns": [
            {
                mData: "id",
                bSortable:false,
                sWidth:"5%",
                sClass:"text-center",
                mRender: function (v, t, o) {
                  return '<div class="animated-checkbox"><label class="m-0"><input class="checkbox" type="checkbox" id="chk_'+v+'" name="special_id[]" value="'+v+'"/><span class="label-text"></span></label></div>';
                },
            },
            { mData:"image_caption",bSortable : true,sWidth:"20%"},
            { mData:"image_name",bSortable : false,sWidth:"15%",
                mRender: function(v,t,o){
                    var act_html = '<img src="upload/permenent/' +v+ '" style="border:1px solid #000;width:101px;" />';
                    return act_html;
                }
            },
            {
                mData: null,
                bSortable: false,
                sWidth: "7%",
                sClass: "text-center",
                mRender: function(v, t, o) {

                    var editurl = "{{ route('image-tag.edit',':id')}}";
                    editurl = editurl.replace(':id',o['id']);


                    var act_html = "<div class='btn-group'>"
                    +"<a href='"+editurl+"'class='btn btn-default btn-sm'><i class='fa fa-pencil' title='Edit Record'></i></a>"
                    +"</div>"
                    return act_html;
                }
            },
            ],
            fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
            fnDrawCallback : function (oSettings) {
                $("div.overlay").hide();
            }
        });
    });
</script>
@include('layout.alert')
@stop