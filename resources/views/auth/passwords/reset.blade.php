<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Project</title>

    <!-- Bootstrap core CSS -->
    <?= Html::style('backend/css/main.css') ?>
    <?= Html::style('backend/toastr/toastr.min.css',[],IS_SECURE) ?>

</head>
<body>
    <section class="material-half-bg"></section>
    <section class="login-content">
        <div class="login-box">
            <div class="login_logo">
                <p class="m-0"><b>Thinktanker Lab</b></p>
            </div>
            <?= Form::open(['url'=>route('password.reset.post'),'class'=>'form-signin','method'=>'POST']) ?>
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="login-head">
                    <h3 class="m-0">Log in to your account</h3>
                </div>
                <div class="form-group @if($errors->first('email')) has-error @endif">
                    <label class="control-label"></label>
                    <?= Form::label('email', 'USERNAME',['class'=>'control-label']); ?>
                    <?= Form::email('email',null,['class'=>'form-control','autofocus','placeholder'=>'Enter your email']) ?>
                    <span class="text-primary"><?= $errors->first('email') ?></span>
                </div>
                <div class="form-group @if($errors->first('password')) has-error @endif">
                    <label class="control-label"></label>
                    <?= Form::label('password', 'PASSWORD',['class'=>'control-label']); ?>
                    <?= Form::password('password',['class'=>'form-control','placeholder'=>'Enter your password']) ?>
                    <span class="text-primary"><?= $errors->first('password') ?></span>
                </div>
                <div class="form-group @if($errors->first('password_confirmation')) has-error @endif">
                    <label class="control-label"></label>
                    <?= Form::label('password_confirmation', 'CONFIRMATION PASSWORD',['class'=>'control-label']); ?>
                    <?= Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Enter your password']) ?>
                    <span class="text-primary"><?= $errors->first('password_confirmation_error') ?></span>
                </div>
                <div class="form-group btn-container">
                  <button class="btn btn-primary btn-block form-control">SIGN IN <i class="fa fa-sign-in fa-lg"></i></button>
                </div>
            <?= Form::close() ?>
        </div>
    </section>
</body>


