<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?=Html::style('backend/css/main.css')?>
    <?=Html::style('backend/toastr/toastr.min.css')?>
    <?=Html::style('backend/css/new-login.css')?>
    <title>Thinktanker Lab</title>
</head>
<body>
    <section class="login-new">
        <div id="lab"><div id="beaker"> <span class="bubble"> <span class="glow"> </span> </span> <span class="bubble1"> <span class="glow"> </span> </span> <span class="bubble2"> <span class="glow"> </span> </span> <span class="bubble3"> <span class="glow"> </span> </span> <span class="bubble4"> <span class="glow"> </span> </span> </div></div>
        <section class="login-content">
            <div class="login-box">
                <div class="login_logo">
                    <p class="m-0"><b>Thinktanker Lab</b></p>
                </div>
                <?=Form::open(['url' => 'login', 'id' => 'login_form', 'class' => 'login-form'])?>
                    <div class="login-head">
                        <h3 class="m-0">Log in to your account</h3>
                    </div>
                    <div class="form-group @if($errors->first('email')) has-error @endif">
                        <label class="control-label"></label>
                        <?=Form::label('email', 'USERNAME', ['class' => 'control-label']);?>
                        <?=Form::email('email', null, ['class' => 'form-control', 'autofocus', 'placeholder' => 'Enter your email'])?>
                        <span class="text-primary"><?=$errors->first('email')?></span>
                    </div>
                    <div class="form-group @if($errors->first('email')) has-error @endif">
                        <label class="control-label"></label>
                        <?=Form::label('password', 'PASSWORD', ['class' => 'control-label']);?>
                        <?=Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter your password'])?>
                        <span class="text-primary"><?=$errors->first('password')?></span>
                    </div>
                    <div class="form-group">
                        <div class="utility">
                            <div class="animated-checkbox">
                                <label class="semibold-text">
                                  <?=Form::checkbox('name', 'remember');?><span class="label-text">Stay Signed in</span>
                                </label>
                            </div>
                            <p class="semibold-text mb-0"><a id="toFlip" href="#" class="forget_link">Forgot Password ?</a></p>
                        </div>
                    </div>
                    <div class="form-group btn-container">
                      <button class="btn btn-primary btn-block form-control">SIGN IN <i class="fa fa-sign-in fa-lg"></i></button>
                    </div>
                <?=Form::close()?>
                <form id="forget-form" class="forget-form">
                    <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
                    <div class="form-group @if($errors->first('email')) has-error @endif">
                        <label class="control-label"></label>
                        <?=Form::label('email', 'EMAIL', ['class' => 'control-label']);?>
                        <?=Form::email('email', null, ['class' => 'form-control', 'autofocus', 'placeholder' => 'Enter your email'])?>
                        <span id="email_error" class="text-danger help-block"></span>
                    </div>
                    <div class="form-group btn-container">
                        <button class="btn btn-primary btn-block" type="submit" id="send_mail">SEND <i class="fa fa-unlock fa-lg"></i></button>
                    </div>
                    <div class="form-group mt-20">
                        <p class="semibold-text mb-0"><a id="noFlip" href="#"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
                    </div>
                </form>
            </div>
        </section>
    </section>
</body>
<?=Html::script('backend/js/jquery-2.1.4.min.js')?>
<?=Html::script('backend/js/bootstrap.min.js')?>
<?=Html::script('backend/js/essential-plugins.js')?>
<?=Html::script('backend/js/jquery.form.min.js')?>
<?=Html::script('backend/toastr/toastr.min.js')?>
<?=Html::script('backend/js/main.js')?>

<script type="text/javascript">
    var token = "<?=csrf_token()?>";
    $('form').submit(function(){
        $('.overlay').show();
    });
    $(document).ready(function(){
        $('#send_mail').click(function(e,ele) {
            $('#forget-form').ajaxSubmit({
                url: "<?=URL::route('password.email')?>",
                type: 'post',
                data: { "_token" : token },
                dataType: 'json',
                beforeSubmit : function()
                {
                    $("[id$='_error']").empty();
                },
                success : function(resp)
                {
                    if (resp.success == false) {
                        $('#email_error').text(resp.message);
                    }
                    if (resp.success == true) {
                        $('#forget-form')[0].reset();
                        toastr.success(resp.message)
                    }
                },
                error : function(respObj){
                    $.each(respObj.responseJSON, function(k,v){
                        $('#'+k+'_error').text(v);
                    });
                }
            });
            return false;
        });
    });
</script>
</html>


