<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportExcelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_excels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100)->comment = "name of user";
            $table->string('email',100)->comment = "Email address of user";
            $table->char('mobile',15)->comment = "Mobile number of user";
            $table->tinyInteger('is_active')->comment = "Status of user";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_excels');
    }
}
