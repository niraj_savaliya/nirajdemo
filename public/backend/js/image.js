Dropzone.autoDiscover = false;

function bulidDropZone(ele, maxfile, root, field, folder, token) {

    field = field || "image_id";

   // console.log(field);

    var myDropzone = new Dropzone(ele, {
        url             : root + '/images/move',
        uploadMultiple  : true,
        addRemoveLinks  : true,
        parallelUploads : 1,
        maxFiles        : maxfile,
    });

    myDropzone.on("sending", function(file, xhr, formData) {
        formData.append('_token', token);
    })

    var dz_count = 0;
    myDropzone.on("success", function(file, response) {
        
        $('#' + field + '_data').append('<input type="hidden" value="'+response.image_id+'" class="dropzone_hidden" name="'+field+'[]">');
        // $('#' + field + '_datas').append('<input type="hidden" value="'+response.image_id+'" class="dropzone_hidden" name="'+field+'[]">');
        $(file.previewTemplate).append('<span class="image_id" style="display:none">'+response.image_id+'</span>');
        dz_count++;
      
        $('#images_name').append('<div class="input'+response.image_id+'"><input type="text" name="image_name[]"></div>');
       });
   
    myDropzone.on("removedfile", function(file, response) {

       var image_id = $(file.previewTemplate).children('.image_id').text();
       // console.log(image_id)
       $('#images_name').remove();
       $('input:hidden[value="'+image_id+'"]').remove();
        $.ajax({
            url: root + '/images/delete',
            type: 'POST',
            data: { 'id': image_id, 'folder' : folder, _token : token }
            
        });
    });

    return myDropzone;
}
function buildEditDropZone(ele, maxfile, root, field, folder, token) {

    field = field || "image_id";

   // console.log(field);

    var myDropzone = new Dropzone(ele, {
        url             : root + '/images/move',
        uploadMultiple  : true,
        addRemoveLinks  : true,
        parallelUploads : 1,
        maxFiles        : maxfile,
    });

    myDropzone.on("sending", function(file, xhr, formData) {
        formData.append('_token', token);
    })

    var dz_count = 0;
    myDropzone.on("success", function(file, response) {
        
        $('#' + field + '_data').append('<input type="hidden" value="'+response.image_id+'" class="dropzone_hidden" name="'+field+'[]">');
        // $('#' + field + '_datas').append('<input type="hidden" value="'+response.image_id+'" class="dropzone_hidden" name="'+field+'[]">');
        $(file.previewTemplate).append('<span class="image_id" style="display:none">'+response.image_id+'</span>');
        dz_count++;
      
        $('#images_name').append('<div class="input'+response.image_id+'"><input type="text" name="image_name[]"></div>');
       });
   
    myDropzone.on("removedfile", function(file, response) {
        var token = "<?=csrf_token()?>";
        // var url = root+"/images/edit/delete";
        // console.log(url);
        $.ajax({
            url: root + '/images/edit/delete',
            type: 'get',
            dataType: 'json',
            data: { 'file': file.name,'_token':token},
            success : function(resp)
            {
                window.location.reload();
            }
        });
    });

    return myDropzone;
}


function loadDropZoneImage(myDropzone, img_id, img_name, img_size, img_path, field) {
    var mockFile = { name : img_name, size : img_size };
    myDropzone.options.addedfile.call(myDropzone, mockFile);
    $(mockFile.previewElement).attr(field, img_id);
    myDropzone.emit("complete", mockFile);
    myDropzone.options.thumbnail.call(myDropzone, mockFile, img_path);
    $(mockFile.previewTemplate).append('<span class="image_id" style="display:none">'+img_id+'</span>');
    $('#'+field+'_data').append('<input type="hidden" value="'+img_id+'" class="dropzone_hidden" name="'+field+'[]">');
}