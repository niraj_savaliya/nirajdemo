<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'mailgun.thinktanker.in',
        'secret' => 'key-9fb639560e6cfbb0ae289ea7eb783bfa',
    ],
    
    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id'     => env('FACEBOOK_ID'),
        'client_secret' => env('FACEBOOK_SECRET'),
        'redirect'      => env('FACEBOOK_URL'),
    ],
    'google' => [
        'client_id'     => env('GOOGLE_ID'),
        'client_secret' => env('GOOGLE_SECRET'),
        'redirect'      => env('GOOGLE_REDIRECT')
    ],
    'github' => [
        'client_id' => env('GH_ID'),
        'client_secret' => env('GH_SECRET'),
        'redirect' => env('GH_URL'),
    ],
    'twitter' => [
        'client_id' =>  env('twitter_ID'),
        'client_secret' =>  env('twitter_SECRET'),
        'redirect' => env('twitter_URL'),
    ],
    'linkedin' => [
        'client_id' => env('linkedin_ID'),
        'client_secret' =>env('linkedin_SECRET'),
        'redirect' =>env('linkedin_URL'), 
    ],
    'slack' => [
        'webhook_url' => env('SLACK_WEBHOOK_URL'),
    ],
];
